#!/bin/sh

## Annotations
## The indexed FASTA file for the genome build. See http://www.broadinstitute.org/gsa/wiki/index.php/Input_files_for_the_GATK#Reference_sequence
## FASTA must also be indexed for BWA. $ bwa index
REFERENCE=@REFERENCE@
## VCF file for dbSNP

## TODO: Probaly don't really need to set anything more. Just Pull everything out of ponc.properties
#DBSNP_VCF=@DBSNP_VCF@
## VCF file for NHLBI Exom sequencing project
#NHLBI_VCF=@NHLBI_VCF@

#DBNSFP_TABIX=@DBNSFP_TABIX@

#COSMIC_VCF=@COSMIC_VCF@
#CLD_TO_MODEL=@CLD_TO_MODEL@

## Tools   HCHCHC
#PONC_TOOLS=${PONC_HOME}/bin/PoncTools

## Tested with snpEff_2_0_5d
#EFF_DIR=@EFF_DIR@
#EFF_DB=@EFF_DB@
## Tested with picard-tools-1.59
#PICARD_TOOLS_DIR=@PICARD_TOOLS_DIR@
#SAMTOOLS=@SAMTOOLS@
## tested with bwa Version: 0.5.9-r16
#BWA=@BWA@
#SCRIPTS_DIR=@SCRIPTS_DIR@
## Require Python-2.6.6 or later
#PYTHON_PATH=@PYTHON_PATH@
#PYTHON=$PYTHON_PATH/bin/python
#PATH=$PATH:$PYTHONPATH
#PYTHON_SCRIPTS_DIR=$SCRIPTS_DIR/python

## Require java version 1.6 or later
#PONC_JAVA_HOME=@PONC_JAVA_HOME@
#JAVA=${PONC_JAVA_HOME}/bin/java
JAVA=@PONC_JAVA_HOME@
GATK=@GATK@

#FASTQC=@FASTQC@

#PARSE_VCF=@PARSE_VCF@
#PONCOTATOR=@PONCOTATOR@

#BWA=@BWA@

## export PYTHONPATH=/home/kornjo1/Python-2.6.6/my-pkgs/lib/python
