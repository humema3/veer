#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M maxwell.hume@novartis.com
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-@NUM_SAMPLES@
#$ -l m_mem_free=@NUM_GIGS_MEM@G
#$ -l h_rt=@MAX_RUNTIME@
#$ -pe smp @NUM_CORES@
#$ -j y
#$ -N @JOB_NAME@

source ../ponc_profile.sh

stime=$(date '+%s')

ID=$SGE_TASK_ID
ALLELES_DIR=@ALLELES_DIR@
ALLELES_FILENAME=@ALLELES_FILENAME@
SMF_BAM_LIST_FILE=@SMF_BAM_LIST_FILE@
NUM_SAMPLES=@NUM_SAMPLES@
NUM_CONTIGS=@NUM_CONTIGS@
MEMORY=@NUM_GIGS_MEM@g
JAVA="${JAVA} -Xmx${MEMORY}"

# check that there are enough samples listed in the file
NUM_SAMPLE_LINES=$((`wc -l $SMF_BAM_LIST_FILE | awk '{print $1}'` - 1))
if [ "$NUM_SAMPLE_LINES" -lt "$NUM_SAMPLES" ]; then
    echo "More samples requested than are present in the file"
    exit 1
fi
# warn if not all samples are used
if [ "$NUM_SAMPLE_LINES" -gt "$NUM_SAMPLES" ]; then
    echo "********** WARNING: Only the first $NUM_SAMPLES out of the total $NUM_SAMPLE_LINES samples from the file $SMF_BAM_LIST_FILE are being used **********"
fi

SAMPLE_NUM=$ID # 1-based sample number
LINE=`sed "$((SAMPLE_NUM+1))q;d" $SMF_BAM_LIST_FILE`
SMF=`echo $LINE | awk '{print $1}'`
BAM_FILE=`echo $LINE | awk '{print $2}'`
BAM_INDEX=${BAM_FILE}.bai
TEMP_DIR=/scratch/`whoami`/tmpdir.${JOB_ID}.${ID}
mkdir -p $TEMP_DIR
cp $BAM_FILE -t $TEMP_DIR
checkForError $LINENO $?
cp $BAM_INDEX -t $TEMP_DIR
checkForError $LINENO $?
BAM_FILE=${TEMP_DIR}/`basename $BAM_FILE`
BAM_INDEX=${TEMP_DIR}/`basename $BAM_INDEX`

NUM_CPU_THREADS_PER_DATA_THREAD=1
NUM_DATA_THREADS=@NUM_CORES@
GATK_PARAMS="-nct $NUM_CPU_THREADS_PER_DATA_THREAD -nt $NUM_DATA_THREADS -R $REFERENCE"
GENOTYPE_LIKELIHOOD_MODEL=BOTH

for i in `seq 1 $NUM_CONTIGS`; do
    if [ "$NUM_CONTIGS" -eq "1" ] && [ ! -f ${ALLELES_DIR}/1_${ALLELES_FILENAME}.vcf ] && [ -f ${ALLELES_DIR}/${ALLELES_FILENAME}.vcf ]; then
        ALLELES_FILE=${ALLELES_DIR}/${ALLELES_FILENAME}.vcf
    else
        ALLELES_FILE=${ALLELES_DIR}/${i}_${ALLELES_FILENAME}.vcf
    fi
    cp $ALLELES_FILE -t $TEMP_DIR
    checkForError $LINENO $?
    ALLELES_FILE=${TEMP_DIR}/`basename $ALLELES_FILE`
    OUTPUT_FILE=${TEMP_DIR}/${i}_recallVariants_${SMF}.vcf
    CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T UnifiedGenotyper -gt_mode GENOTYPE_GIVEN_ALLELES \
        -L $ALLELES_FILE --alleles $ALLELES_FILE -glm $GENOTYPE_LIKELIHOOD_MODEL \
        -o $OUTPUT_FILE \
        -out_mode EMIT_ALL_SITES \
        -I $BAM_FILE --filter_reads_with_N_cigar"
    echo ${CMD}
    $CMD
    checkForError $LINENO $?

    mv $OUTPUT_FILE -t .
    checkForError $LINENO $?
    mv ${OUTPUT_FILE}.idx -t .
    checkForError $LINENO $?
    rm $ALLELES_FILE
    checkForError $LINENO $?
    rm ${ALLELES_FILE}.idx
    checkForError $LINENO $?
done

rm -r $TEMP_DIR
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total RecallVariants Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO