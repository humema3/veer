#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M maxwell.hume@novartis.com
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-24
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=@MAX_RUNTIME@
#$ -N @JOB_NAME@

source ../ponc_profile.sh

stime=$(date '+%s')

VCF_LIST_FILE=@VCF_LIST_FILE@

ID=$SGE_TASK_ID

###############################################################

### Make VCF List ##
exec 10<"${VCF_LIST_FILE}"
let count=0
VARIANT_FILES=""
while read -u 10 LINE; do
    ((count++))
    if [ $count -gt 1 ] ; then
       VCF=`echo ${LINE} | awk '{print $1}'`
       VARIANT_FILES=${VARIANT_FILES}" -V ${VCF} "
    fi
done

GATK_PARAMS="--suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -L ${CONTIG} -R ${REFERENCE} --disable_auto_index_creation_and_locking_when_reading_rods"
CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o ./${ID}_combineVariants.vcf ${VARIANT_FILES}"

echo $CMD
$CMD

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total CombineVariants Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO
