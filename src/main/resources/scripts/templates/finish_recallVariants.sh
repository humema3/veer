#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M maxwell.hume@novartis.com
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-@NUM_CONTIGS@
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=172800
#$ -hold_jid @PARENT_JOB@

##############################################################################
# After running RecallVariants we combine all the results from individual
# samples (for each chromosome or other genome division) into one VCF.
##############################################################################

source ../ponc_profile.sh

stime=$(date '+%s')

ID=$SGE_TASK_ID
VARIANT_FILES=""
for file in $(ls ./${ID}_recallVariants_*-*.vcf); do
    if [[ -s $file ]]; then
        VARIANT_FILES+="-V $file "
    else
        echo "${ID}_recallVariants_*-*.vcf is empty or nonexistent"
        echo "Make sure all files are present, then rerun finish_recallVariants.sh"
        exit 1
    fi
done

NUM_THREADS=1
GATK_PARAMS="--suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -R ${REFERENCE} -nt $NUM_THREADS --disable_auto_index_creation_and_locking_when_reading_rods"

CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o ./${ID}_recallVariants.vcf ${VARIANT_FILES}"
echo $CMD
$CMD

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_recallVariants run time: %d:%02d:%02d\n' $dh $dm $ds
