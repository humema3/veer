#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M @EMAIL@
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-@NUM_FILES@
#$ -j y
#$ -N @JOB_NAME@

source ../ponc_profile.sh

stime=$(date '+%s')

if [ ! -z $SGE_TASK_ID ]; then
    ID=$SGE_TASK_ID
else
    if [ "$NUM_FILES" -ne "1" ]; then
        echo "No SGE parallelization detected. Therefore, the NUM_FILES variable must be set to 1. \
        To run consecutively multiple times, rerun changing the INPUT_FILENAME variable each time."
        exit 1
    fi
    ID=1
fi
INPUT_DIR=@INPUT_DIR@
INPUT_FILENAME=@INPUT_FILENAME@
NUM_FILES=@NUM_FILES@
ANNOTATE_SCORES=@ANNOTATE_SCORES@
COUNTS_RESOURCE=@COUNTS_RESOURCE@
LOCKFILE=@LOCKFILE@

if [ ! -z $INPUT_FILENAME ] && [ "$NUM_FILES" -eq "1" ] && [ ! -f ${INPUT_DIR}/1_${INPUT_FILENAME}.vcf ] && [ -f ${INPUT_DIR}/${INPUT_FILENAME}.vcf ]; then
    INPUT_VCF=${INPUT_DIR}/${INPUT_FILENAME}.vcf
elif [ ! -z $INPUT_FILENAME ]; then
    INPUT_VCF="${INPUT_DIR}/${ID}_${INPUT_FILENAME}.vcf"
else
    INPUT_VCF=`ls $INPUT_DIR/*.vcf | head -n $ID | tail -n 1`
fi
INPUT_INDEX=${INPUT_VCF}.idx

TEMP_DIR=/scratch/`whoami`/tmpdir.${JOB_ID}.${ID}
mkdir -p $TEMP_DIR
checkForError $LINENO $?
cp $INPUT_VCF -t $TEMP_DIR
checkForError $LINENO $?
INPUT_VCF=${TEMP_DIR}/`basename $INPUT_VCF`
if [ -f $INPUT_INDEX ]; then
  cp $INPUT_INDEX -t $TEMP_DIR
  checkForError $LINENO $?
fi

if [ ! -z $COUNTS_RESOURCE ]; then
    RESOURCE_INDEX=${COUNTS_RESOURCE}.idx
    LOCKFILE=/scratch/`whoami`/ponc.lock
    lockfile $LOCKFILE
    checkForError $LINENO $?
    if [ ! -f `dirname $TEMP_DIR`/`basename $COUNTS_RESOURCE` ]; then
      cp $COUNTS_RESOURCE -t `dirname $TEMP_DIR`
      checkForError $LINENO $?
      if [ -f $RESOURCE_INDEX ] && [ ! -f `dirname $TEMP_DIR`/`basename $RESOURCE_INDEX` ]; then
        cp $RESOURCE_INDEX -t `dirname $TEMP_DIR`
        checkForError $LINENO $?
      fi
    fi
    rm -f $LOCKFILE
    checkForError $LINENO $?
    COUNTS_RESOURCE=`dirname ${TEMP_DIR}`/`basename $COUNTS_RESOURCE`
fi

VCF_FILENAME="${INPUT_VCF%.*}"
OUTPUT_VCF=${VCF_FILENAME}_annotateWithVEER.vcf
OUTPUT_INDEX=${OUTPUT_VCF}.idx
CMD="$PONCOTATOR -o $OUTPUT_VCF -r $REFERENCE -vcf $INPUT_VCF"
if [ ! -z $COUNTS_RESOURCE ]; then
    CMD="$CMD -a Resource:VeerCountDB:VEERCounts -resource VeerCountDB:${COUNTS_RESOURCE}"
else
    CMD="$CMD -a VEERCounts"
fi
if [ "$ANNOTATE_SCORES" -eq "1" ]; then
    CMD="$CMD -a VS"
fi
echo $CMD
$CMD
checkForError $LINENO $?

mv $OUTPUT_VCF -t .
checkForError $LINENO $?
if [ -f $OUTPUT_INDEX ]; then
  mv $OUTPUT_INDEX -t .
  checkForError $LINENO $?
fi

rm -r $TEMP_DIR
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total AnnotateWithVEER action run time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO