#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M maxwell.hume@novartis.com
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-1
#$ -j y
#$ -l m_mem_free=1G
#$ -l h_rt=1800
#$ -hold_jid @PARENT_JOB@

##############################################################################
# After running CombineVariants we combine all the results from the individual
# chromosomes into one VCF.
##############################################################################

source ../ponc_profile.sh

stime=$(date '+%s')

egrep "^#" 1_combineVariants.vcf > combineVariants.vcf # take header lines once
# For now make this the same as num chr but if use scattered intervals could change this.
NUM_OF_NODES=24
for i in $(seq 1 $NUM_OF_NODES); do
   VCF=${i}_combineVariants.vcf

   if [[ -s $VCF ]]; then
        echo "Merging $VCF"
        grep -v "^#" $VCF >> combineVariants.vcf
   else
        echo "Ignore empty file: $VCF"
   fi
done

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_combineVariants run time: %d:%02d:%02d\n' $dh $dm $ds