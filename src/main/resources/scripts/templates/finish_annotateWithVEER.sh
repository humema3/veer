#!/bin/bash
#$ -S /bin/bash
#$ -m beas
#$ -M @EMAIL@
#$ -wd @OUTPATH_ABSOLUTEPATH@
#$ -t 1-1
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=43200
#$ -hold_jid @PARENT_JOB@

##############################################################################
# After running AnnotateWithVEER we combine all the results from the individual
# chromosomes into one VCF.
##############################################################################

source ../ponc_profile.sh

stime=$(date '+%s')

RUN_COMBINE_VARIANTS=@RUN_COMBINE_VARIANTS@
NUM_FILES=@NUM_FILES@
VARIANT_FILES=""
if [ "$RUN_COMBINE_VARIANTS" -eq "1" ] && [ "$NUM_FILES" -gt "1" ]; then
    for i in `seq 1 $NUM_FILES`; do
        VARIANT_FILES+="-V ./${i}_annotateWithVEER.vcf "
    done
    NUM_THREADS=1
    GATK_PARAMS="-R ${REFERENCE} --suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -nt $NUM_THREADS --disable_auto_index_creation_and_locking_when_reading_rods"
    CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o ./annotateWithVEER.vcf --assumeIdenticalSamples ${VARIANT_FILES}"
    echo $CMD
    $CMD
fi
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_annotateWithVEER run time: %d:%02d:%02d\n' $dh $dm $ds