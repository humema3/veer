package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.io.File;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.tribble.index.IndexFactory;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.tribble.index.interval.IntervalTreeIndex;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFCodec;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFFileReader;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.Allele;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 *
 * Created by jonesmic on 7/8/2014.
 */
public class VCFVariantResource extends VariantResource {
    private final static Logger logger = Logger.getLogger(VCFVariantResource.class);

    private final VCFFileReader reader;
    private CloseableIterator<VariantContext> variantContextIterator = null;

    boolean isChrContig;
    boolean printChrFix = true;

    public VCFVariantResource(final String resourceName, final File location, final QueryType queryType) throws IOException {
        super(resourceName, location, queryType);
        checkIndex(location);
        this.reader = new VCFFileReader(location, true);
        if(queryType.equals(QueryType.Iterative)){
            this.variantContextIterator = reader.iterator();
            this.currentVariantContext = variantContextIterator.next();
        }
        validate();
    }

    private void checkIndex(File vcf) throws IOException {
        File indexFile = new File(vcf + ".idx");
        if(!indexFile.exists()){
            logger.info("No INDEX " + indexFile + " exists. Attempting to create");
            IntervalTreeIndex idx = IndexFactory.createIntervalIndex(vcf, new VCFCodec());
            idx.writeBasedOnFeatureFile(vcf);
        }
    }

    private void validate() {
        CloseableIterator<VariantContext> it = iterator();
        it.hasNext();
        VariantContext variantContext = it.next();
        isChrContig=variantContext.getChr().startsWith("chr");
        it.close();
    }

    @Override
    public String query(String chr, int start, int end, Allele refAllele, Allele altAllele, String infoID, boolean altAlleleSpecific) {
        if(chr.startsWith("chr") && !isChrContig){
            chr = chr.replace("chr", "");
            if(printChrFix) {
                logger.info("Striped 'chr' off of contig while searching " + getResourceName());
                printChrFix=false;
            }
        }

        if(getQueryType().equals(QueryType.Index)) {
            return doIndexQuery(chr, start, end, altAllele, refAllele, infoID, altAlleleSpecific);
        }else if(getQueryType().equals(QueryType.Iterative)){
            return doIterativeQuery(chr, start, end, refAllele, altAllele, infoID, altAlleleSpecific);
        }else{
            throw new UnsupportedOperationException("No Query type " + getQueryType());
        }
    }

    private VariantContext currentVariantContext;

    private String doIterativeQuery(String chr, int start, int end, Allele refAllele, Allele altAllele, String infoID, boolean altAlleleSpecific) {
        Assert.notNull(variantContextIterator);
        Set<Allele> queryAltAlleleSet = ImmutableSet.of(altAllele);

        do {
            if (!currentVariantContext.getChr().equals(chr) ||
                    currentVariantContext.getStart() < start) {
                continue;
            }
            if (currentVariantContext.getStart() == start) {
                if (currentVariantContext.getReference().equals(refAllele) && currentVariantContext.hasAttribute(infoID)) {
                    if (altAlleleSpecific) {
                        final List<Allele> altAlleles = currentVariantContext.getAlternateAlleles();
                        for (int i = 0; i < altAlleles.size(); i++) {
                            if (altAlleles.get(i).equals(altAllele)) {
                                final Object value = currentVariantContext.getAttribute(infoID);
                                final String valueStr = format(value);
                                return (valueStr.split(","))[i];
                            }
                        }
                    } else {
                        final Set<Allele> altAlleles = Sets.newHashSet(currentVariantContext.getAlternateAlleles());
                        if (!Sets.intersection(queryAltAlleleSet, altAlleles).isEmpty()) {
                            final Object value = currentVariantContext.getAttribute(infoID);
                            return format(value);
                        }
                    }
                }
            } else if (currentVariantContext.getStart() > end) {
                break;
            }
        } while (variantContextIterator.hasNext());
        return "";
    }

    private String doIndexQuery(String chr, int start, int end, Allele altAllele, Allele refAllele, String infoID, boolean altAlleleSpecific) {
        final CloseableIterator<VariantContext> matchingVCIt = reader.query(chr, start, end);
        final Set<Allele> queryAltAlleleSet = ImmutableSet.of(altAllele);
        while (matchingVCIt.hasNext()) {
            final VariantContext variantContext = matchingVCIt.next();
            if (variantContext.getReference().equals(refAllele) && variantContext.hasAttribute(infoID)) {
                if (altAlleleSpecific) {
                    final List<Allele> altAlleles = variantContext.getAlternateAlleles();
                    for (int i = 0; i < altAlleles.size(); i++) {
                        if (altAlleles.get(i).equals(altAllele)) {
                            final Object value = variantContext.getAttribute(infoID);
                            final String valueStr = format(value);
                            return (valueStr.split(","))[i];
                        }
                    }
                } else {
                    final Set<Allele> altAlleles = Sets.newHashSet(variantContext.getAlternateAlleles());
                    if (!Sets.intersection(queryAltAlleleSet, altAlleles).isEmpty()) {
                        final Object value = variantContext.getAttribute(infoID);
                        return format(value);
                    }
                }
            }
        }
        matchingVCIt.close();
        return "";
    }

    private String format(Object value) {
        if(value instanceof String || value instanceof Boolean){
            return value.toString();
        }else if(value instanceof List){
            return value.toString().replaceAll("[\\[\\]\\s]", "");
        }else{
            throw new IllegalArgumentException("Don't know how to parse " + value + " of type " + value.getClass().getSimpleName());
        }
    }

    @Override
    public boolean hasInfoLine(String infoField) {
        return  reader.getFileHeader().hasInfoLine(infoField);
    }

    @Override
    public VCFInfoHeaderLine getInfoHeaderLine(String infoField) {
        return reader.getFileHeader().getInfoHeaderLine(infoField);
    }

    @Override
    public CloseableIterator<VariantContext> iterator() {
        return reader.iterator();
    }
}
