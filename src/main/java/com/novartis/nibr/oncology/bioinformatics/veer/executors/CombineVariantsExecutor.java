package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.novartis.nibr.oncology.bioinformatics.veer.actions.CombineVariants;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 *
 * Created by JONESMIC on 4/24/2014.
 */
@Component
//@ActionType("PoncTools")
public class CombineVariantsExecutor extends VEERActionExecutor {

//    private static final int DEFAULT_MAX_RUNTIME = 345599;

    private final CombineVariants combineVariants;

    @Autowired
    public CombineVariantsExecutor(CombineVariants combineVariants) {
        this.combineVariants = combineVariants;
    }

//    @Override
//    protected Options getOptions() {
//        final Options options = super.getOptions();
//        return options;
//    }
//
    @Override
    public void build(final String[] args) throws PipelineException {
        super.build(args);
        combineVariants.setSparkContext(getSparkContext());
    }
//        final Options options = getOptions();
//        final CommandLineParser parser = new PosixParser();
//
////        options.addOption("type", true, "The action type");
////        options.addOption("o", true, "The directory for analysis output");
////        options.addOption("v", "vcf_list_file", true, "The reference file list the VCFs. Must have a column with the header VCF_URI");
////        options.getOption("v").setRequired(true);
////        options.addOption("r", true, "The reference FastA");
////        options.addOption("t", true, "Max running time in seconds (default is " + DEFAULT_MAX_RUNTIME + ")");
//        final CommandLine cmd = parser.parse(options, args);
////        final String[] required = {"type", "o", "r", "v"};
////        checkArgs(cmd, required, options);
////        checkToolArg(cmd, options);
//
////        final File outPath = new File(cmd.getOptionValue("o"));
////        checkOutputPath(outPath);
////        final File vcfReference = new File(cmd.getOptionValue("v"));
////        final File reference = new File(cmd.getOptionValue("r"));
////        final int maxRunTime = cmd.hasOption("t") ? Integer.parseInt(cmd.getOptionValue("t")) : DEFAULT_MAX_RUNTIME;
//
////        combineVariants.setOutputDirPath(outPath);
////        combineVariants.setReference(reference);
////        combineVariants.setRefVcfListFile(vcfReference);
////        combineVariants.setMaxRunTime(maxRunTime);
////        printArgs(args, combineVariants.getResultsDir(), options, getActionType().name());
////        combineVariants.promptForSubmit();
//    }
//
//    @Override
//    public void start() throws PipelineException {
//        combineVariants.start();
//    }

    @Override
    protected VEERPipelineAction getAction() {
        return combineVariants;
    }

    @Override
    public void start() throws PipelineException {
        combineVariants.start();
    }
}
