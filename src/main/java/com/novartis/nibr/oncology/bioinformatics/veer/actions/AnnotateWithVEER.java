package com.novartis.nibr.oncology.bioinformatics.veer.actions;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.io.File;

import com.google.common.collect.Sets;
import com.novartis.nibr.oncology.bioinformatics.poncotator.PoncotatorActionExecutor;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERCountAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERScoreAnnotator;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import htsjdk.variant.vcf.VCFHeaderLine;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by humema3 on 10/21/2015.
 */
@Component
public class AnnotateWithVEER extends VEERPipelineAction {

//    private int numFiles;
//    private File inputDir;
//    private Optional<String> inputFilename;
//    private String outputFilename;
//    private boolean annotateScores;
//    private Optional<String> countsResource;
//    private boolean runCombineVariants;
//    private String sparkMaster;
    private String[] annotators = new String[0];
    private Optional<String> resource = Optional.absent();
//    private JavaSparkContext sparkContext;

//    @Autowired
//    protected AnnotateWithVEER(final RunInfoFactory runInfoFactory) {
//        super(VEERPipelineAction.PipelineActionType.AnnotateWithVEER, runInfoFactory);
//    }

    public AnnotateWithVEER() {
        super(PipelineActionType.AnnotateWithVEER);
    }

//    @Override
//    protected boolean runInfoRequired() {
//        return false;
//    }

    @Autowired
    private PoncotatorActionExecutor poncotator;

//    @Override
//    protected void checkFileSystem() throws PipelineException {
//        super.checkFileSystem();
//
//        final Optional<String> inputFilename = getInputFilename();
//        final int numFiles = getNumFiles();
//        File[] inputFiles;
//        if (inputFilename.isPresent()) {
//            inputFiles = inputDir.listFiles(new FilenameFilter() {
//                public boolean accept(File dir, String name) {
//                    final Pattern p = Pattern.compile("(\\d+)_" + inputFilename.get() + "\\.vcf");
//                    final Matcher m = p.matcher(name);
//                    if (!m.matches()) {
//                        return false;
//                    }
//                    final int contigNum = Integer.parseInt(m.group(1));
//                    return contigNum >= 1 && contigNum <= numFiles;
//                }
//            });
//            if (inputFiles.length == 0 && numFiles == 1) {
//                inputFiles = inputDir.listFiles(new FilenameFilter() {
//                    public boolean accept(File dir, String name) {
//                        final Pattern p = Pattern.compile(getInputFilename() + "\\.vcf");
//                        final Matcher m = p.matcher(name);
//                        return m.matches();
//                    }
//                });
//            }
//            Assert.isTrue(inputFiles.length == numFiles,
//                    "Number of input files found in directory " + inputDir.getAbsolutePath() + " with file suffix " + inputFilename.get() + " is " + inputFiles.length + ", should be " + numFiles);
//        } else {
//            inputFiles = inputDir.listFiles(new FilenameFilter() {
//                public boolean accept(File dir, String name) {
//                    final Pattern p = Pattern.compile(".+\\.vcf");
//                    final Matcher m = p.matcher(name);
//                    return m.matches();
//                }
//            });
//            final int numVcfFilesFound = inputFiles.length;
//            Assert.isTrue(numVcfFilesFound > 0, "No VCF files found in input directory");
//            Assert.isTrue(numVcfFilesFound <= numFiles, numVcfFilesFound + " VCF files found in input directory, but " + numFiles + " input files specified");
//            setNumFiles(Optional.of(numVcfFilesFound));
//        }
//    }

    @Override
    public void execute() throws PipelineException {
        final List<String> inputVcfFilepaths = getInputVcfFilepaths();
        final List<String> outputVcfFiles = Lists.newArrayList();
        final String outputDirPath = getOutputDirPath();
        final String outputFilename = getOutputFilename();
        for (final String filepath : inputVcfFilepaths) {
            final String filename = FilenameUtils.removeExtension(filepath);
            outputVcfFiles.add(Paths.get(outputDirPath, FilenameUtils.getBaseName(filename) + "_" + outputFilename + ".vcf").toString());
        }
        poncotator.setVcfFilepaths(inputVcfFilepaths);
        poncotator.setOutputFilepaths(outputVcfFiles);
        poncotator.setSparkContext(getSparkContext());
        final Optional<Integer> numPartitions = getNumPartitions();
        if (numPartitions.isPresent()) {
            poncotator.setNumPartitions(numPartitions.get());
        }
        poncotator.setReferenceGenomeFilepath(getReferenceGenomeFilepath());
        final Collection<VCFHeaderLine> newHeaderLines = new HashSet<VCFHeaderLine>();
        newHeaderLines.add(VEERCountAnnotator.INFO_HEADER_LINE);
        newHeaderLines.add(VEERScoreAnnotator.VCF_FORMAT_HEADER_LINE);
        poncotator.setNewHeaderLines(newHeaderLines);
        final Optional<String> resource = getResource();
        try {
            poncotator.processAnnotations(getAnnotators(), resource.isPresent() ? new String[]{resource.get()} : new String[0]);
        } catch (final IOException e)  {
            throw new PipelineException("Error processing annotation request", e);
        }
        poncotator.build(new String[0]);
        poncotator.start();
    }

//    @Override
//    public List<File> getInputVcfFilepaths() throws PipelineException {
//        final List<File> inputVcfFiles = Lists.newArrayList();
//        final int numFiles = getNumFiles();
//        for (int i = 1; i <= numFiles; i++) {
//            inputVcfFiles.add(getInputVcf(i));
//        }
//        return inputVcfFiles;
//    }
//
//    private File getInputVcf(final int fileNum) throws PipelineException {
//        final Optional<String> inputFilename = getInputFilename();
//        final int numFiles = getNumFiles();
//        final File inputDir = getInputDir();
//        if (inputFilename.isPresent() && numFiles == 1 && !(Paths.get(inputDir.getAbsolutePath(), "1_" + inputFilename.get() + ".vcf").toFile().isFile())
//            && (Paths.get(inputDir.getAbsolutePath(), inputFilename.get() + ".vcf").toFile().isFile())) {
//            return Paths.get(inputDir.getAbsolutePath(), inputFilename.get() + ".vcf").toFile();
//        } else if (inputFilename.isPresent()) {
//            return Paths.get(inputDir.getAbsolutePath(), fileNum + "_" + inputFilename.get() + ".vcf").toFile();
//        } else {
//            final File[] vcfFiles = inputDir.listFiles(new FilenameFilter() {
//                public boolean accept(final File dir, final String name) {
//                    return name.endsWith(".vcf");
//                }
//            });
//            try {
//                return vcfFiles[fileNum];
//            } catch (final ArrayIndexOutOfBoundsException e) {
//                throw new PipelineException("Only " + vcfFiles.length + " files found, at least " + fileNum + " requested ", e);
//            }
//        }
//    }



//    public int getNumFiles() {
//        return numFiles;
//    }
//
//    public void setNumFiles(final int numFiles) {
//        this.numFiles = numFiles;
//    }

//    public File getInputDir() {
//        return inputDir;
//    }
//
//    public void setInputDir(final File inputDir) {
//        this.inputDir = inputDir;
//    }

//    public Optional<String> getInputFilename() {
//        return inputFilename;
//    }
//
//    public void setInputFilename(final Optional<String> inputFilename) {
//        this.inputFilename = inputFilename;
//    }
//
//    public String getOutputFilename() {
//        return outputFilename;
//    }
//
//    public void setOutputFilename(final String filename) {
//        outputFilename = filename;
//    }

//    public boolean annotateScores() {
//        return annotateScores;
//    }
//
//    public void setAnnotateScores(final boolean annotateScores) {
//        this.annotateScores = annotateScores;
//    }
//
//    public Optional<String> getCountsResource() {
//        return countsResource;
//    }
//
//    public void setCountsResource(final Optional<String> countsResource) {
//        this.countsResource = countsResource;
//    }

//    public boolean isRunCombineVariants() {
//        return runCombineVariants;
//    }
//
//    public void setRunCombineVariants(final boolean runCombineVariants) {
//        this.runCombineVariants = runCombineVariants;
//    }

//    public String getSparkMaster() {
//        return sparkMaster;
//    }
//
//    public void setSparkMaster(final String sparkMaster) {
//        this.sparkMaster = sparkMaster;
//    }

    public String[] getAnnotators() {
        return annotators;
    }

    public void setAnnotators(final String[] annotators) {
        this.annotators = annotators;
    }

    public Optional<String> getResource() {
        return resource;
    }

    public void setResource(final Optional<String> resource) {
        this.resource = resource;
    }

    public void setResource(final String resource) {
        this.resource = Optional.<String>of(resource);
    }

//    public JavaSparkContext getSparkContext() {
//        return sparkContext;
//    }
//
//    public void setSparkContext(final JavaSparkContext sparkContext) {
//        this.sparkContext = sparkContext;
//    }
}
