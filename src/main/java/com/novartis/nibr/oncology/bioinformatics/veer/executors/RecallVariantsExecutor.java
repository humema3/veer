package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.RecallVariants;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.File;

/**
 * Created by humema3 on 10/22/2015.
 */
@Component
public class RecallVariantsExecutor extends VEERActionExecutor {

//    private static final int DEFAULT_MAX_RUNTIME = 345599;
//    private static final int DEFAULT_NUM_CORES = 1;
//    private static final int DEFAULT_GIGS_MEM = 6;

    private final RecallVariants recallVariants;

    @Autowired
    public RecallVariantsExecutor(final RecallVariants recallVariants) {
        this.recallVariants = recallVariants;
    }

    @Override
    protected Options getOptions() {
        final Options options = super.getOptions();
//        options.addOption("type", true, "The action type");
//        options.addOption("o", true, "The directory for analysis output");
//        options.addOption("r", true, "The reference FastA");
        options.addOption("b", "bam_sample_file", true, "The BAM/sample reference file. First column is sample name and second column is BAM file location, separated by a tab");
        options.getOption("b").setRequired(true);
//        options.addOption("d", "allele_vcf_dir", true, "The directory in which the allele VCF files are stored");
//        options.getOption("d").setRequired(true);
//        options.addOption("n", true, "The number of contigs by which to parallelize. Allele VCF files should start with an integer from 1 to this number (see -f argument).");
//        options.getOption("n").setRequired(true);
//        options.addOption("f", true, "The filename for all the allele VCF files. Each file should be called [i]_[filename].vcf, where [i] is an integer between " +
//                "1 and the '-n' argument, and [filename] is this argument. NOTE: Variants with filter calls besides '.' and 'PASS' will not come up in the output!");
//        options.getOption("f").setRequired(true);
//        options.addOption("t", true, "Max running time in seconds (default is " + DEFAULT_MAX_RUNTIME + ")");
//        options.addOption("c", true, "Number of cores to use per task (default is " + DEFAULT_NUM_CORES + ")");
//        options.addOption("gigs_mem", true, "Number of gigs of memory to use (default is " + DEFAULT_GIGS_MEM + ")");
        return options;
    }

    @Override
    public void build(final String[] args) throws PipelineException {
        try {
            super.build(args);
        } catch (final PipelineException e) {
            throwIfUnrecognized(e);
        }
        final Options options = getOptions();
//        options.addOption("type", true, "The action type");
//        options.addOption("o", true, "The directory for analysis output");
//        options.addOption("r", true, "The reference FastA");
//        options.addOption("b", "bam_sample_file", true, "The BAM/sample reference file. Must have columns with the header CONCEPT_SMFID and BAM_URI"); //TODO SMF ID is NIBR-specific
//        options.getOption("b").setRequired(true);
//        options.addOption("d", "allele_vcf_dir", true, "The directory in which the allele VCF files are stored");
//        options.getOption("d").setRequired(true);
//        options.addOption("n", true, "The number of contigs by which to parallelize. Allele VCF files should start with an integer from 1 to this number (see -f argument).");
//        options.getOption("n").setRequired(true);
//        options.addOption("f", true, "The filename for all the allele VCF files. Each file should be called [i]_[filename].vcf, where [i] is an integer between " +
//                "1 and the '-n' argument, and [filename] is this argument. NOTE: Variants with filter calls besides '.' and 'PASS' will not come up in the output!");
//        options.getOption("f").setRequired(true);
//        options.addOption("t", true, "Max running time in seconds (default is " + DEFAULT_MAX_RUNTIME + ")");
//        options.addOption("c", true, "Number of cores to use per task (default is " + DEFAULT_NUM_CORES + ")");
//        options.addOption("gigs_mem", true, "Number of gigs of memory to use (default is " + DEFAULT_GIGS_MEM + ")");

        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd = processArgs(parser, options, args);
//        final String[] required = { "type", "o", "r", "b", "d", "n", "f" };
//        checkArgs(cmd, required, options);

//        final File outPath = new File(cmd.getOptionValue("o"));
//        checkOutputPath(outPath);
        final File bamReference = new File(cmd.getOptionValue("b"));
//        final File genomeReference = new File(cmd.getOptionValue("r"));
//        final int numContigs = Integer.parseInt(cmd.getOptionValue("n"));
//        final File alleleVcfDir = new File(cmd.getOptionValue("d"));
//        final String alleleVcfFilename = cmd.getOptionValue("f");
//        final int maxRunTime = cmd.hasOption("t") ? Integer.parseInt(cmd.getOptionValue("t")) : DEFAULT_MAX_RUNTIME;
//        final int numCores = cmd.hasOption("c") ? Integer.parseInt(cmd.getOptionValue("c")) : DEFAULT_NUM_CORES;
//        final int gigsMem = cmd.hasOption("g") ? Integer.parseInt(cmd.getOptionValue("gigs_mem")) : DEFAULT_GIGS_MEM;

//        recallVariants.setOutputDirPath(outPath);
//        recallVariants.setReferenceGenomeFilepath(genomeReference);
        recallVariants.setRefBamListFile(bamReference);
//        recallVariants.setNumContigs(numContigs);
//        recallVariants.setAlleleVcfDir(alleleVcfDir);
//        recallVariants.setAlleleVcfFilename(alleleVcfFilename);
        recallVariants.setSparkContext(getSparkContext());
//        recallVariants.setMaxRunTime(maxRunTime);
//        recallVariants.setNumCores(numCores);
//        recallVariants.setNumGigsMem(gigsMem);
//        recallVariants.start();
//        printArgs(args, recallVariants.getResultsDir(), options, getActionType().name());
//        recallVariants.promptForSubmit();
    }

    @Override
    protected VEERPipelineAction getAction() {
        return recallVariants;
    }

//    @Override
//    protected VEERPipelineAction getAction() {
//        return recallVariants;
//    }

//    @Override
//    public void start() throws PipelineException {
//        recallVariants.start();
//    }
}
