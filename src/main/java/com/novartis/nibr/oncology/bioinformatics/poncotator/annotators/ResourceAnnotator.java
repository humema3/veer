package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import com.novartis.nibr.oncology.bioinformatics.poncotator.io.VariantResource;
import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

/**
 *
 * Created by jonesmic on 7/7/2014.
 */
@Component
@Scope("prototype")
public class ResourceAnnotator extends InfoAnnotator {
    private final static Logger logger = Logger.getLogger(ResourceAnnotator.class);

    private VariantResource variantResource;
    private VCFInfoHeaderLine sourceInfoHeaderLine;

    @Override
    protected AnnotatorValueHolder buildAnnotationValue(VariantContext queryVariantContext) {
        final Allele refAllele = queryVariantContext.getReference();
        final List<Allele> altAlleles = queryVariantContext.getAlternateAlleles();
        Assert.notEmpty(altAlleles);

        final StringBuilder sb = new StringBuilder();
        for (final Allele altAllele : altAlleles) {
            final String value = variantResource.query(
                    queryVariantContext.getChr(),
                    queryVariantContext.getStart(),
                    queryVariantContext.getEnd(), refAllele,
                    altAllele, sourceInfoHeaderLine.getID(),
                    sourceInfoHeaderLine.getCountType() == VCFHeaderLineCount.A);
            sb.append(value + ",");
        }
        sb.setLength(sb.length()-1);

        return new AnnotatorValueHolder(sb.toString());
    }

    @Override
    public VCFInfoHeaderLine getVCFInfoHeaderLine() {

        VCFInfoHeaderLine vcfInfoHeaderLine=null;
        if(sourceInfoHeaderLine!=null) {
            String id = variantResource.getResourceName() + "." + sourceInfoHeaderLine.getID();
            String desc = sourceInfoHeaderLine.getDescription();

            VCFHeaderLineType type = sourceInfoHeaderLine.getType();

            if (sourceInfoHeaderLine.getCountType() == VCFHeaderLineCount.INTEGER) {
                int count = sourceInfoHeaderLine.getCount();
                vcfInfoHeaderLine = new VCFInfoHeaderLine(id, count, type, desc);
            } else {
                VCFHeaderLineCount countEnum = sourceInfoHeaderLine.getCountType();
                vcfInfoHeaderLine = new VCFInfoHeaderLine(id, countEnum, type, desc);
            }
        }else{
            String id = "Resource";
            String desc = "Resource";
            VCFHeaderLineType type = VCFHeaderLineType.String;
            vcfInfoHeaderLine = new VCFInfoHeaderLine(id, VCFHeaderLineCount.UNBOUNDED, type, desc);
        }

        return vcfInfoHeaderLine;
    }

    /**
     *
     * @param value representing the attribute under investigation
     * @return if the parser can support this attribute
     */
    private boolean isSupportedAttribute(Object value) {
        return value instanceof String || value instanceof Boolean;
    }

    public void setSourceInfoHeaderLine(VCFInfoHeaderLine sourceInfoHeaderLine) {
        this.sourceInfoHeaderLine = sourceInfoHeaderLine;
    }

    public void setVariantResource(VariantResource variantResource) {
        this.variantResource = variantResource;
    }
}
