package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERCountAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERScoreAnnotator;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Created by humema3 on 1/23/17.
 */
@Component
public class AnnotateVCFsExecutor extends VEERActionExecutor {
    @Autowired
    private AnnotateWithVEERExecutor annotator;

    private File resourceVcf;

    @Override
    protected VEERPipelineAction getAction() {
        return annotator.getAction();
    }

    @Override
    public void start() throws PipelineException {
        annotator.start();
    }

    @Override
    protected Options getOptions() {
        final Options options = annotator.getOptions();
        // duplicated from AnnotateWithVEER for usage
        options.addOption("res", "counts_resource", true, "Counts resource VCF. Default is to use input VCF file.");
        options.getOption("res").setRequired(false);
        return options;
    }

    //TODO
    @Override
    public void build(final String[] args) throws PipelineException {
//        super.build(args);
//        final Options options = getOptions();
//        final CommandLineParser parser = new PosixParser();
        try {
            annotator.build(args);
        } catch (final PipelineException e) {
            throwIfUnrecognized(e);
        }
//        final CommandLine cmd;
//        try {
//            cmd = parser.parse(options, args);
//        } catch (final ParseException e) {
//            throw new PipelineException("Error parsing command-line args", e);
//        }

//        Assert.isTrue(annotator.getCountsResource().isPresent());
        final String countsFieldId = VEERCountAnnotator.ID;
        annotator.setAnnotations(new String[]{ "Resource:VeerCountDB:"+countsFieldId, VEERScoreAnnotator.ID});
        annotator.setResource("VeerCountDB:"+resourceVcf.getAbsolutePath()+":"+countsFieldId);
    }

    public File getResourceVcf() {
        return resourceVcf;
    }

    public void setResourceVcf(final File resourceVcf) {
        this.resourceVcf = resourceVcf;
    }

    @Override
    public VEERPipelineAction.PipelineActionType getActionType() {
        return VEERPipelineAction.PipelineActionType.AnnotateVCFs;
    }
}
