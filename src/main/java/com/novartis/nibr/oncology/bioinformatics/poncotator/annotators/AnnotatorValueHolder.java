package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Holds the value and any errors or warnings
 * Created by jonesmic on 11/6/2014.
 */
public class AnnotatorValueHolder {

    private final String value;
    private Set<AnnotWarning.ID> warnings;

    public AnnotatorValueHolder(String value) {
        this.value = value;
        warnings = Sets.newHashSet();
    }

    public void addWarnings(Set<AnnotWarning.ID> warnings){
        this.warnings.addAll(warnings);
    }

    public String getValue() {
        return value;
    }

    public boolean hasWarnings() {
        return !warnings.isEmpty();
    }

    public boolean hasMessages() {
        return hasWarnings();
    }

    public String getMessageString(){
        Set<AnnotWarning.ID> messages = Sets.newHashSet();
        messages.addAll(warnings);
        String value = messages.toString();
        return value.substring(1, value.length() - 1).replaceAll("\\s", "");
    }

    public Set<AnnotWarning.ID> getMessages() {
        return ImmutableSet.copyOf(warnings);
    }
}
