package com.novartis.nibr.oncology.bioinformatics.veer.actions;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.variantcontext.writer.VariantContextWriter;
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder;
import htsjdk.variant.vcf.VCFFileReader;
import htsjdk.variant.vcf.VCFHeader;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by humema3 on 10/27/2015.
 */
@Component
public class CheckVariantReferenceAlleles extends VEERPipelineAction {
    private static final Logger logger = Logger.getLogger(CheckVariantReferenceAlleles.class);
    private static final String TEMP_FILE_FOR_IN_PLACE_EDITING = "temp";
    private static final String[] CHROMOSOMES = new String[24];
    static {
        for (int i = 1; i <= 22; i++) {
            CHROMOSOMES[i-1] = (i+"");
        }
        CHROMOSOMES[22] = "X";
        CHROMOSOMES[23] = "Y";
    }

//    private File vcfListFile;
//    private boolean hasHeader;

    private File reportFile;
    private PrintWriter reportWriter;
    private boolean toEditFiles;

    private boolean editInPlace;
    private File copyDir;
    private File copyListFile;
    private PrintWriter copyListFileWriter;

    private IndexedFastaSequenceFile indexedReferenceGenome;
    private final Map<String, String> chrSequences = Maps.newHashMap();

    public CheckVariantReferenceAlleles() {
        super(PipelineActionType.CheckVariantReferenceAlleles);

    }

//    public File getVcfListFile() {
//        return vcfListFile;
//    }
//
//    public void setVcfListFile(final File vcfListFile) {
//        this.vcfListFile = vcfListFile;
//    }

//    public boolean hasHeader() {
//        return hasHeader;
//    }
//
//    public void setHasHeader(final boolean hasHeader) {
//        this.hasHeader = hasHeader;
//    }

    public IndexedFastaSequenceFile getIndexedReferenceGenome() {
        return indexedReferenceGenome;
    }

    public void setIndexedReferenceGenome(final IndexedFastaSequenceFile indexedReferenceGenome) {
        this.indexedReferenceGenome = indexedReferenceGenome;
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(final File reportFile) {
        this.reportFile = reportFile;
    }

    public PrintWriter getReportWriter() {
        return reportWriter;
    }

    public void setReportWriter(final PrintWriter reportWriter) {
        this.reportWriter = reportWriter;
    }

    public boolean toEditFiles() {
        return toEditFiles;
    }

    public void setToEditFiles(final boolean toEditFiles) {
        this.toEditFiles = toEditFiles;
    }

    public boolean editInPlace() {
        return editInPlace;
    }

    public void setEditInPlace(final boolean editInPlace) {
        this.editInPlace = editInPlace;
    }

    public File getCopyDir() {
        return copyDir;
    }

    public void setCopyDir(final File copyDir) {
        this.copyDir = copyDir;
    }

    public File getCopyListFile() {
        return copyListFile;
    }

    public void setCopyListFile(final File copyListFile) {
        this.copyListFile = copyListFile;
    }

    public PrintWriter getCopyListFileWriter() {
        return copyListFileWriter;
    }

    public void setCopyListFileWriter(final PrintWriter copyListFileWriter) {
        this.copyListFileWriter = copyListFileWriter;
    }


    @Override
    public void execute() throws PipelineException {
        logger.info("Building chromosomal map");
        buildChrMap();

        logger.info("Checking VCF files");

        final File reportFile = getReportFile();
        try {
            setReportWriter(new PrintWriter(reportFile));
        } catch (final FileNotFoundException e) {
            throw new PipelineException("Could not create report file " + reportFile.getAbsolutePath(), e);
        }
        final boolean generateModifiedListCopy = toEditFiles() && !editInPlace();
        if (generateModifiedListCopy) {
            final File copyListFile = getCopyListFile();
            Assert.notNull(copyListFile);
            try {
                setCopyListFileWriter(new PrintWriter(copyListFile));
            } catch (final FileNotFoundException e) {
                throw new PipelineException("Could not create copy list file " + copyListFile.getAbsolutePath(), e);
            }
        }

//        final Scanner sc = new Scanner(getVcfListFile());
//        Assert.isTrue(sc.hasNextLine());
//        if (hasHeader()) {
//            final String header = sc.nextLine();
//            if (generateModifiedListCopy) {
//                getCopyListFileWriter().println(header);
//            }
//            Assert.isTrue(sc.hasNextLine());
//        }

        int fileCount = 0, errorCount = 0;
//        while (sc.hasNextLine()) {
        for (final String vcfFilepath : getInputVcfFilepaths()) {
//            final File vcfFilepath = new File(sc.nextLine());
            logger.info("Checking " + vcfFilepath + " (file " + (++fileCount) + ")");
            errorCount += checkVariants(new File(vcfFilepath));
        }

//        sc.close();
        getReportWriter().close();
        if (generateModifiedListCopy) {
            copyListFileWriter.close();
        }

        logger.info("TOTAL: " + errorCount + " inconsistent variants found");
    }

    private void throwIoException(final IOException e, final File f) throws PipelineException {
        throw new PipelineException("IO error on file " + f.getAbsolutePath(), e);
    }

    private void buildChrMap() {
        for (final String chr : CHROMOSOMES) {
            final ReferenceSequence refseq = getIndexedReferenceGenome().getSequence(chr);
            final byte[] bases = refseq.getBases();
            final StringBuilder sb = new StringBuilder();
            for (final byte b : bases) {
                sb.append((char) b);
            }
            final String baseString = sb.toString();
            chrSequences.put(chr, baseString);
        }
    }

    private int checkVariants(final File vcfFile) {
        Assert.notNull(vcfFile);
        final String vcfFilepath = vcfFile.getAbsolutePath();
        final VCFFileReader reader = new VCFFileReader(vcfFile, false);
        final String vcfCopyFilepath = toEditFiles() ? getVcfCopyFilepath(vcfFile) : null;
        final File vcfCopyFile = vcfCopyFilepath != null ? new File(vcfCopyFilepath) : null;
        final List<Integer> inconsistentVariantIndices = Lists.newArrayList();

        final CloseableIterator<VariantContext> iter = reader.iterator();
        int variantCount = 0, errorCount = 0;
        while (iter.hasNext()) {
            final VariantContext variant = iter.next();
            if (!chrSequences.containsKey(variant.getChr())) {
                continue;
            }
            final boolean consistent = checkVariant(variant, vcfFilepath);
            if (!consistent) {
                errorCount++;
                inconsistentVariantIndices.add(variantCount);
            }
            variantCount++;
        }
        iter.close();

        final boolean hasErrors = errorCount > 0;
        if (hasErrors) {
            logger.info(errorCount + " inconsistent variants found");
        }
        if (toEditFiles()) {
            if (hasErrors) {
                Assert.notNull(vcfCopyFile);
                createModifiedVcf(vcfFile, vcfCopyFile, inconsistentVariantIndices);
            }
            completeFileEdit(vcfFile, vcfCopyFile, hasErrors);
        }

        return errorCount;
    }

    private void createModifiedVcf(final File original, final File copy, final List<Integer> badVariantIndices) {
        final VCFFileReader reader = new VCFFileReader(original, false);
        final VariantContextWriter vcWriter = buildVariantContextWriter(reader, copy);
        Assert.notNull(vcWriter);
        final CloseableIterator<VariantContext> iter = reader.iterator();
        int variantIndex = 0, badVariantIndex = 0;
        while (iter.hasNext()) {
            final VariantContext variant = iter.next();
            if (badVariantIndex < badVariantIndices.size() && variantIndex == badVariantIndices.get(badVariantIndex)) {
                badVariantIndex++;
            } else {
                vcWriter.add(variant);
            }
            variantIndex++;
        }
        Assert.isTrue(badVariantIndex == badVariantIndices.size());
        iter.close();
        vcWriter.close();
    }

    private void completeFileEdit(final File vcfFile, final File vcfCopyFile, final boolean hasErrors) {
        confirmFileEdit(vcfFile, vcfCopyFile, hasErrors);
        if (!editInPlace()) {
            final PrintWriter copyListFileWriter = getCopyListFileWriter();
            Assert.notNull(copyListFileWriter);
            final String filepath = hasErrors ? vcfCopyFile.getAbsolutePath() : vcfFile.getAbsolutePath();
            copyListFileWriter.println(filepath);
        }
    }

    private void confirmFileEdit(final File vcfFile, final File vcfCopyFile, final boolean hasErrors) {
        Assert.notNull(vcfFile);
        Assert.notNull(vcfCopyFile);
        final String oldPath = vcfFile.getAbsolutePath();
        if (hasErrors) {
            if (editInPlace()) {
                final boolean moved = vcfCopyFile.renameTo(vcfFile);
                Assert.isTrue(moved, "Could not overwrite " + oldPath + " from temp copy");
                logger.info("Removed inconsistent variants from " + oldPath);
            } else {
                final String newPath = vcfCopyFile.getAbsolutePath();
                logger.info("Copied " + oldPath + " to " + newPath + " without inconsistent variants");
            }
        }
    }

    private String getVcfCopyFilepath(final File vcfFile) {
        if (!editInPlace()) {
            final File copyDir = getCopyDir();
            Assert.isTrue(copyDir.exists() || copyDir.mkdir());
            return copyDir + "/" + vcfFile.getName();
        } else {
            return TEMP_FILE_FOR_IN_PLACE_EDITING;
        }
    }

    private VariantContextWriter buildVariantContextWriter(final VCFFileReader vcfFileReader, final File vcfCopyFile) {
        if (vcfCopyFile == null) {
            return null;
        }
        final SAMSequenceDictionary refDict = getIndexedReferenceGenome().getSequenceDictionary();
        final VariantContextWriter vcWriter = new VariantContextWriterBuilder().setReferenceDictionary(refDict).setOutputFile(vcfCopyFile).setOutputFileType(VariantContextWriterBuilder.OutputType.VCF).build();
        final VCFHeader header = vcfFileReader.getFileHeader();
        vcWriter.writeHeader(header);
        return vcWriter;
    }

    private boolean checkVariant(final VariantContext variant, final String vcfFilepath) {
        final String chr = variant.getContig();
        final int start = variant.getStart();
        final String vcfRefAlleleSequence = variant.getReference().getBaseString();
        final int vcfRefAlleleLength = vcfRefAlleleSequence.length();
        final String fastaRefAlleleSequence = chrSequences.get(chr).substring(start - 1, start - 1 + vcfRefAlleleLength);
        final boolean refAllelesMatch = vcfRefAlleleSequence.equals(fastaRefAlleleSequence);
        if (!refAllelesMatch) {
            getReportWriter().println(chr + ":" + start + " - " + vcfFilepath + " lists " + vcfRefAlleleSequence + " as reference allele while reference FASTA lists " + fastaRefAlleleSequence);
        }
        return refAllelesMatch;
    }


}
