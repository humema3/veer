package com.novartis.nibr.oncology.bioinformatics.veer.actions;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.util.Assert;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by humema3 on 1/23/17.
 */
public abstract class VEERPipelineAction {
    private static final Logger LOGGER = Logger.getLogger(VEERPipelineAction.class);

    public enum PipelineActionType {
        CheckVariantReferenceAlleles,
        CombineVariants,
        RecallVariants,
        RemoveFilterCalls,
        GenerateVEERScores,
        AnnotateVCFs,
        AnnotateWithVEER
    }

    protected final PipelineActionType actionType;
//    private final SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.LENIENT);
    private String referenceGenomeFilepath;
    private String outputDirPath;
    private String outputFilename;
    private File javaHome;
    private List<String> inputVcfFilepaths = null;
    private Optional<File> inputDir;
    private Optional<String> inputFilename = Optional.absent();
    private Optional<Integer> numFiles = Optional.absent();
    private boolean local;
    private JavaSparkContext sparkContext;
    private boolean useHdfs;
    private Optional<FileSystem> hdfs;

    protected VEERPipelineAction(final PipelineActionType actionType) {
        this.actionType = actionType;
    }

    protected abstract void execute() throws PipelineException;

    public final void start() throws PipelineException {
        checkFileSystem();
        execute();
        getSparkContext().close();
    }

    protected void checkFileSystem() throws PipelineException {
        if (isLocal()) {
            Assert.isTrue((new File(referenceGenomeFilepath)).isFile(), "Reference genome file " + referenceGenomeFilepath + " not found");
            final List<String> inputFiles = getInputVcfFilepaths();
            for (final String filepath : inputFiles) {
                Assert.isTrue((new File(filepath)).isFile());
            }
        }
        createResultsDir();
    }

    private void createResultsDir() throws PipelineException {
        final String outputDirPath = getOutputDirPath();
        final boolean useHdfs = useHdfs();
        if (useHdfs) {
            final Optional<FileSystem> hdfsOpt = getHdfs();
            Assert.isTrue(hdfsOpt.isPresent());
            final FileSystem hdfs = hdfsOpt.get();
            final Path homeDirectory = hdfs.getHomeDirectory();
            final Path outputDirectory = new Path(homeDirectory, outputDirPath);
            try {
                if (!hdfs.isDirectory(outputDirectory)) {
                    hdfs.mkdirs(outputDirectory);
                }
            } catch (final IOException e) {
                throw new PipelineException("Error creating HDFS directory", e);
            }
            setOutputDirPath(outputDirectory.toString());
        } else {
            //        final File resultDir = new File(getOutputDirPath(), getActionType().name());
            final File resultDir = new File(outputDirPath);
            if (!resultDir.exists()) {
                resultDir.mkdir();
            } else if (!resultDir.mkdir()) {
                throw new PipelineException("Could not create dir: " + resultDir);
            }
        }
    }

//    protected boolean hasFinishAction() { // override as necessary
//        return false;
//    }

//    private boolean isValidReference() throws PipelineException {
//        final Properties projectProperties = getProjectProperties();
//
//        if(projectProperties.containsKey("ref")){
//            return new File(projectProperties.getProperty("ref")).equals(getReferenceGenomeFilepath());
//        }else{
//            projectProperties.put("ref", getReferenceGenomeFilepath().getAbsolutePath());
//            saveProjectProperties(projectProperties);
//            return true;
//        }
//    }
//
//    private void saveProjectProperties(final Properties projectProperties) throws PipelineException {
//        try{
//            final OutputStream out = new FileOutputStream(new File(getOutputDirPath(), "project.properties"));
//            projectProperties.store(out, "Stores project properties for consistency checks");
//        }catch (IOException e){
//            final String msg = "Could not create project properties";
//            throw new PipelineException(msg, e);
//        }
//    }
//
//    private Properties getProjectProperties() throws PipelineException {
//        final File projectPropertiesFile = new File(getOutputDirPath(), "project.properties");
//
//        final Properties projectProperties = new Properties();
//        if(projectPropertiesFile.exists()){
//            try {
//                projectProperties.load(new FileInputStream(projectPropertiesFile));
//            } catch (IOException e) {
//                final String msg = "Could not check project properties";
//                throw new PipelineException(msg, e);
//            }
//        }
//
//        return projectProperties;
//    }

    public PipelineActionType getActionType() {
        return actionType;
    }

    public String getReferenceGenomeFilepath() {
        return referenceGenomeFilepath;
    }

    public void setReferenceGenomeFilepath(final String referenceGenomeFilepath) {
        this.referenceGenomeFilepath = referenceGenomeFilepath;
    }

    public String getOutputDirPath() {
        return outputDirPath;
    }

    public void setOutputDirPath(final String outputDirPath) {
        this.outputDirPath = outputDirPath;
    }

    public File getJavaHome() {
        return javaHome;
    }

    public void setJavaHome(final File javaHome) {
        this.javaHome = javaHome;
    }

    private static final Function<File, String> GET_PATH = new Function<File, String>() {
        public String apply(@Nullable final File file) {
            return file.getAbsolutePath();
        }
    };

    public List<String> getInputVcfFilepaths() throws PipelineException {
//        LOGGER.info("Extracting VCF files from " + inputDir.getAbsolutePath());
        if (inputVcfFilepaths == null) {
            Assert.isTrue(isLocal(), "VCF filepaths not specified while not in local mode");
            final Optional<File> inputDirOpt = getInputDir();
            Assert.isTrue(inputDirOpt.isPresent(), "Input files and directory not specified in local mode");
            final File inputDir = inputDirOpt.get();
            if (getInputFilename().isPresent()) {
                setFilesFromDirAndFilename(inputDir);
            } else {
                setFilesFromDirOnly(inputDir);
            }
        }
//        if (useHdfs()) {
//            final Optional<FileSystem> hdfsOpt = getHdfs();
//            Assert.isTrue(hdfsOpt.isPresent());
//            final FileSystem hdfs = hdfsOpt.get();
//            inputVcfFilepaths = Lists.newArrayList(Iterables.transform(inputVcfFilepaths, new Function<String, String>() {
//                @Override
//                public String apply(@Nullable final String s) {
//                    return new Path(hdfs.getHomeDirectory(), s).toString();
//                }
//            }));
//        }
        return ImmutableList.copyOf(inputVcfFilepaths);
    }

    private void setFilesFromDirOnly(final File inputDir) {
        final List<File> files = Lists.newArrayList(inputDir.listFiles(new FilenameFilter() {
            public boolean accept(final File dir, final String name) {
                return name.endsWith(".vcf");
            }
        }));
        setInputVcfFilepaths(Lists.newArrayList(Iterables.transform(files, GET_PATH)));
        setNumFiles(Optional.of(inputVcfFilepaths.size()));
    }

    private void setFilesFromDirAndFilename(final File inputDir) {
        final String inputFilename = getInputFilename().get();
        LOGGER.info("Looking for file suffix " + inputFilename);
        if (getNumFiles().isPresent()) {
            setFilesFromDirAndFilenameWithNumFiles(inputDir, inputFilename);
        } else {
            setFilesFromDirAndFilenameOnly(inputDir, inputFilename);
        }
    }

    private void setFilesFromDirAndFilenameWithNumFiles(final File inputDir, final String inputFilename) {
        final int numFiles = getNumFiles().get();
        LOGGER.info("Looking for numeric prefixes from 1 to " + numFiles);
        final List<File> files = Lists.newArrayList(inputDir.listFiles(new FilenameFilter() {
               public boolean accept(final File dir, final String name) {
                   if (numFiles == 1 && name.equals(inputFilename + ".vcf")) {
                       return true;
                   }
                   final Pattern p = Pattern.compile("^(\\d+)_" + inputFilename + ".vcf$");
                   final Matcher m = p.matcher(name);
                   if (!m.matches()) {
                       return false;
                   }
                   final int fileNum = Integer.parseInt(m.group(1));
                   return fileNum >= 1 && fileNum <= numFiles;
               }
           }));
        setInputVcfFilepaths(Lists.newArrayList(Iterables.transform(files, GET_PATH)));
    }

    private void setFilesFromDirAndFilenameOnly(final File inputDir, final String inputFilename) {
        final List<File> files = Lists.newArrayList(inputDir.listFiles(new FilenameFilter() {
              public boolean accept(final File dir, final String name) {
                  final Pattern p = Pattern.compile("^(?:(\\d+)_)?" + inputFilename + "\\.vcf$");
                  return p.matcher(name).matches();
              }
        }));
        setInputVcfFilepaths(Lists.newArrayList(Iterables.transform(files, GET_PATH)));
        setNumFiles(Optional.of(inputVcfFilepaths.size()));
    }

    public void setInputVcfFilepaths(final List<String> inputVcfFilepaths) {
        this.inputVcfFilepaths = inputVcfFilepaths;
    }

    public Optional<File> getInputDir() {
        return inputDir;
    }

    public void setInputDir(final File inputDir) {
        this.inputDir = Optional.of(inputDir);
    }

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(final String outputFilename) {
        this.outputFilename = outputFilename;
    }

    public Optional<String> getInputFilename() {
        return inputFilename;
    }

    public void setInputFilename(final Optional<String> inputFilename) {
        this.inputFilename = inputFilename;
    }

    public Optional<Integer> getNumFiles() {
        return numFiles;
    }

    public void setNumFiles(final Optional<Integer> numFiles) {
        this.numFiles = numFiles;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(final boolean local) {
        this.local = local;
    }

    public JavaSparkContext getSparkContext() {
        return sparkContext;
    }

    public void setSparkContext(final JavaSparkContext sparkContext) {
        this.sparkContext = sparkContext;
    }

    public boolean useHdfs() {
        return useHdfs;
    }

    public void setUseHdfs(final boolean useHdfs) throws PipelineException {
        this.useHdfs = useHdfs;
        if (useHdfs) {
            final Configuration conf = new Configuration(); // TODO ?
            final FileSystem hdfs;
            try {
                hdfs = FileSystem.get(conf);
            } catch (final IOException e) {
                throw new PipelineException("Could not instantiate new HDFS FileSystem object", e);
            }
            setHdfs(hdfs);
        }
    }

    public Optional<FileSystem> getHdfs() {
        return hdfs;
    }

    public void setHdfs(final Optional<FileSystem> hdfs) {
        this.hdfs = hdfs;
    }

    public void setHdfs(final FileSystem hdfs) {
        this.hdfs = hdfs != null ? Optional.of(hdfs): Optional.<FileSystem>absent();
    }

    private Optional<Integer> numPartitions = Optional.absent();
    public Optional<Integer> getNumPartitions() {
        return numPartitions;
    }

    public void setNumPartitions(final Optional<Integer> numPartitions) {
        this.numPartitions = numPartitions;
    }

    public void setNumPartitions(final int numPartitions) {
        this.numPartitions = Optional.of(numPartitions);
    }

}
