package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.io.File;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.common.util.ActionExecutor;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.ActionHalted;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.util.Assert;

import java.util.List;

public abstract class VEERActionExecutor extends ActionExecutor {
    private static final Logger LOGGER = Logger.getLogger(VEERActionExecutor.class);

    protected Options getOptions() {
        final Options options = new Options();
        options.addOption("t", "tool", true, "The tool to use");
        options.getOption("t").setRequired(true);
        options.addOption("r", "reference_genome", true, "The reference genome FASTA file");
        options.getOption("r").setRequired(true);
        options.addOption("j", "java_home", true, "JDK home directory (optional if JAVA_HOME environment variable is specified");
        options.getOption("j").setRequired(false);
//        options.addOption("e", "email", true, "Email address for SGE cluster notifications.");
//        options.getOption("e").setRequired(true);
        options.addOption("d", "input_dir", true, "The directory in which the input VCF files are stored");
        options.getOption("d").setRequired(false);
        options.addOption("o", "output_dir", true, "The directory for annotation output");
        options.getOption("o").setRequired(false);
        options.addOption("of", "output_filename", true, "The file suffix for all output VCF files");
        options.getOption("of").setRequired(false);
        options.addOption("sm", "spark_master", true, "The master URL for Spark-based parallelization");
        options.getOption("sm").setRequired(false);
        options.addOption("n", "num_files", true, "The number of files over which the input is parallelized. Input VCF files should start with an integer from 1 to this number inclusive (see -f argument). Default is 1 (no parallelization).");
        options.getOption("n").setRequired(false);
        options.addOption("f", "input_filename", true, "The filename for all the input VCF files. If not supplied, all files in the directory will be used. If supplied, each input file should be called [i]_[filename].vcf, where [i] is an integer between " +
                "1 and the '-n' argument, and [filename] is this argument. If you supply -n 1, the '[i]_' prefix is optional.");
        options.getOption("f").setRequired(false);
        options.addOption("vcfs", "vcf_files", true, "Comma-delimited list of files (for use with spark-submit)");
        options.getOption("vcfs").setRequired(false);
        options.addOption("hdfs", "use_hdfs", false, "All file I/O is to be done through HDFS");
        options.getOption("hdfs").setRequired(false);
        options.addOption("p", "num_partitions", true, "Number of partitions to be used");
        options.getOption("p").setRequired(false);
        return options;
    }

    @Override
    public void build(final String[] args) throws PipelineException {
        LOGGER.info("Building " + this.getClass().getSimpleName() + " from arguments");
        final Options options = getOptions();
        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd = processArgs(parser, options, args);
        checkArgs(cmd, new String[]{"t", "r", "o"}, options);
//        checkToolArg(cmd, options);

//        final String sparkMaster = cmd.getOptionValue("spark_master", "local"); // uncomment for IDE
        final SparkConf sparkConf = new SparkConf().setAppName(getActionType().toString())
                .set("spark.executor.extraJavaOptions", "-XX:hashCode=0")
                .set("spark.driver.extraJavaOptions", "-XX:hashCode=0")
                ; // remove semicolon for IDE
//                .setMaster(sparkMaster); // uncomment for IDE
//                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
//        sparkConf.setAppName(getActionType().toString()).setMaster(sparkMaster);  // uncomment for IDE

//                .set("spark.kryo.registrationRequired", "true"); // would be good but may be unwieldy

//        sparkConf.registerKryoClasses(new Class[]{
//                FeatureReader.class,
//                VEERCountAnnotator.class,
//                VEERScoreAnnotator.class,
//                SAMSequenceRecord.class,
//                SAMSequenceDictionary.class,
//                VCFFileReader.class,
//                VariantContext.class});
        setSparkContext(new JavaSparkContext(sparkConf));
        final JavaSparkContext sparkContext = getSparkContext();
        final String sparkMaster = sparkContext.master(); // uncomment for spark-submit
        setSparkMaster(sparkMaster);
        LOGGER.info("Using spark master " + sparkMaster);

        final VEERPipelineAction action = getAction();
        final String reference = cmd.getOptionValue("r");

        final boolean useHdfs = cmd.hasOption("hdfs");
        action.setUseHdfs(useHdfs);
        if (sparkMaster.startsWith("local")) {
            LOGGER.info("Initializing action in Spark local mode");
            action.setLocal(true);
            if (cmd.hasOption("vcfs")) {
                action.setInputVcfFilepaths(Lists.newArrayList(cmd.getOptionValue("vcfs").split(",")));
            } else {
                action.setInputDir(new File(cmd.getOptionValue("d")));
                action.setInputFilename(cmd.hasOption("f") ? Optional.of(cmd.getOptionValue("f")) : Optional.<String>absent());
            }
        } else {
            LOGGER.info("Initializing action with Spark master " + sparkMaster);
            action.setLocal(false);
            final List<String> vcfFilepaths = Lists.newArrayList(cmd.getOptionValue("vcfs").split(","));
            LOGGER.info("VCF files: " + StringUtils.join(vcfFilepaths, ", "));
            action.setInputVcfFilepaths(vcfFilepaths);
        }
        action.setReferenceGenomeFilepath(reference);
        action.setNumFiles(cmd.hasOption("n") ? Optional.of(Integer.parseInt(cmd.getOptionValue("n"))) : Optional.<Integer>absent());
        String outputDirPath = cmd.getOptionValue("o");
        if (outputDirPath.startsWith("hdfs://")) {
            Assert.isTrue(useHdfs, "Output directory prefixed with hdfs:// but use_hdfs flag not passed");
            outputDirPath = outputDirPath.substring(7); // we already add the prefix later, don't want it twice
        }
        action.setOutputDirPath(outputDirPath);
        action.setOutputFilename(cmd.getOptionValue("of", getActionType().toString()));
//        final String email = cmd.getOptionValue("e");
        final File javaHome = new File(cmd.getOptionValue("j", System.getenv("JAVA_HOME")));
        checkJavaHome(javaHome);
        action.setJavaHome(javaHome);
        if (cmd.hasOption("p")) {
            action.setNumPartitions(Integer.parseInt(cmd.getOptionValue("p")));
        }
//        try {
//            checkOutputPath(outPath);
//        } catch (final IOException e) {
//            throw new PipelineException("Error checking output path", e);
//        }
//        action.setEmail(email);
    }

    protected abstract VEERPipelineAction getAction();

    @Override
    public VEERPipelineAction.PipelineActionType getActionType() {
        return getAction().getActionType();
    }

    private void checkToolArg(final CommandLine cmd, final Options options) {
        //None of this should ever happen
        if (!cmd.hasOption("t")) {
            usage("Type not set for " + getActionType(), options);
        } else {
            final String type = WordUtils.capitalize(cmd.getOptionValue("t"));
            if (!type.equals(getActionType().name())) {
                usage("Wrong action type", options);
            }
        }
    }

    public static CommandLine processArgs(final CommandLineParser parser, final Options options, final String[] args) throws PipelineException {
        CommandLine cmd = null;
        boolean processingFinished = false;
        while (!processingFinished) {
            try {
                cmd = parser.parse(options, args);
                processingFinished = true;
            } catch (final UnrecognizedOptionException e) {
                throwIfUnrecognized(new PipelineException("Error parsing command-line args", e));
                final List<String> argsList = Lists.newArrayList(args);
                final String arg = e.getOption();
                int argIndex = -1;
                for (int i = 0; i < argsList.size(); i++) {
                    if (argsList.get(i).equals(arg)) {
                        argsList.remove(i);
                        argIndex = i;
                        break;
                    }
                }
                Assert.isTrue(argIndex != -1);
                if (argIndex < argsList.size() && !options.hasOption(argsList.get(argIndex).replace("-",""))) {
                    argsList.remove(argIndex);
                }
                cmd = processArgs(parser, options, argsList.toArray(new String[argsList.size()]));
                processingFinished = true;
            } catch (final ParseException e) {
                throw new PipelineException("Error parsing command-line args", e);
            }
        }
        Assert.notNull(cmd);
        return cmd;
    }



//    protected static final void checkOutputPath(final File outPath) throws PipelineException {
//        if (!outPath.exists()) {
//            if (promptForSubmit("Would you like to create " + outPath + " for analysis")) {
//                if (!outPath.mkdir()) {
//                    throw new ActionHalted("Could not create directory " + outPath);
//                }
//            } else {
//                throw new ActionHalted("Directory not created and action halted");
//            }
//        } else if (!outPath.isDirectory()) {
//            throw new ActionHalted("There already seems to be a file (not a directory) with the path " + outPath.getAbsolutePath());
//        }
//    }

    private static void checkJavaHome(final File javaHome) {
        checkPath(javaHome, "Supplied or default (via JAVA_HOME) JDK home directory does not exist");
    }

    protected static final void checkPath(final File path, final String errorMessage) {
        if (!path.exists()) {
            throw new ActionHalted(errorMessage + ": " + path.getAbsolutePath());
        }
    }

    @Override
    public void start() throws PipelineException {
        getAction().start();
    }

    public static void throwIfUnrecognized(final PipelineException e) throws PipelineException {
        throwIfUnrecognized(e, ALL_ARGS);
    }

    public static void throwIfUnrecognized(final PipelineException e, final List<String> okOptions) throws PipelineException {
        final Throwable cause = e.getCause();
        if (cause instanceof UnrecognizedOptionException) {
            final String option = ((UnrecognizedOptionException) cause).getOption();
            if (!okOptions.contains(option)) {
                LOGGER.info("Unrecognized option: " + option);
                throw e;
            }
        } else {
            throw e;
        }
    }

    private static final List<String> ALL_ARGS = Lists.newArrayList("-t", "--tool", "-r", "--reference_genome",
            "-j", "--java_home", "-d", "--input_dir", "-o", "--output_dir", "-of", "--output_filename",
            "-sm", "--spark_master", "-n", "--num_files", "-f", "--input_filename", "-vcfs", "--vcf_files",
            "-res", "--counts_resource", "-a", "--annotator", "-c", "--counts_only", "-no_cv", "--no_combine_variants",
            "-b", "--bam_sample_file");

}
