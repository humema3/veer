package com.novartis.nibr.oncology.bioinformatics.common.util;

import com.google.common.collect.Sets;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.springframework.util.Assert;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 *
 * Created by jonesmic on 2/25/14.
 */
public abstract class AbstractApplication {

    public static void checkArgs(CommandLine cmd, String[] requiredOptions, Options options) {
        String missing = "";
        Set<String> cmdOptionNames = Sets.newHashSet();

        for(Option option : cmd.getOptions()){
            cmdOptionNames.add(option.getOpt());
        }

        boolean hasMissing = false;
        for(String reqOption : requiredOptions){
            if(!cmdOptionNames.contains(reqOption)){
                missing += " " + reqOption;
                hasMissing = true;
            }
        }
        if(hasMissing){
            usage("You must specify all required options (Missing: " + missing + ")", options);
        }
    }

    /**
     * Implementing classes can override this for app specific behavior
     * @param msg - the error message
     * @param options - the cli options object
     */
    protected static void usage(String msg, Options options){
        HelpFormatter formatter = new HelpFormatter();
        msg = "\n\nError:"
                + msg + "\n\n";

        formatter.printHelp(msg, options, true);

        System.out.println();
        System.exit(-1);
    }


    public static void printArgs(String[] args, File outDir, Options options, String app) throws IOException {
        //File resultsFile = new File(outDir, "command_line.txt");

        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd.kk.mm.ss");
        System.out.println("Current Date: " + ft.format(dNow));

        File resultsFile = new File(outDir, app + ".cl." + ft.format(dNow) + ".txt");

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(resultsFile)));

        out.println("CWD: " + System.getProperty("user.dir"));

        out.print(app+" ");
        for(String arg : args){
            out.print(" ");
            out.print(arg);
        }
        out.println();
        out.println();

        HelpFormatter formatter = new HelpFormatter();

        formatter.printHelp(out, 100, AbstractApplication.class.getSimpleName(),
                "Performed on " + new Date(), options, 5, 1, "GROUND", true);
        out.flush();
        out.close();
    }

    public static void printArgs(String[] args, File outDir, Options options) throws IOException {
        printArgs(args, outDir, options, "PoncNGSPipeline.jar ");
    }

    private static int lastPercent = 0; //Note: This will goof up if more then one process is using printProgBar
    public static void printProgBar(int percent){
        boolean print = false;
        StringBuilder bar = new StringBuilder("[");

        for(int i = 0; i < 50; i++){
            if( i < (percent/2)){
                bar.append("=");
            }else if( i == (percent/2)){
                bar.append(">");
            }else{
                bar.append(" ");
            }
        }

        bar.append("]   " + percent + "%     ");
        if(lastPercent<percent){
            lastPercent = percent;
            print = true;
        }
        if(print)
            System.out.print("\r" + bar.toString());
    }

    public static class ProgressBar{
        int lastPercent = 0;
        private final int size;

        public ProgressBar(int size) {
            this.size = size;
        }

        public void printProgBar(int iteration){
            int percent = (int)((double)iteration/(double)size*100);

            boolean print = false;
            StringBuilder bar = new StringBuilder("[");

            for(int i = 0; i < 50; i++){
                if( i < (percent/2)){
                    bar.append("=");
                }else if( i == (percent/2)){
                    bar.append(">");
                }else{
                    bar.append(" ");
                }
            }

            bar.append("]   ").append(percent).append("%     ");
            if(lastPercent<percent){
                lastPercent = percent;
                print = true;
            }
            if(print)
                System.out.print("\r" + bar.toString());
        }
    }

    public static boolean promptForSubmit() throws IOException {

        System.out.print("Submit to cluster now? [y/n]: ");

        //  open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String answer;
        try {
            answer = br.readLine();
        } catch (IOException ioe) {
            System.err.println("IO error trying to read your input!");
            throw ioe;
        }
        return answer.toLowerCase().startsWith("y");
    }

    public static boolean promptForSubmit(String question) throws IOException {

        System.out.print(question + " [y/n]: ");

        //  open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String answer;
        try {
            answer = br.readLine();
        } catch (IOException ioe) {
            System.err.println("IO error trying to read your input!");
            throw ioe;
        }
        return answer.toLowerCase().startsWith("y");
    }

    public static void registerSpringComponent(Object main, String... configLocations){
        Assert.isTrue(configLocations.length>0, "Please specify at least one configuration");
        new ApplicationContextLoader().load(main, configLocations);
    }
}
