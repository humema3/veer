package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import htsjdk.variant.variantcontext.Allele;

/**
 * POJO that holds some essential fields.
 * Created by jonesmic on 6/29/2015.
 */
public class TabixRecord {

    private final String chr;
    private final int position;
    private final String[] tabixRecordValues;
    private final Allele refAllele, altAllele;

    public TabixRecord(final String chr, final int position, final String[] tabixRecordValues) {
        this.chr = chr;
        this.position = position;
        this.tabixRecordValues = tabixRecordValues;

        refAllele = Allele.create(tabixRecordValues[2], true);
        altAllele = Allele.create(tabixRecordValues[3]);

    }

    public String getChr() {
        return chr;
    }

    public int getPosition() {
        return position;
    }

    public String getTabixRecordValue(int index) {
        return tabixRecordValues[index];
    }

    public Allele getRefAllele() {
        return refAllele;
    }

    public Allele getAltAllele() {
        return altAllele;
    }
}
