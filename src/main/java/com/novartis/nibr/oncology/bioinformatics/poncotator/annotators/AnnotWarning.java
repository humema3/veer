package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.google.common.collect.ImmutableMap;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;

/**
 * Helps track standard issues with annotation
 * Created by jonesmic on 6/17/2015.
 */
public class AnnotWarning {
    public enum ID {TT, MG, MET, GM, NCT}

    static final ImmutableMap<ID, String> WARNING_DESCRIPTION_MAP =
            new ImmutableMap.Builder<ID, String>()
                    .put(ID.TT, "Tied Transcript")
                    .put(ID.MG, "Ambiguous Multi Gene Variant")
                    .put(ID.NCT, "Could not determine best transcript from CatABs")
                    .put(ID.MET, "MultipleExpertTranscripts")
                    .put(ID.GM, "Gene Not in NIBR GENOME")
                    .build();
    
    private static String DESCRIPTION_STRING = "";
    static {
        for(ID ID : WARNING_DESCRIPTION_MAP.keySet()){
            DESCRIPTION_STRING += ID.toString() + "=" + WARNING_DESCRIPTION_MAP.get(ID) + ",";
        }
        DESCRIPTION_STRING = DESCRIPTION_STRING.substring(0, DESCRIPTION_STRING.length()-1);
    }    

    public static VCFInfoHeaderLine INFO_HEADER
            = new VCFInfoHeaderLine("PONC", VCFHeaderLineCount.UNBOUNDED, VCFHeaderLineType.String,
                                        DESCRIPTION_STRING);
}

