package com.novartis.nibr.oncology.bioinformatics;

import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.common.util.AbstractApplication;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import com.novartis.nibr.oncology.bioinformatics.veer.executors.VEERActionExecutor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

/**
 *
 * Created by jonesmic on 2/4/14.
 */
@Component
public class VEERTools extends AbstractApplication {
    private final static Logger LOGGER = Logger.getLogger(VEERTools.class);

    @Autowired
    private List<VEERActionExecutor> actions;

    public static void main(final String[] args) throws Exception {
//        configureApplication();

        final VEERTools app = new VEERTools();
        registerSpringComponent(app, "application-context.xml");
        app.run(args);
    }

//    private static void configureApplication() throws IOException {
//        PoncPropertiesValidator.validateProperties();
//    }

    private void run(final String[] args) throws PipelineException {
        LOGGER.info("Beginning application run");

        final Options options = new Options();
        options.addOption("t", "tool", true, "The tool to use");

        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd;
        try {
            cmd = parser.parse(options, args, true);
        } catch (final ParseException e) {
            throw new PipelineException("Error parsing command-line args", e);
        }

        if(!cmd.hasOption("tool")){
            usage("Missing action type", options);
        }else{
            final String typeString = cmd.getOptionValue("tool");
            VEERPipelineAction.PipelineActionType actionType=null;
            try {
                actionType = VEERPipelineAction.PipelineActionType.valueOf(typeString);
            }catch (final IllegalArgumentException e){
                LOGGER.error(e.getMessage(), e);
                usage("No action of type " + typeString, options);
            }

            final VEERActionExecutor executor = getActionFromType(actionType);
//            try {
//                executor.build(args);
//            } catch (final PipelineException e) {
//                LOGGER.error(e.getMessage(), e);
//                usage("Error: " + e.getMessage(), options);
//            } catch (final ActionHalted ah){
//                LOGGER.error(ah.getMessage(), ah);
//                usage("Error: " + ah.getMessage(), options);
//            }
            executor.build(args);
            executor.start();
        }
    }

    private VEERActionExecutor getActionFromType(VEERPipelineAction.PipelineActionType actionType) {
        VEERActionExecutor newAction = null;
        for(final VEERActionExecutor actionExecutor : actions){
            if(actionExecutor.getActionType() == actionType){
                newAction= actionExecutor;
            }
        }
        Assert.notNull(newAction, "This should never happen");
        return newAction;
    }

    private List<VEERPipelineAction.PipelineActionType> getTypes() {
        final List<VEERPipelineAction.PipelineActionType> types = Lists.newArrayList();
        for(final VEERActionExecutor actionExecutor : actions){
            types.add(actionExecutor.getActionType());
        }
        return types;
    }
}
