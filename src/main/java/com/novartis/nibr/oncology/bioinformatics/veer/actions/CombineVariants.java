package com.novartis.nibr.oncology.bioinformatics.veer.actions;


import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.broadinstitute.gatk.engine.CommandLineGATK;
import org.springframework.stereotype.Component;
import scala.Tuple2;

import java.io.*;
import java.util.List;

@Component
public class CombineVariants extends VEERPipelineAction {
    private final static Logger LOGGER = Logger.getLogger(CombineVariants.class);

//    private static String VCF_LIST_FILE_NAME = "vcf_list.txt";

//    private File refVcfListFile;
//    private List<String> vcfList;
//    private JavaSparkContext sparkContext;

    public CombineVariants() {
        super(PipelineActionType.CombineVariants);
    }

//    @Override
//    protected void checkFileSystem() throws PipelineException {
////        super.checkFileSystem();
//        //Copy the file to keep a reference
////        LOGGER.info("Deriving VCF list from " + getRefVcfListFile());
//        try {
//            buildVcfListFile();
//        } catch (final IOException e) {
//            throw new PipelineException("Error checking file system", e);
//        }
//    }

    //Build list of VCFs
//    private void buildVcfListFile() throws IOException {
//        //File to write list to
//        final File vcfListFile = new File(getOutputDirPath(), VCF_LIST_FILE_NAME);
//        final PrintWriter out
//                = new PrintWriter(new BufferedWriter(new FileWriter(vcfListFile)));
//        setVcfList(extractVcfFileLocations(getRefVcfListFile()));
//        out.print(StringUtils.join(getVcfList(), "\n"));
//        out.flush();
//        out.close();
//    }
//
//    static final List<String> extractVcfFileLocations(final File refVcfListFile) throws IOException {
//        final Pattern pattern = Pattern.compile("file:\\/\\/(\\w+)(\\/.*\\.vcf)");
//        final FileIterator fit = new FileIterator(refVcfListFile);
//        Assert.isTrue(fit.getHeader().contains("VCF_URI"), "File must have the VCF_URI column header");
//        final List<String> locations = Lists.newArrayList("VCF_URI");
//        for (final Row row : fit) {
//            final String uri = row.valueAt("VCF_URI");
//            final Matcher m = pattern.matcher(uri);
//            Assert.isTrue(m.matches(), "Bad VCF URI " + uri);
//            final String vcf = m.group(2);
//
//            if(! new File(vcf).exists()){
//                LOGGER.error("Ignoring missing File does not exist: " + vcf);
//            }else {
//                locations.add(vcf);
//            }
//        }
//        return locations;
//    }

    @Override
    protected void execute() throws PipelineException {
        final List<String> inputVcfFilepaths = getInputVcfFilepaths();
        final JavaSparkContext sparkContext = getSparkContext();
        final JavaRDD<File> inputVcfRDD = sparkContext.parallelize(inputVcfFilepaths).map(new org.apache.spark.api.java.function.Function<String, File>() {
            public File call(final String filepath) throws Exception {
                return new File(filepath);
            }
        });
        final List<String> gatkArgs = Lists.newArrayList("--suppressCommandLineHeader", "--unsafe", "LENIENT_VCF_PROCESSING", "-R", getReferenceGenomeFilepath(), "--disable_auto_index_creation_and_locking_when_reading_rods", "-T", "CombineVariants", "-V");
        gatkArgs.addAll(Lists.newArrayList(StringUtils.join(inputVcfFilepaths, " -V ").split(" ")));
        final List<String[]> gatkCommands = Lists.newArrayList();
        final String outputFilename = getOutputFilename();
        for (int chr = 1; chr <= 24; chr++) {
            final List<String> gatkCommandList = Lists.newArrayList(gatkArgs);
            gatkCommandList.addAll(Lists.newArrayList("-L", chr+"", "-o", chr + "_" + outputFilename + ".vcf"));
            gatkCommands.add((String[]) gatkCommandList.toArray());
        }
        final JavaRDD<String[]> gatkCommandsRDD = sparkContext.parallelize(gatkCommands);
        final List<CommandLineGATK> gatkRunners = Lists.newArrayList();
        for (int i = 0; i < gatkCommands.size(); i++) {
            gatkRunners.add(new CommandLineGATK());
        }
        final JavaRDD<CommandLineGATK> gatkRunnersRDD = sparkContext.parallelize(gatkRunners);
        final JavaPairRDD<CommandLineGATK, String[]> runnersAndArgs = gatkRunnersRDD.zip(gatkCommandsRDD);
        runnersAndArgs.foreach(new VoidFunction<Tuple2<CommandLineGATK, String[]>>() {
            public void call(Tuple2<CommandLineGATK, String[]> tuple) throws Exception {
                CommandLineGATK.start(tuple._1(), tuple._2());
            }
        });
//        gatkCommandsRDD.foreach(new VoidFunction<String[]>() {
//            public void call(final String[] args) throws Exception {
//                CommandLineGATK.main(args);
//            }
//        });
    }

//    public JavaSparkContext getSparkContext() {
//        return sparkContext;
//    }
//
//    public void setSparkContext(final JavaSparkContext sparkContext) {
//        this.sparkContext = sparkContext;
//    }
    //    public File getRefVcfListFile() {
//        return refVcfListFile;
//    }
//
//    public void setRefVcfListFile(final File refVcfListFile) {
//        this.refVcfListFile = refVcfListFile;
//    }

//    public void setVcfList(final List<String> vcfList) {
//        this.vcfList = vcfList;
//    }
//
//    public List<String> getVcfList() {
//        return ImmutableList.copyOf(vcfList);
//    }
}
