package com.novartis.nibr.oncology.bioinformatics.poncotator;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.novartis.nibr.oncology.bioinformatics.common.util.ActionExecutor;

import java.io.*;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.samtools.reference.IndexedFastaSequenceFile;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.writer.VariantContextWriter;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.writer.VariantContextWriterBuilder;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.*;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.Annotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.ResourceAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERCountAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERScoreAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.io.TabixVariantResource;
import com.novartis.nibr.oncology.bioinformatics.poncotator.io.VCFVariantResource;
import com.novartis.nibr.oncology.bioinformatics.poncotator.io.VariantResource;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.variant.variantcontext.writer.Options;
import htsjdk.variant.vcf.VCFHeaderLine;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.Partitioner;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.storage.StorageLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import scala.Tuple2;

import javax.annotation.Nullable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This ActionExecutor processes through any classes of type Annotator and adds them to the annotator chain according to
 * 1) CatAB and VEERCounts (including any Resource annotator with VEERCounts as INFO field) are always first
 * 2) User input order.
 *
 * It then annotates every VariantContext in the input VCF based on the annotation chain.
 *
 * If a VEERScores annotation is requested, a VEERCounts annotation is automatically added onto the beginning of the
 * chain if one was not supplied (either by explicit annotation or through a Resource).
 *
 * Created by jonesmic on 3/3/14.
 */
@Component
public class PoncotatorActionExecutor extends ActionExecutor implements Serializable {
    private final static Logger LOGGER = Logger.getLogger(PoncotatorActionExecutor.class);
    private static final int NUM_VARIANTS_PER_PARTITION = 100;

    static {
        LOGGER.setLevel(Level.INFO);
    }

//    private static final Set<String> SNPEFF_REQUIRED_ANNOTATORS = ImmutableSet.of(CatAB.INFO.getID(), CDD.INFO.getID(), RankedTranscript.INFO.getID());

//    private File VCFFile;
//    private File outFile;
    private List<String> vcfFilepaths;
    private List<String> outputFilepaths;

    // TODO autowire?
//    @Value("${reference.genome.file}")
//    private String referenceGenomeFilepath;
    private String referenceGenomeFilepath;

//    private JavaRDD<VCFFileReader> vcfReaders;
    private JavaRDD<String> vcfLines;
//    private List<VariantUtils> variantUtils;
    private static SAMSequenceDictionary REF_DICT;
//    private JavaRDD<MyVCFWriter> variantContextWriters;
    private ResourceLocationMap resourceLocationMap = new ResourceLocationMap();
    private LinkedList<Object> types;
    private VariantResource.QueryType queryType = VariantResource.QueryType.Index;
    private Optional<Integer> numPartitions = Optional.absent();

    public void setQueryType(VariantResource.QueryType queryType) {
        this.queryType = queryType;
    }

    public VariantResource.QueryType getQueryType() {
        return queryType;
    }

    public enum ActionType {Poncotator}

    @Autowired
    private List<Annotator> annotators;

    @Autowired
    private transient ApplicationContext ctx;

    private transient JavaSparkContext sparkContext;
//    private String sparkMaster;

    private Pattern annotatorResourcePattern = Pattern.compile("^Resource:(.+):(.+)$");
    private Pattern vcfResourceInfoLocationPattern = Pattern.compile("^([\\w.]+):([-\\w.\\/\\\\]+\\.vcf)$");
    private Pattern tabixTestResourceInfoLocationPattern = Pattern.compile("^([\\w.]+):([-\\w.\\/\\\\]+\\.txt\\.gz)$");
    //TODO: Add support for tabix VCF

    private Annotator annotatorChain;
    private boolean annotationsProcessed = false;
    private Collection<VCFHeaderLine> newHeaderLines;


    public void processAnnotations(final String[] annotationStrings, final String[] resources) throws IOException {
        Assert.isTrue(!annotationsProcessed, "Annotations already processed");
        buildResourceLocationMap(resources);

        final LinkedList<Annotator> annotators = Lists.newLinkedList();
        this.types = Lists.newLinkedList();
//        boolean snpEffRequired = false;
        boolean veerCountResourceBasedAnnotatorSupplied = false;
        boolean veerCountSelfBasedAnnotatorSupplied = false;
        boolean veerScoreAnnotatorSupplied = false;

        for (final String annotatorString : annotationStrings){
            final boolean isVeerCountResource = isVEERCountResource(annotatorString);
            if(annotatorString.equals(VEERCountAnnotator.ID) || isVeerCountResource) {
                addAnnotatorFirst(annotators, annotatorString);
                if (isVeerCountResource) {
                    veerCountResourceBasedAnnotatorSupplied = true;
                } else if (annotatorString.equals(VEERCountAnnotator.ID)) {
                    veerCountSelfBasedAnnotatorSupplied = true;
                }
            } else {
                addAnnotator(annotators, annotatorString);
                if (annotatorString.equals(VEERScoreAnnotator.ID)) {
                    veerScoreAnnotatorSupplied = true;
                }
            }
        }

        if (veerCountResourceBasedAnnotatorSupplied && veerCountSelfBasedAnnotatorSupplied) {
            throw new IllegalArgumentException("VEER count annotation from file was requested in conjunction with request for " + VEERCountAnnotator.ID + " INFO field from resource file");
        }
        if (veerScoreAnnotatorSupplied && !veerCountSelfBasedAnnotatorSupplied && !veerCountResourceBasedAnnotatorSupplied) {
            addAnnotatorFirst(annotators, VEERCountAnnotator.ID);
        }

//        setVariantUtils(Lists.newArrayList(Collections2.transform(getVcfReaders(), new Function<VCFFileReader, VariantUtils>() {
//            @Nullable
//            public VariantUtils apply(final VCFFileReader vcfFileReader) {
//                return new VariantUtils(vcfFileReader);
//            }
//        })));
        setAnnotatorChain(annotators);
        annotationsProcessed = true;
    }

    private boolean isVEERCountResource(final String typeStr) {
        return typeStr.startsWith("Resource") && typeStr.contains(VEERCountAnnotator.ID);
    }

    private void addAnnotatorFirst(final LinkedList<Annotator> annotators, final String newAnnotatorString) throws IOException {
        if(newAnnotatorString.startsWith("Resource")) {
            final List<ResourceAnnotator> resourceAnnotators = buildResourceAnnotators(newAnnotatorString);
            for (final ResourceAnnotator resourceAnnotator : resourceAnnotators) {
                annotators.addFirst(resourceAnnotator);
            }
        } else {
            annotators.addFirst(getAnnotator(newAnnotatorString));
        }
        types.addFirst(newAnnotatorString);
    }

    private void addAnnotator(final LinkedList<Annotator> annotators, final String newAnnotatorString) throws IOException {
        if(newAnnotatorString.startsWith("Resource")) {
            annotators.addAll(buildResourceAnnotators(newAnnotatorString));
        }else{
            annotators.add(getAnnotator(newAnnotatorString));
        }
        types.add(newAnnotatorString);
    }

    private void setAnnotatorChain(final LinkedList<Annotator> annotators) {
        LOGGER.info("Adding annotators in the following order: " + types);
        final Annotator firstAnnotator = annotators.remove();
        Annotator previousAnnotator = firstAnnotator;
        for(Annotator annotator : annotators){
            previousAnnotator.setNext(annotator);
            previousAnnotator = annotator;
        }
        annotatorChain=firstAnnotator;
    }

    private List<ResourceAnnotator> buildResourceAnnotators(final String resourceAnnotatorString) throws IOException {
        final List<ResourceAnnotator> resourceAnnotators = Lists.newArrayList();
        final Matcher m = annotatorResourcePattern.matcher(resourceAnnotatorString);
        if (m.matches()) {
            final String resourceKey = m.group(1);
            final String infoFields = m.group(2);

            for (final String infoField : infoFields.split(",")) {
                resourceAnnotators.add(buildResourceAnnotator(resourceKey, infoField));
            }
        } else {
            LOGGER.error("Malformed suspected resource annotation: " + m);
            throw new IllegalArgumentException(resourceAnnotatorString);
        }
        return resourceAnnotators;
    }

    private ResourceAnnotator buildResourceAnnotator(String sourceName, String infoField) throws IOException {
        VariantResource variantResource = resourceLocationMap.getLocation(sourceName);
        Assert.isTrue(variantResource.hasInfoLine(infoField), variantResource.getResourceName() + " does not have field " + infoField);

        ResourceAnnotator resourceAnnotator = getPrototypeResourceAnnotator();
        resourceAnnotator.setVariantResource(variantResource);
        resourceAnnotator.setSourceInfoHeaderLine(variantResource.getInfoHeaderLine(infoField));
        return resourceAnnotator;
    }

    private void buildResourceLocationMap(final String[] resources) throws IOException {
        if(resources != null) {
            for (final String resourceLocation : resources) {
                addResource(resourceLocation);
            }
        }
    }

    private void addResource(final String resourceLocation) throws IOException {
        final VariantResource variantResource = getVariantResource(resourceLocation);
        resourceLocationMap.putLocation(variantResource.getResourceName(), variantResource);
    }

    private VariantResource getVariantResource(String resourceLocation) throws IOException {
        Matcher m = vcfResourceInfoLocationPattern.matcher(resourceLocation);
        if(m.matches()){
            String resourceName = m.group(1);
            File location = new File(m.group(2));
            return new VCFVariantResource(resourceName, location, getQueryType());
        }
        m = tabixTestResourceInfoLocationPattern.matcher(resourceLocation);
        if(m.matches()){
            String resourceName = m.group(1);
            File location = new File(m.group(2));
            return new TabixVariantResource(resourceName, location, getQueryType());
        }
        throw new IllegalArgumentException("Invalid resource location: " + resourceLocation);
    }

    private ResourceAnnotator getPrototypeResourceAnnotator() {
        for(Annotator annotator : annotators){
            if(annotator instanceof ResourceAnnotator){
                ResourceAnnotator resourceAnnotatorPrototype = (ResourceAnnotator) annotator;
                return ctx.getBean(resourceAnnotatorPrototype.getClass());
            }
        }
        throw new IllegalArgumentException("No ResourceAnnotator present");
    }

    private Annotator getAnnotator(final String annotatorString) {
        final Annotator annotator = getAnnotatorMap().get(annotatorString);
        Assert.notNull(annotator, "Unrecognized annotator: " + annotatorString + ". Should be one of " +
                getAnnotatorMap().keySet());
        return annotator;
    }

    private Map<String, Annotator> getAnnotatorMap() {
        final Map<String, Annotator> map = Maps.newHashMap();
        for(final Annotator annotator : annotators){
            map.put(annotator.getVCFHeaderLine().getID(), annotator);
        }
        return map;
    }

//    public String getSparkMaster() {
//        return sparkMaster;
//    }
//
//    public void setSparkMaster(final String sparkMaster) {
//        this.sparkMaster = sparkMaster;
//    }

    public void setSparkContext(final JavaSparkContext sparkContext) {
        this.sparkContext = sparkContext;
    }


    public String getReferenceGenomeFilepath() {
        return referenceGenomeFilepath;
    }

    public void setReferenceGenomeFilepath(final String referenceGenomeFilepath) {
        this.referenceGenomeFilepath = referenceGenomeFilepath;
    }

    /**
     * Add a new INFO called CatAB to the VCF Header and process each variant context and add CatAB as well as
     * CDD and Best Transcript. May think about moving this to some sort of chained annotation in the future.
     *

     * @throws IOException -
     */
    void annotate() throws IOException {
        final FlatMapFunction<Iterator<Tuple2<Long, String>>, String> getFirstStringInPartition = new FlatMapFunction<Iterator<Tuple2<Long, String>>, String>() {
            @Override
            public Iterable<String> call(final Iterator<Tuple2<Long, String>> iter) throws Exception {
                final List<String> dummyList = Lists.newArrayList();
                if (iter.hasNext()) {
                    final Tuple2<Long, String> tuple = iter.next();
                    final long index = tuple._1();
                    final String first = tuple._2();
                    dummyList.add(index + ":" + first);
                }
                return dummyList;
            }
        };
        final FlatMapFunction<Iterator<Tuple2<Long, VariantContext>>, String> getFirstVariantContextInPartition = new FlatMapFunction<Iterator<Tuple2<Long, VariantContext>>, String>() {
            @Override
            public Iterable<String> call(final Iterator<Tuple2<Long, VariantContext>> iter) throws Exception {
                final List<String> dummyList = Lists.newArrayList();
                if (iter.hasNext()) {
                    final Tuple2<Long, VariantContext> tuple = iter.next();
                    final long index = tuple._1();
                    final String first = tuple._2().toString();
                    dummyList.add(index + ":" + first);
                }
                return dummyList;
            }
        };
        final FlatMapFunction<Iterator<Tuple2<Long, Tuple2<VCFEncoder, VariantContext>>>, String> getFirstEncoderVariantContextTupleInPartition = new FlatMapFunction<Iterator<Tuple2<Long, Tuple2<VCFEncoder, VariantContext>>>, String>() {
            @Override
            public Iterable<String> call(final Iterator<Tuple2<Long, Tuple2<VCFEncoder, VariantContext>>> iter) throws Exception {
                final List<String> dummyList = Lists.newArrayList();
                if (iter.hasNext()) {
                    final Tuple2<Long, Tuple2<VCFEncoder, VariantContext>> tuple = iter.next();
                    final long index = tuple._1();
                    final String encoder = tuple._2()._1().toString();
                    final String variant = tuple._2()._2().toString();
                    dummyList.add(index + ":" + encoder + "; " + variant);
                }
                return dummyList;
            }
        };

        final Broadcast<Annotator> annotatorChainBroadcast = sparkContext.broadcast(annotatorChain);
        final List<String> outputFilepaths = getOutputFilepaths();

        final JavaRDD<String> lines = getVcfLines();
        // separate header lines... alternatively feed in two different files
        final JavaRDD<String> headerLines = lines.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(final String line) throws Exception {
                return line.startsWith("#");
            }
        });
        final JavaRDD<String> variantLines = lines.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(final String line) throws Exception {
                return !line.startsWith("#");
            }
        });
        final JavaRDD<String> variantChrs = lines.map(new Function<String, String>() {
            @Override
            public String call(final String line) throws Exception {
                return line.split("\t")[0];
            }
        });
        final JavaPairRDD<String, String> variantLinesByChr = variantChrs.zip(variantLines);
        final JavaPairRDD<String, VariantContext> variantsByChr = variantLinesByChr.mapValues(new Function<String, VariantContext>() {
            @Override
            public VariantContext call(final String line) throws Exception {
                final VCFCodec codec = new VCFCodec();
                return codec.decode(line);
            }
        });
        final JavaPairRDD<String, VariantContext> annotatedVariantsByChr = variantsByChr.mapValues(new Function<VariantContext, VariantContext>() {
            @Override
            public VariantContext call(final VariantContext variant) throws Exception {
                return annotatorChainBroadcast.value().createNewVariantContext(variant);
            }
        });
        final VCFHeader header = new VCFFileReader(new File(vcfFilepaths.get(0))).getFileHeader();
        for (final VCFHeaderLine headerLine : getNewHeaderLines()) {
            header.addMetaDataLine(headerLine);
        }
        final VCFEncoder encoder = new VCFEncoder(header, false, false);
        final Broadcast<VCFEncoder> encoderBroadcast = sparkContext.broadcast(encoder);
        final JavaPairRDD<String, String> annotatedVariantLinesByChr = annotatedVariantsByChr.mapValues(new Function<VariantContext, String>() {
            @Override
            public String call(final VariantContext variant) throws Exception {
                return encoderBroadcast.value().encode(variant);
            }
        });
        final List<String> newHeaderLines = extractHeaderLineStrings(header);





        final JavaRDD<VCFFileReader> readers = getVcfReaders();
        final JavaPairRDD<Long, VCFFileReader> readersWithIndex = readers.zipWithIndex().mapToPair(new PairFunction<Tuple2<VCFFileReader, Long>, Long, VCFFileReader>() {
            @Override
            public Tuple2<Long, VCFFileReader> call(final Tuple2<VCFFileReader, Long> tuple) throws Exception {
                return tuple.swap();
            }
        });
        readersWithIndex.cache();
        final JavaPairRDD<Long, VariantContext> variantsWithIndex = readersWithIndex.flatMapValues(new Function<VCFFileReader, Iterable<VariantContext>>() {
            @Override
            public Iterable<VariantContext> call(final VCFFileReader reader) throws Exception {
                final List<VariantContext> list = Lists.newLinkedList();
                final VCFHeader header = reader.getFileHeader();
                for (final VariantContext variant : reader) {
                    list.add(variant.fullyDecode(header, false));
                }
                return list;
            }
        });
        final long numVariants = variantsWithIndex.count();
        final int numPartitionsToUse = (int) (numVariants / NUM_VARIANTS_PER_PARTITION);
        LOGGER.info("Repartitioning decodedVariantsWithIndex: " + numVariants + " over " + numPartitionsToUse + " partitions");
//        variantsWithIndex.unpersist();
//        variantsWithIndex.cache();
        final JavaPairRDD<Long, VariantContext> variantsWithIndexRepartitioned = variantsWithIndex.repartition(numPartitionsToUse);
        //.repartitionAndSort
//        LOGGER.info("Printing first entry of each partition from variantsWithIndex (" + variantsWithIndex.partitions().size() + " partitions total)");
//        variantsWithIndex.persist(StorageLevel.MEMORY_AND_DISK());
//        List<String> debugPartitionOutput = variantsWithIndex.mapPartitions(getFirstVariantContextInPartition).collect();
//        for (int i = 0; i < debugPartitionOutput.size(); i++) {
//            LOGGER.info("Partition " + i + ": " + debugPartitionOutput.get(i));
//        }
        final JavaPairRDD<Long, VariantContext> annotatedVariantsWithIndex = variantsWithIndexRepartitioned.mapValues(new Function<VariantContext, VariantContext>() {
            @Override
            public VariantContext call(VariantContext variant) throws Exception {
                return annotatorChainBroadcast.value().createNewVariantContext(variant);
            }
        });
//        LOGGER.info("Printing first entry of each partition from annotatedVariantsWithIndex (" + annotatedVariantsWithIndex.partitions().size() + " partitions total)");
//        annotatedVariantsWithIndex.persist(StorageLevel.MEMORY_AND_DISK());
//        debugPartitionOutput = annotatedVariantsWithIndex.mapPartitions(getFirstVariantContextInPartition).collect();
//        for (int i = 0; i < debugPartitionOutput.size(); i++) {
//            LOGGER.info("Partition " + i + ": " + debugPartitionOutput.get(i));
//        }

        final JavaPairRDD<Long, VCFHeader> newHeadersWithIndex = readersWithIndex.mapValues(new Function<VCFFileReader, VCFHeader>() {
            @Override
            public VCFHeader call(final VCFFileReader reader) throws Exception {
                final VCFHeader header = reader.getFileHeader();
                for (final VCFHeaderLine headerLine : getNewHeaderLines()) {
                    header.addMetaDataLine(headerLine);
                }
                return header;
            }
        });
        LOGGER.info("newHeadersWithIndex:\n" + newHeadersWithIndex.toDebugString());
        LOGGER.info("Caching newHeadersWithIndex");
        newHeadersWithIndex.cache();
        final JavaPairRDD<Long, String> headerLinesWithIndex = newHeadersWithIndex.flatMapValues(new Function<VCFHeader, Iterable<String>>() {
            @Override
            public Iterable<String> call(final VCFHeader header) throws Exception {
                final List<String> headerLines = Lists.newArrayList(Iterables.transform(header.getMetaDataInSortedOrder(), new com.google.common.base.Function<VCFHeaderLine, String>() {
                    @Override
                    public String apply(@Nullable final VCFHeaderLine line) {
                        return "##" + line.toString();
                    }
                }));
                final List<String> headerFields = Lists.newArrayList(Iterables.transform(header.getHeaderFields(), new com.google.common.base.Function<VCFHeader.HEADER_FIELDS, String>() {
                    @Override
                    public String apply(final @Nullable VCFHeader.HEADER_FIELDS header_fields) {
                        return header_fields.toString();
                    }
                }));
                headerFields.add("FORMAT");
                headerFields.addAll(header.getSampleNamesInOrder());
                headerLines.add("#" + StringUtils.join(headerFields, "\t"));
                return headerLines;
            }
        });
        headerLinesWithIndex.cache();
//        LOGGER.info("Printing header lines:");
//        for (final String headerLine : headerLinesWithIndex.values().collect()) {
//            LOGGER.info(headerLine);
//        }
        final JavaPairRDD<Long, VCFEncoder> encodersWithIndex = newHeadersWithIndex.mapValues(new Function<VCFHeader, VCFEncoder>() {
            @Override
            public VCFEncoder call(final VCFHeader header) throws Exception {
                return new VCFEncoder(header, false, false);
            }
        });
        LOGGER.info("encodersWithIndex: " + encodersWithIndex.toDebugString());
//        LOGGER.info("Caching encodersWithIndex");
//        encodersWithIndex.cache(); // remove after debugging
//        headersWithIndex.unpersist(); // remove after debugging
//        for (final VCFEncoder encoder : encodersWithIndex.values().collect()) {
//            LOGGER.info("After encoding, header: " + encoder.getVCFHeader());
//        }
        final JavaPairRDD<Long, Tuple2<VCFEncoder, VariantContext>> encodersAndAnnotatedVariantsWithIndex;
        final Optional<Integer> numPartitions = getNumPartitions();
        if (numPartitions.isPresent()) {
            encodersAndAnnotatedVariantsWithIndex = encodersWithIndex.join(annotatedVariantsWithIndex);
        } else {
            encodersAndAnnotatedVariantsWithIndex = encodersWithIndex.join(annotatedVariantsWithIndex, numPartitions.get());
        }

        LOGGER.info("encodersAndAnnotatedVariantsWithIndex: " + encodersAndAnnotatedVariantsWithIndex.toDebugString());
//        LOGGER.info("Printing first entry of each partition from encodersAndAnnotatedVariantsWithIndex (" + encodersAndAnnotatedVariantsWithIndex.partitions().size() + " partitions total)");
////        encodersAndAnnotatedVariantsWithIndex.persist(StorageLevel.MEMORY_AND_DISK());
//        debugPartitionOutput = encodersAndAnnotatedVariantsWithIndex.mapPartitions(getFirstEncoderVariantContextTupleInPartition).collect();
//        for (int i = 0; i < debugPartitionOutput.size(); i++) {
//            LOGGER.info("Partition " + i + ": " + debugPartitionOutput.get(i));
//        }
        final JavaPairRDD<Long, String> variantLinesWithIndex = encodersAndAnnotatedVariantsWithIndex.mapValues(new Function<Tuple2<VCFEncoder, VariantContext>, String>() {
            @Override
            public String call(final Tuple2<VCFEncoder, VariantContext> tuple) throws Exception {
                final VCFEncoder encoder = tuple._1();
                final VariantContext variant = tuple._2();
                try {
                    return encoder.encode(variant);
                } catch (final IllegalStateException e) {
                    throw new IllegalStateException("Encoder header: " + encoder.getVCFHeader() + "\nVariant: " + variant, e);
                }
            }
        })
                //.partitionBy(new VariantPartitioner())
        ;
        LOGGER.info("variantLinesWithIndex: " + variantLinesWithIndex.toDebugString());
        variantLinesWithIndex.persist(StorageLevel.MEMORY_AND_DISK());
        variantsWithIndex.unpersist();
//        LOGGER.info("Printing first entry of each partition from variantLinesWithIndex (" + variantLinesWithIndex.partitions().size() + " partitions total)");
//        debugPartitionOutput = variantLinesWithIndex.mapPartitions(getFirstStringInPartition).collect();
//        for (int i = 0; i < debugPartitionOutput .size(); i++) {
//            LOGGER.info("Partition " + i + ": " + debugPartitionOutput.get(i));
//        }
//        LOGGER.info("Collecting / printing 500 lines from variantLinesWithIndex");
//        List<String> lines = variantLinesWithIndex.values().partitions().get(0). take(500);
//        for (int i = 0; i < 500; i++) {
//            LOGGER.info("Line #" + (i+1) + ": " + lines.get(i));
//        }
//        LOGGER.info("Repartitioning variantLinesWithIndex");
//        final JavaPairRDD<Long, String> repartitionedVariantLinesWithIndex = variantLinesWithIndex.partitionBy(new VariantPartitioner());
//        LOGGER.info("repartitionedVariantLinesWithIndex: " + repartitionedVariantLinesWithIndex.toDebugString());
//        LOGGER.info("Collecting / printing 500 lines from repartitionedVariantLinesWithIndex");
//        lines = repartitionedVariantLinesWithIndex.values().take(500);
//        for (int i = 0; i < 500; i++) {
//            LOGGER.info("Line #" + (i+1) + ": " + lines.get(i));
//        }
//        final JavaPairRDD<Long, String> linesWithIndex = variantLinesWithIndex.union(headerLinesWithIndex);
//        LOGGER.info("linesWithIndex: " + linesWithIndex.toDebugString());
//        linesWithIndex.persist(StorageLevel.MEMORY_AND_DISK());
//        LOGGER.info("Collecting / printing 500 lines from linesWithIndex");
//        List<String> lines = linesWithIndex.values().take(500);
////        encodersWithIndex.unpersist(); // remove after debugging
//        for (int i = 0; i < 500; i++) {
//            LOGGER.info("Line #" + (i+1) + ": " + lines.get(i));
//        }
//        final JavaPairRDD<Long, Iterable<String>> linesGroupedByIndex = linesWithIndex.groupByKey();
//        final JavaPairRDD<Long, String> outputFilepathsWithIndex = outputFilepathsRDD.zipWithIndex().mapToPair(new PairFunction<Tuple2<String, Long>, Long, String>() {
//            @Override
//            public Tuple2<Long, String> call(final Tuple2<String, Long> tuple) throws Exception {
//                return tuple.swap();
//            }
//        });
//        LOGGER.info("outputFilePathsWithIndex: " + outputFilepathsWithIndex.toDebugString());
//        final JavaPairRDD<String, String> filepathsAndLines = JavaPairRDD.fromJavaRDD(outputFilepathsWithIndex.join(linesWithIndex).values());
//        LOGGER.info("filepathsAndLines: " + filepathsAndLines.toDebugString());
//        LOGGER.info("Caching filepathsAndLines");
//        filepathsAndLines.cache(); // memory?
//        filepathsAndLines.persist(StorageLevel.MEMORY_AND_DISK());
//        final Map<String, JavaRDD<String>> filepathToLinesMap = Maps.newHashMap();
        final Map<String, VCFLineRDDHolder> filepathToLinesMap = Maps.newHashMap();
        for (int i = 0; i < outputFilepaths.size(); i++) {
            final int fileIndex = i;
            final Function<Tuple2<Long, String>, Boolean> matchesIndex = new Function<Tuple2<Long, String>, Boolean>() {
                @Override
                public Boolean call(final Tuple2<Long, String> tuple) throws Exception {
                    return tuple._1() == fileIndex;
                }
            };
            final String filepath = outputFilepaths.get(i);
            LOGGER.info("Populating filepathToLinesMap for output file " + filepath);
            final JavaRDD<String> headerLines = headerLinesWithIndex.filter(matchesIndex).values();/*.sortBy(new Function<String, String>() {
                @Override
                public String call(final String line) throws Exception {
                    return line;
                }
            }, true, numPartitions.isPresent() ? numPartitions.get() : headerLinesWithIndex.partitions().size()); */
            final JavaRDD<String> variantLines = variantLinesWithIndex.filter(matchesIndex).values().sortBy(new Function<String, Integer>() {
                @Override
                public Integer call(String s) throws Exception {
                    return Integer.parseInt(s.split("\t")[1]);
                }
            }, true, numPartitions.isPresent() ? numPartitions.get() : variantLinesWithIndex.partitions().size());
            filepathToLinesMap.put(filepath, new VCFLineRDDHolder(headerLines, variantLines));
        }
//        final ExecutorService executorService = Executors.newFixedThreadPool(outputFilepaths.size());
        LOGGER.info("Writing output to the following files:");
        for (String filepath : filepathToLinesMap.keySet()) {
            if (filepath.startsWith("hdfs:/") && filepath.charAt(6) != '/') { // sometimes it comes out with only one /
                final String newFilepath = filepath.substring(0, 6) + '/' + filepath.substring(6);
                filepathToLinesMap.put(newFilepath, filepathToLinesMap.get(filepath));
                filepathToLinesMap.remove(filepath);
                filepath = newFilepath;
            }
            LOGGER.info("\t" + filepath);
            Assert.isTrue(Files.notExists(Paths.get(filepath)), "Output file    " + filepath + " already exists");
//            executorService.execute(new Runnable() {
//                @Override
//                public void run() {
                    filepathToLinesMap.get(filepath).saveAsTextFile(filepath);
//                }
//            });
        }
//        executorService.shutdown();
//        for (final String filepath : filepathToLinesMap.keySet()) {
//            saveFile(filepath, filepathToLinesMap);
//        }


//        outputFilepathsRDD.foreach(new VoidFunction<String>() {
//            @Override
//            public void call(final String filepath) throws Exception {
//                filepathToLinesMap.get(filepath).saveAsTextFile(filepath);
//            }
//        });
//        final JavaPairRDD<String, Iterable<String>> filepathsAndLines = JavaPairRDD.fromJavaRDD(outputFilepathsWithIndex.join(linesGroupedByIndex).values());
//        outputFilepathsRDD.foreach(new VoidFunction<String>() {
//            @Override
//            public void call(final String filepath) throws Exception {
//                final JavaRDD<String> lines = filepathsAndLines.filter(new Function<Tuple2<String, String>, Boolean>() {
//                    @Override
//                    public Boolean call(final Tuple2<String, String> tuple) throws Exception {
//                        return tuple._1().equals(filepath);
//                    }
//                }).values();
//                lines.saveAsTextFile(filepath);
//            }
//        });
//        filepathsAndLines.foreach(new VoidFunction<Tuple2<String, Iterable<String>>>() {
//            @Override
//            public void call(final Tuple2<String, Iterable<String>> tuple) throws Exception {
//                final PrintWriter writer = new PrintWriter(new PrintStream(tuple._1()), true);
//                for (final String line : tuple._2()) {
//                    writer.println(line);
//                }
//                writer.close();
//            }
//        });

        //HIGHLY PARALLELIZED ATTEMPT 2
        //        final JavaRDD<MyVCFWriter> writers = getVariantContextWriters();
//
//        final JavaPairRDD<Long, VCFFileReader> readersWithIndex = readers.zipWithIndex().mapToPair(new PairFunction<Tuple2<VCFFileReader, Long>, Long, VCFFileReader>() {
//            @Override
//            public Tuple2<Long, VCFFileReader> call(final Tuple2<VCFFileReader, Long> tuple) throws Exception {
//                return tuple.swap();
//            }
//        });
//        final JavaPairRDD<Long, VariantContext> variantsWithIndex = readersWithIndex.flatMapValues(new Function<VCFFileReader, Iterable<VariantContext>>() {
//            @Override
//            public Iterable<VariantContext> call(final VCFFileReader reader) throws Exception {
//                return reader;
//            }
//        });
//        final JavaPairRDD<Long, VariantContext> annotatedVariantsWithIndex = variantsWithIndex.mapValues(new Function<VariantContext, VariantContext>() {
//            @Override
//            public VariantContext call(VariantContext variant) throws Exception {
//                return annotatorChainBroadcast.value().createNewVariantContext(variant);
//            }
//        });
//        final JavaPairRDD<Long, Iterable<VariantContext>> groupedAnnotatedVariants = annotatedVariantsWithIndex.groupByKey();
//        final JavaPairRDD<Long, MyVCFWriter> writersWithIndex = writers.zipWithIndex().mapToPair(new PairFunction<Tuple2<MyVCFWriter, Long>, Long, MyVCFWriter>() {
//            @Override
//            public Tuple2<Long, MyVCFWriter> call(final Tuple2<MyVCFWriter, Long> tuple) throws Exception {
//                return tuple.swap();
//            }
//        });
//        final JavaPairRDD<MyVCFWriter, Iterable<VariantContext>> writersAndAnnotatedVariants = JavaPairRDD.fromJavaRDD(writersWithIndex.join(groupedAnnotatedVariants).values());
//        writersAndAnnotatedVariants.foreach(new VoidFunction<Tuple2<MyVCFWriter, Iterable<VariantContext>>>() {
//            @Override
//            public void call(final Tuple2<MyVCFWriter, Iterable<VariantContext>> tuple) throws Exception {
//                final MyVCFWriter writer = tuple._1();
//                final Iterable<VariantContext> variants = tuple._2();
//                for (final VariantContext variant : variants) {
//                    writer.add(variant);
//                }
//                writer.write();
//                writer.close();
//            }
//        });

//        final JavaRDD<VariantUtils> variantUtilsRDD = readers.map(new Function<VCFFileReader, VariantUtils>() {
//            @Override
//            public VariantUtils call(final VCFFileReader reader) throws Exception {
//                return new VariantUtils(reader);
//            }
//        });
//        int maxNumPartitions = Math.min(writers.getNumPartitions(), variantUtilsRDD.getNumPartitions());
//        writers.coalesce(maxNumPartitions, false);
//        variantUtilsRDD.coalesce(maxNumPartitions, false);
//        final JavaPairRDD<VariantContextWriter, VariantUtils> writersAndVariantUtils = writers.zip(variantUtilsRDD);
//        maxNumPartitions = Math.min(writersAndVariantUtils.getNumPartitions(), readers.getNumPartitions());
//        writersAndVariantUtils.coalesce(maxNumPartitions, false);
//        readers.coalesce(maxNumPartitions, false);
//        final JavaPairRDD<Tuple2<VariantContextWriter, VariantUtils>, VCFFileReader> writersVariantUtilsAndReaders = writersAndVariantUtils.zip(readers);
//        final JavaPairRDD<Tuple2<VariantContextWriter, VariantUtils>, VariantContext> writersVariantUtilsAndVariants = writersVariantUtilsAndReaders.flatMapValues(new Function<VCFFileReader, Iterable<VariantContext>>() {
////        final JavaPairRDD<Tuple2<VariantContextWriter, VariantUtils>, VariantContext> writersVariantUtilsAndVariants = writers.zip(variantUtilsRDD).zip(readers).flatMapValues(new Function<VCFFileReader, Iterable<VariantContext>>() {
//            @Override
//            public Iterable<VariantContext> call(final VCFFileReader reader) throws Exception {
//                return reader;
//            }
//        });

//        final int maxNumPartitions = Math.min(writers.getNumPartitions(), readers.getNumPartitions());
//        if (writers.getNumPartitions() > maxNumPartitions) {
//            writers.coalesce(maxNumPartitions, false);
//        } else if (readers.getNumPartitions() > maxNumPartitions) {
//            readers.coalesce(maxNumPartitions, false);
//        }
//        final JavaPairRDD<VariantContextWriter, VCFFileReader> writersAndReadersRDD = writers.zip(readers);
////        final JavaPairRDD<VariantContextWriter, VariantContext> writersAndVariantsRDD = writersAndReadersRDD.flatMapValues(new Function<VCFFileReader, Iterable<VariantContext>>() {
////            @Override
////            public Iterable<VariantContext> call(final VCFFileReader reader) throws Exception {
////                return reader;
////            }
////        });
//
//        try {
////            writersVariantUtilsAndVariants.foreach(new VoidFunction<Tuple2<Tuple2<VariantContextWriter, VariantUtils>, VariantContext>>() {
////                @Override
////                public void call(final Tuple2<Tuple2<VariantContextWriter, VariantUtils>, VariantContext> tuple) throws Exception {
////                    final VariantContextWriter writer = tuple._1()._1();
////                    final VariantUtils variantUtils = tuple._1()._2();
////                    final VariantContext variant = tuple._2();
////                    final VariantContext annotatedVariant = variantUtils.buildAnnotatedVariantContext(variant, annotatorChainBroadcast.value());
////                    writer.add(annotatedVariant);
////                }
////            });
//            writersAndReadersRDD.foreach(new VoidFunction<Tuple2<VariantContextWriter, VCFFileReader>>() {
//                 public void call(final Tuple2<VariantContextWriter, VCFFileReader> tuple) throws Exception {
//                     final VariantContextWriter writer = tuple._1();
//                     final VCFFileReader reader = tuple._2();
//                     final VariantUtils variantUtils = new VariantUtils(reader);
//                     // iterative
//                     for (final VariantContext variant : reader) {
//                         writer.add(variantUtils.buildAnnotatedVariantContext(variant, annotatorChainBroadcast.value()));
//                     }
//                 }
//            });
////                    // as RDD - fails, can't do nested RDDs
//////                    final List<VariantContext> variantsList = Lists.newArrayList(reader);
//////                    Assert.notNull(variantsList);
//////                    for (final VariantContext variantContext : variantsList) {
//////                        Assert.notNull(variantContext);
//////                    }
//////                    final JavaRDD<VariantContext> variants = sparkContext.parallelize(variantsList);
//////                    variants.foreach(new VoidFunction<VariantContext>() {
//////                        public void call(final VariantContext variant) throws Exception {
//////                            writer.add(variantUtils.buildAnnotatedVariantContext(variant, annotatorChainBroadcast.value()));
//////                        }
//////                    });
//////                    final JavaRDD<VariantContext> annotatedVariants = variants.map(new Function<VariantContext, VariantContext>() {
//////                        public VariantContext call(final VariantContext variant) throws Exception {
//////                            return variantUtils.buildAnnotatedVariantContext(variant, annotatorChainBroadcast.value());
//////                        }
//////                    });
//////                    annotatedVariants.foreach(new VoidFunction<VariantContext>() {
//////                        public void call(final VariantContext annotatedVariant) throws Exception {
//////                            writer.add(annotatedVariant);
//////                        }
//////                    });
////                    // workaround for nested RDD
////
////                    writer.close();
////                }
////            });
//        } catch (final IllegalArgumentException e) {
//            final int numReaderPartitions = readers.getNumPartitions();
//            final int numWriterPartitions = writers.getNumPartitions();
//            if (numReaderPartitions != numWriterPartitions) {
//                throw new IllegalArgumentException("Different numbers of partitions for readers (" + numReaderPartitions + ") and writers (" + numWriterPartitions + "). Debug strings: \n" + readers.toDebugString() + "\n" + writers.toDebugString());
//            } else {
//                throw e;
//            }
//        }
//        writers.foreach(new VoidFunction<VariantContextWriter>() {
//            @Override
//            public void call(final VariantContextWriter writer) throws Exception {
//                writer.close();
//            }
//        });
////        readersAndWritersRDD.foreach(new VoidFunction<Tuple2<VCFFileReader, VariantContextWriter>>() {
////            public void call(final Tuple2<VCFFileReader, VariantContextWriter> readerWriterTuple) throws Exception {
////                final VCFFileReader reader = readerWriterTuple._1();
////                final VariantContextWriter writer = readerWriterTuple._2();
////                final JavaRDD<VariantContext> variantContexts = sparkContext.parallelize(Lists.newArrayList(reader));
////                final VariantUtils variantUtils = new VariantUtils(reader);
////                final JavaRDD<VariantContext> annotatedVariantContexts = variantContexts.map(new org.apache.spark.api.java.function.Function<VariantContext, VariantContext>() {
////                    public VariantContext call(final VariantContext variantContext) throws Exception {
////                        return variantUtils.buildAnnotatedVariantContext(variantContext, annotatorChainBroadcast.value());
////                    }
////                });
////                annotatedVariantContexts.foreach(new VoidFunction<VariantContext>() {
////                    public void call(final VariantContext variantContext) throws Exception {
////                        writer.add(variantContext);
////                    }
////                });
////            }
////        });
////        readersAndWritersRDD.values().foreach(new VoidFunction<VariantContextWriter>() {
////            public void call(final VariantContextWriter variantContextWriter) throws Exception {
////                variantContextWriter.close();
////            }
////        });

        //Do final Log
        Annotator annotator = annotatorChain;
        while (annotator != null) {
            annotator.logFinalStatus();
            annotator = annotator.next();
        }


//        final String currentChr = "ZZZZZZ"; //Could look at number of bases covered and then do a progress meter but might be slow so wait.
//        long counter = 0;
//        for (VariantContext variantContext : readers) {
////            counter++;
////            if(!variantContext.getChr().equals(currentChr)){
////                LOGGER.info("Current Chromosome " + variantContext.getChr());
////                currentChr=variantContext.getChr();
////            }
//            VariantContext annotatedVariantContext = variantUtils.buildAnnotatedVariantContext(variantContext, annotatorChain);
//            getVariantContextWriters().add(annotatedVariantContext);
//        }
//        LOGGER.info("Number of variants: " + counter);
//        getVariantContextWriters().close();
//        closeVariantContextWriters();

    }

    private List<String> extractHeaderLineStrings(final VCFHeader header) {
        final List<String> headerLines = Lists.newArrayList(Iterables.transform(header.getMetaDataInSortedOrder(), new com.google.common.base.Function<VCFHeaderLine, String>() {
            @Override
            public String apply(@Nullable final VCFHeaderLine line) {
                return "##" + line.toString();
            }
        }));
        final List<String> headerFields = Lists.newArrayList(Iterables.transform(header.getHeaderFields(), new com.google.common.base.Function<VCFHeader.HEADER_FIELDS, String>() {
            @Override
            public String apply(final @Nullable VCFHeader.HEADER_FIELDS header_fields) {
                return header_fields.toString();
            }
        }));
        headerFields.add("FORMAT");
        headerFields.addAll(header.getSampleNamesInOrder());
        headerLines.add("#" + StringUtils.join(headerFields, "\t"));
        return headerLines;
    }

//    @Async
//    private void saveFile(final String filepath, final Map<String, JavaRDD<String>> filepathToLinesMap) {
//
//    }

//    private void closeVariantContextWriters() {
//        final List<VariantContextWriter> writers = getVariantContextWriters();
//        for (final VariantContextWriter writer : writers) {
//            writer.close();
//        }
//    }

    VariantContextWriter buildVCFWriter(final VCFFileReader vcfFileReader, final File outputFile, final SAMSequenceDictionary refSeqDict) throws PipelineException {
        final VariantContextWriter writer
                = new VariantContextWriterBuilder()
                .setOutputFile(outputFile).setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
//                .setReferenceDictionary(getRefDict()).build();
                .setReferenceDictionary(refSeqDict)
                .setOptions(EnumSet.noneOf(Options.class))
                .build();

        final VCFHeader vcfHeader = new VCFHeader(vcfFileReader.getFileHeader());
        validateReference(vcfHeader, refSeqDict);

        Annotator annotator = annotatorChain;
        while (annotator != null) {
            final VCFCompoundHeaderLine vcfHeaderLine = annotator.getVCFHeaderLine();
            final boolean alreadyAnnotated = lineAlreadyExists(vcfHeader, vcfHeaderLine);
            Assert.isTrue(!alreadyAnnotated, "VCF already has annotation - " + vcfHeaderLine);
            vcfHeader.addMetaDataLine(vcfHeaderLine);
            annotator = annotator.next();
        }

        vcfHeader.addMetaDataLine(Annotator.ANNOTATOR_MSG_INFO);

        vcfHeader.addMetaDataLine(new VCFHeaderLine("poncotator.resources", resourceLocationMap.map.toString()));

        vcfHeader.addMetaDataLine(new VCFHeaderLine("poncotator.annotations", types.toString()));

        writer.writeHeader(vcfHeader);

        return writer;
    }

    private boolean lineAlreadyExists(final VCFHeader vcfHeader, final VCFCompoundHeaderLine vcfHeaderLine) {
        if (vcfHeaderLine instanceof VCFInfoHeaderLine) {
            return vcfHeader.hasInfoLine(vcfHeaderLine.getID());
        } else if (vcfHeaderLine instanceof VCFFormatHeaderLine) {
            return vcfHeader.hasFormatLine(vcfHeaderLine.getID());
        } else {
            throw new IllegalArgumentException("Unrecognized VCFCompoundHeaderLine type " + vcfHeaderLine.getClass().getName());
        }
    }

    private void validateReference(final VCFHeader vcfHeader, final SAMSequenceDictionary referenceSequenceDictionary) {
        SAMSequenceDictionary vcfSequenceDictionary = vcfHeader.getSequenceDictionary();

        boolean indexMisMatch = false;
        if (vcfSequenceDictionary != null) {
            //File refHeaderFile = new File(vcfHeader.get("reference").toString());
            if(vcfSequenceDictionary.size() == referenceSequenceDictionary.size()){
                for (int i = 0; i < vcfSequenceDictionary.size(); i++) {
                    SAMSequenceRecord vcfRecord = vcfSequenceDictionary.getSequence(i);
                    SAMSequenceRecord appRecord = referenceSequenceDictionary.getSequence(vcfRecord.getSequenceName());

//                    if (!vcfRecord.isSameSequence(appRecord)) {
//                        Assert.isTrue(vcfRecord.getSequenceLength() == appRecord.getSequenceLength(),
//                                "Sequences have different length: " + vcfRecord.getSequenceName() + " " +
//                                        vcfRecord.getSequenceLength() + "!=" + appRecord.getSequenceLength()
//                        );
//
//                        if (vcfRecord.getSequenceIndex() != appRecord.getSequenceIndex()) {
//                            indexMisMatch = true;
//                        } else {
//                            throw new IllegalArgumentException("Non-matching sequence. " +
//                            "Reference: " + appRecord.toString() + "; Variant: " + vcfRecord.toString() + "; "
//                                    + vcfRecord.getSequenceName()
//                                    + "!=" + appRecord.getSequenceName() + ":" +
//                                    vcfRecord.getSequenceLength()
//                                    + "!=" + appRecord.getSequenceLength() + ":" +
//                                    vcfRecord.getSequenceIndex()
//                                    + "!=" + appRecord.getSequenceIndex() + ". MD5: " + appRecord.getAttribute("M5") + ", " + vcfRecord.getAttribute("M5"));
//                        }
//                    }
                    Assert.isTrue(vcfRecord.getSequenceLength() == appRecord.getSequenceLength(),
                            "Sequences have different length: " + vcfRecord.getSequenceLength()
                            + "!=" + appRecord.getSequenceLength()
                    );
                    Assert.isTrue(vcfRecord.getSequenceName().equals(appRecord.getSequenceName()),
                             "Sequences have different names: " + vcfRecord.getSequenceName()
                            + "!=" + appRecord.getSequenceName()
                    );
                    if (vcfRecord.getSequenceIndex() != appRecord.getSequenceIndex()) {
                        indexMisMatch = true;
                    }
                }
            } else {
                LOGGER.warn("Missing contigs in VCF header");
            }
        } else {
            //            LOGGER.warn("No sequence dictionary in VCF [" + getVCFFile() + "]. Using input application reference");
            LOGGER.warn("No sequence dictionary. Using input application reference");
        }
        if (indexMisMatch) {
            LOGGER.warn("Index does not match for sequence. Using Index provided by the application");
        }
    }

    public List<String> getVcfFilepaths() {
        return ImmutableList.copyOf(vcfFilepaths);
    }

    public void setVcfFilepaths(final List<String> vcfFilepaths) {
        this.vcfFilepaths = vcfFilepaths;
    }

    public void setOutputFilepaths(final List<String> outputFilepaths) {
        this.outputFilepaths = outputFilepaths;
    }

    public List<String> getOutputFilepaths() {
        return ImmutableList.copyOf(outputFilepaths);
    }

//    public void setVcfReaders(final JavaRDD<VCFFileReader> vcfReaders) {
//        this.vcfReaders = vcfReaders;
//    }
//
//    public JavaRDD<VCFFileReader> getVcfReaders() {
////        return ImmutableList.copyOf(vcfReaders);
//        return vcfReaders;
//    }

//    public void setVariantUtils(final List<VariantUtils> variantUtils) {
//        this.variantUtils = variantUtils;
//    }

    public void setVcfLines(final JavaRDD<String> vcfLines) {
        this.vcfLines = vcfLines;
    }

    public JavaRDD<String> getVcfLines() {
        return vcfLines;
    }

    public void setRefDict(SAMSequenceDictionary refDict) {
        this.REF_DICT = refDict;
    }

    public SAMSequenceDictionary getRefDict() {
        return REF_DICT;
    }

    //TODO: add in CMD to vcf
    @Override
    public void build(final String[] args) throws PipelineException {
//        LOGGER.info("Checking Logging: " + LOGGER.getLevel());
//        System.out.println("CHECKING Logging");

//        Options options = new Options();
//
//        options.addOption("vcf", true, "The SNPEff annotated VCF");
//        options.addOption("o", true, "Annotated VCF");
//        options.addOption("r", true, "The Genotype Reference");
//        options.addOption("a", true, "Specify the annotator(s). Needs to be one or more of "
//                + getAnnotatorMap().keySet() + ". Resource annotators are like this -a Resource:ResourceName:ID. Ex: Resource:COSMICv68:GENE,SAMPLE");
//
//        options.addOption("resource",
//                true, "This is a VCF to accompany a resource annotator. Format: ResourceName:VCF. Ex: COSMICv68:/da/onc/sequencing/annotation/CosmicCodingMuts_v68.vcf");
//
//        options.addOption("q",
//                true, "The resource query type. One of " + Arrays.asList(VariantResource.QueryType.values()) +
//                        ". Defaults to Index"
//        );
//
//        CommandLineParser parser = new PosixParser();
//        CommandLine cmd = parser.parse(options, args);
//
//        String[] req = {"vcf", "o", "r", "a"};
//        checkArgs(cmd, req, options);

//        File vcf = new File(cmd.getOptionValue("vcf"));
//        File out = new File(cmd.getOptionValue("o"));
//        String genomeReference = cmd.getOptionValue("r");

//        setOutputFilepaths(out);
//        setVCFFile(vcf);
//        setVcfReaders(new VCFFileReader(vcf, false));

//        if (cmd.hasOption("q")) {
//            setQueryType(VariantResource.QueryType.valueOf(cmd.getOptionValue("q")));
//        } else {
//            setQueryType(VariantResource.QueryType.Index);
//        }

//        processAnnotations(cmd.getOptionValues("a"), cmd.getOptionValues("resource"));

//        final String sparkMaster = getSparkMaster();
//        Assert.notNull(sparkMaster, "Spark master not set");
//        sparkContext.getConf().setAppName("Poncotator").setMaster(sparkMaster);

        final List<String> vcfFilepaths = getVcfFilepaths();
        LOGGER.info("Using input VCF files: " + StringUtils.join(vcfFilepaths, ", "));
        final List<String> outFiles = getOutputFilepaths();
        LOGGER.info("Using output VCF files: " + StringUtils.join(outFiles, ", "));
        Assert.isTrue(vcfFilepaths.size() == outFiles.size(), "Different numbers of input/output files");
//        final List<VCFFileReader> vcfFileReaders = Lists.newArrayList(Iterables.transform(vcfFilepaths, new com.google.common.base.Function<String, VCFFileReader>() {
//            @Override
//            public VCFFileReader apply(@Nullable final String path) {
//                return new VCFFileReader(new File(path));
//            }
//        }));
//        final JavaRDD<VCFFileReader> readersRDD = sparkContext.parallelize(vcfFileReaders);
        // for Spark assume only one concatenated file
        Assert.isTrue(vcfFilepaths.size() == 0);
        final String vcfFilepath = vcfFilepaths.get(0);
        final JavaRDD<String> vcfLines = sparkContext.textFile(vcfFilepath);


//        final JavaRDD<VCFFileReader> readers = sparkContext.parallelize(vcfFilepaths).map(new Function<String, VCFFileReader>() {
//            public VCFFileReader call(final String filepath) throws Exception {
//                return new VCFFileReader(new File(filepath), false);
//            }
//        });
//        readersRDD.cache(); // going to use it twice: once to build the writers and once to process alongside the writers
//        setVcfReaders(readersRDD);

        setReferenceGenomeFilepath(referenceGenomeFilepath);
        final IndexedFastaSequenceFile indexedReference;
        try {
            indexedReference = new IndexedFastaSequenceFile(new File(getReferenceGenomeFilepath()));
        } catch (final FileNotFoundException e) {
            throw new PipelineException("Reference genome file not found", e);
        }
        final SAMSequenceDictionary refDict = indexedReference.getSequenceDictionary();
        setRefDict(refDict);

//        final List<MyVCFWriter> writers = Lists.newArrayList();
//        for (int i = 0; i < vcfFileReaders.size(); i++) {
//            final VCFFileReader reader = vcfFileReaders.get(i);
//            final File outFile = new File(outFiles.get(i));
////            writers.add(buildVCFWriter(reader, outFile, refDict));
//            final MyVCFWriter writer;
//            try {
//                writer = new MyVCFWriter(outFile, outFile.getName());
//            } catch (final FileNotFoundException e) {
//                throw new PipelineException("Could not create output file" + outFile.getAbsolutePath(), e);
//            }
//            writer.writeHeader(reader.getFileHeader());
//        }
//        final JavaRDD<MyVCFWriter> writersRDD = sparkContext.parallelize(writers);
//        setVariantContextWriters(writersRDD);


//        final JavaRDD<File> outFilesRDD = sparkContext.parallelize(outputFilepaths);
//        final JavaPairRDD<VCFFileReader, File> readersAndOutputFiles = readers.zip(outFilesRDD);
//        final Broadcast<SAMSequenceDictionary> refSeqDictBroadcast = sparkContext.broadcast(getRefDict());
//        final JavaRDD<VariantContextWriter> writers = readersAndOutputFiles.map(new Function<Tuple2<VCFFileReader, File>, VariantContextWriter>() {
//            public VariantContextWriter call(final Tuple2<VCFFileReader, File> tuple) throws Exception {
////                final IndexedFastaSequenceFile indexedReference;
////                try {
////                    indexedReference = new IndexedFastaSequenceFile(new File(getReferenceGenomeFilepath()));
////                } catch (final FileNotFoundException e) {
////                    throw new PipelineException("Reference genome file not found", e);
////                }
////                return buildVCFWriter(tuple._1(), tuple._2(), indexedReference.getSequenceDictionary());
//                return buildVCFWriter(tuple._1(), tuple._2(), refSeqDictBroadcast.value());
//            }
//        });
//        setVariantContextWriters(writers);
//        final List<VariantContextWriter> writers = Lists.newArrayList();
//        for (int i = 0; i < vcfFilepaths.size(); i++) {
//            writers.add(buildVCFWriter(readers.get(i), outputFilepaths.get(i)));
//        }
//        setVariantContextWriters(writers);

    }

    @Override
    public void start() throws PipelineException {
        LOGGER.info("Start Annotation");
        try {
            annotate();
            LOGGER.info("End Annotation");
        } catch (final IOException e) {
            throw new PipelineException("Annotation failed", e);
        }
    }

    @Override
    public Enum getActionType() {
        return ActionType.Poncotator;
    }

//    public void setVariantContextWriters(final JavaRDD<MyVCFWriter> variantContextWriters) {
//        this.variantContextWriters = variantContextWriters;
//    }
//
//    public JavaRDD<MyVCFWriter> getVariantContextWriters() {
////        return ImmutableList.copyOf(variantContextWriters);
//        return variantContextWriters;
//    }

    public Collection<VCFHeaderLine> getNewHeaderLines() {
        return ImmutableSet.copyOf(newHeaderLines);
    }

    public void setNewHeaderLines(final Collection<VCFHeaderLine> newHeaderLines) {
        this.newHeaderLines = newHeaderLines;
    }

    private class ResourceLocationMap implements Serializable {
        private Map<String, VariantResource> map = Maps.newHashMap();

        public VariantResource getLocation(String resource){
            Assert.isTrue(map.containsKey(resource), "No location specified for " + resource);
            return map.get(resource);
        }

        public void putLocation(String resourceName, VariantResource variantResource) {
            map.put(resourceName, variantResource);
        }
    }

    private class FileObject {
        private final String filepath;
        private final JavaRDD<String> lines;
        public FileObject(final String filepath, JavaRDD<String> lines) {
            this.filepath = filepath;
            this.lines = lines;
        }
    }

    public Optional<Integer> getNumPartitions() {
        return numPartitions;
    }

    public void setNumPartitions(final Optional<Integer> numPartitions) {
        this.numPartitions = numPartitions;
    }

    public void setNumPartitions(final int numPartitions) {
        this.numPartitions = Optional.of(numPartitions);
    }

    private class VariantPartitioner<T> extends Partitioner {
        private final int numPartitions;
        private static final int DEFAULT_PARTITION_LENGTH = 10000;
        private final Map<Integer, Long> HUMAN_CHROMOSOME_LENGTHS = Maps.newHashMap();
        private final Map<Integer, Long> chromosomeLengths = Maps.newHashMap();
        private final Map<Integer, Integer> chromosomeStartingPartitions = Maps.newHashMap();
        private VariantPartitioner() throws IOException {
            this(DEFAULT_PARTITION_LENGTH);
        }
        private VariantPartitioner(final int partitionLength) throws IOException {
            this(partitionLength, Optional.<File>absent());
        }
        private VariantPartitioner(final int partitionLength, final Optional<File> chromosomeLengthsFile) throws IOException {
            if (chromosomeLengthsFile.isPresent()) {
                final Properties chrLengthProps = new Properties();
                chrLengthProps.load(new FileReader(chromosomeLengthsFile.get()));
                final Set<String> chrs = chrLengthProps.stringPropertyNames();
                final List<Integer> nChrs = Lists.newArrayList();
                for (final String chr : chrs) {
                    try {
                        nChrs.add(Integer.parseInt(chr));
                    } catch (final NumberFormatException e) {
                        // ignore
                    }
                }
                final int maxChr = Collections.max(nChrs);
                for (final String chr : chrs) {
                    final long chrLength = Long.parseLong(chrLengthProps.getProperty(chr));
                    try {
                        chromosomeLengths.put(Integer.parseInt(chr), chrLength);
                    } catch (final NumberFormatException e) {
                        if (chr.equalsIgnoreCase("X")) {
                            chromosomeLengths.put(maxChr+1, chrLength);
                        } else if (chr.equalsIgnoreCase("Y")) {
                            chromosomeLengths.put(maxChr+2, chrLength);
                        } else {
                            throw e;
                        }
                    }
                }
            } else {
                chromosomeLengths.putAll(HUMAN_CHROMOSOME_LENGTHS);
            }
            numPartitions = calculateNumPartitions(partitionLength, chromosomeLengths);
        }

        private int calculateNumPartitions(final int partitionLength, final Map<Integer, Long> chromosomeLengths) {
            int p = 0;
            for (final int chr : chromosomeLengths.keySet()) {
                chromosomeStartingPartitions.put(chr, p);
                p += chromosomeLengths.get(chr) / partitionLength + 1;
            }
            return p;
        }

        @Override
        public int numPartitions() {
            return numPartitions;
        }

        @Override
        public int getPartition(Object key) {
//            final Tuple2<Long, String> tuple = (Tuple2<Long, String>) key;
            final Tuple2<Long, T> tuple = (Tuple2<Long, T>) key;
            final long index = tuple._1();
            final int indexInt = (int) index;
//            final String line = tuple._2();
//            final int start = Integer.parseInt(line.split("\t", 3)[1]);
            final T val = tuple._2();
            int start = 0;
            if (val instanceof String) {
                start = Integer.parseInt(((String) val).split("\t", 3)[1]);
            } else if (val instanceof VariantContext) {
                start = ((VariantContext) val).getStart();
            } else {
                throw new IllegalArgumentException("Could not derive partition from object type " + val.getClass().getSimpleName());
            }
            return (int) (chromosomeStartingPartitions.get(indexInt) + ((start - 1) / chromosomeLengths.get(indexInt)));
        }

    }

    private class VCFLineRDDHolder implements Serializable {
        private final JavaRDD<String> headerLines;
        private final JavaRDD<String> variantLines;
        private VCFLineRDDHolder(final JavaRDD<String> headerLines, final JavaRDD<String> variantLines) {
            this.headerLines = headerLines;
            this.variantLines = variantLines;
        }
        private JavaRDD<String> getHeaderLines() {
            return headerLines;
        }
        private JavaRDD<String> getVariantLines() {
            return variantLines;
        }
        private void saveAsTextFile(final String filepath) {
            headerLines.union(variantLines).saveAsTextFile(filepath);
        }
    }
}
