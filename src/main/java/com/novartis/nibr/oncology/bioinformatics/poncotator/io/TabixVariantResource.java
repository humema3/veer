package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.common.io.FileIterator;
import java.io.File;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContextBuilder;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.tribble.readers.TabixReader;
import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.List;

/**
 *
 * Created by jonesmic on 7/8/2014.
 */
public class TabixVariantResource extends VariantResource {
    private final static Logger logger = Logger.getLogger(TabixVariantResource.class);


    private final TabixReader reader;
    private List<String> header;

    boolean isChrContig;
    boolean printChrFix = true;


    public TabixVariantResource(final String resourceName, final File location, final QueryType queryType) throws IOException {
        super(resourceName, location, queryType);
        this.reader = new TabixReader(location.getAbsolutePath());
        validate();
    }

    private void validate() {
        CloseableIterator<VariantContext> it = iterator();
        it.hasNext();
        VariantContext variantContext = it.next();
        isChrContig=variantContext.getChr().startsWith("chr");
        it.close();
    }

    @Override
    //TODO: This is slow if the user wants more then one infoID from the same variant.
    public String query(String chr,
                        int start,
                        int end,
                        Allele refAllele,
                        Allele altAllele,
                        String infoID,
                        boolean altAlleleSpecific)  {

        Assert.isTrue(header.indexOf(infoID)>-1,
                "No INFO of " + infoID + " in " + getResourceName() + ":" + getLocation());

        if(chr.startsWith("chr") && !isChrContig){
            chr = chr.replace("chr", "");
            if(printChrFix) {
                logger.info("Striped 'chr' off of contig while searching " + getResourceName());
                printChrFix=false;
            }
        }

        if(getQueryType().equals(QueryType.Index)){
            return doIndexQuery(chr, start, end, refAllele, altAllele, infoID);
        }else if(getQueryType().equals(QueryType.Iterative)){
            return doIterativeQuery(chr, start, end, refAllele, altAllele, infoID);
        }else{
            throw new UnsupportedOperationException("No Query type " + getQueryType());
        }
    }

    private String currentChr = null;
    private TabixLocationGroupedRecordsIterable tabixLocationGroupedRecordsIterable;

    private String doIterativeQuery(String chr, int start, int end, Allele refAllele, Allele altAllele, String infoID) {
        String infoValue = "";
        if(!chr.equals(currentChr)){
            tabixLocationGroupedRecordsIterable = new TabixLocationGroupedRecordsIterable(reader.query(chr));
        }

        for(TabixLocationGroupedRecords tabixLocationGroupedRecords : tabixLocationGroupedRecordsIterable){
            if(tabixLocationGroupedRecords.getPosition() == start){
                TabixRecord tabixRecord = tabixLocationGroupedRecords.query(refAllele, altAllele);
                if(tabixRecord != null) {
                    infoValue = tabixRecord.getTabixRecordValue(header.indexOf(infoID));
                }
            }else if(tabixLocationGroupedRecords.getPosition() > start){
                break;
            }
        }

        return infoValue;
    }

    private String doIndexQuery(String chr, int start, int end, Allele refAllele, Allele altAllele, String infoID) {
        String reg = chr + ":" + start + "-" + end;
        TabixReader.Iterator iterator = reader.query(reg);
        String[] tabixRecordColumns;
        try {
            tabixRecordColumns = queryForAllele(iterator, refAllele, altAllele);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }

        String infoValue = "";
        if(tabixRecordColumns != null){
            infoValue = infoValueClean(tabixRecordColumns[header.indexOf(infoID)]);
            infoValue = infoValue.equals(".") ? "" : infoValue;
        }

        return infoValue;
    }

    private String[] queryForAllele(TabixReader.Iterator iterator, Allele refAllele, Allele altAllele) throws IOException {
        String tabixRecord;
        while (iterator != null && (tabixRecord = iterator.next()) != null) {
            String[] tabixRecordColumns = tabixRecord.split("\t");
            Allele tbxRefAllele = Allele.create(tabixRecordColumns[2], true);
            if (tbxRefAllele.equals(refAllele)) {
                Allele tbxAltAllele = Allele.create(tabixRecordColumns[3]);
                if (tbxAltAllele.equals(altAllele)) {
                    return tabixRecordColumns;
                }
            }
        }
        return null;
    }

    private String extractInfoValue(String[] tabixRecordColumns, Allele refAllele, Allele altAllele, String infoID){
        String value = "";
        Allele tbxRefAllele = Allele.create(tabixRecordColumns[2]);
        if (tbxRefAllele.equals(refAllele)){
            Allele tbxAltAllele = Allele.create(tabixRecordColumns[4]);
            if(tbxAltAllele.equals(altAllele)){
                value = infoValueClean(tabixRecordColumns[header.indexOf(infoID)]);
                value = value.equals(".") ? "" : value;
            }
        }

        return value;
    }

    /**
     * p53, DNA-binding domain (1);p53-like transcription factor, DNA-binding (1);p53/RUNT-type transcription factor, DNA-binding domain (1);;
     * @param infoValue -
     * @return the cleaned value
     */
    private String infoValueClean(String infoValue) {
        infoValue = infoValue.trim().replaceAll(";$", "");
        return infoValue.replaceAll(";", ",").replaceAll("\\s", "_").replaceAll("=", "_eq_").replaceAll(",", "_");
    }

    @Override
    public boolean hasInfoLine(String infoField) {
        FileIterator fit = null;
        try {
            fit = new FileIterator(getLocation(), true);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
        this.header = fit.getHeader();
        return header.contains(infoField);
    }

    @Override
    public VCFInfoHeaderLine getInfoHeaderLine(String infoField) {
        VCFHeaderLineCount count = VCFHeaderLineCount.UNBOUNDED;
        VCFHeaderLineType type = VCFHeaderLineType.String;

        return new VCFInfoHeaderLine(infoField, count, type, infoField);
    }

    @Override
    public CloseableIterator<VariantContext> iterator() {
        try {
            return new TabixCloseableIterator(getLocation());
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private class TabixCloseableIterator implements CloseableIterator<VariantContext> {
        private TabixReader reader;

        private TabixCloseableIterator(File location) throws IOException {
            this.reader = new TabixReader(location.getAbsolutePath());
            while(!reader.readLine().startsWith("#"));
        }

        String line = null;

        public void close() {
            reader.close();
        }

        public boolean hasNext() {
            try {
                line = reader.readLine();
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            }
            return line != null;
        }

        public VariantContext next() {
            String[] toks = line.split("\t");

            String source = getResourceName();
            String contig = toks[0];
            long start = Long.parseLong(toks[1]);
            List<Allele> alleles = Lists.newArrayList(Allele.create(toks[2], true), Allele.create(toks[3], false));
            VariantContextBuilder builder = new VariantContextBuilder(source, contig, start, start, alleles);
            return builder.make();
        }

        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }
}
