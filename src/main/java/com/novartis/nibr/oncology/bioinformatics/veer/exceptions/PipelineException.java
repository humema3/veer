package com.novartis.nibr.oncology.bioinformatics.veer.exceptions;

/**

 * User: jonesmic
 * Date: Feb 25, 2011
 * Time: 4:44:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class PipelineException extends Exception {
    public PipelineException(String message) {
        super(message);
    }

    public PipelineException(String message, Throwable cause) {
        super(message, cause);
    }
}
