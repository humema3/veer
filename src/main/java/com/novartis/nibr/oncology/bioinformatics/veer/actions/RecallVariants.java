package com.novartis.nibr.oncology.bioinformatics.veer.actions;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.broadinstitute.gatk.engine.CommandLineGATK;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import scala.Tuple2;

import java.io.*;
import java.util.*;

/**
 * Created by humema3 on 10/21/2015.
 */
@Component
public class RecallVariants extends VEERPipelineAction {

//    private static final String BAM_LIST_FILE_NAME = "smf_bam_list.txt";
    private File refBamListFile;
//    private List<File> inputBamFiles;
    private Map<String, String> bamFileToSampleName = Maps.newHashMap();
    private int numSamples;
//    private JavaSparkContext sparkContext;
//    private File alleleVcfDir;
//    private String alleleVcfFilename;
//    private int numContigs;
//    private int maxRunTime;
//    private int numCores;
//    private int numGigsMem;

    public RecallVariants() {
        super(PipelineActionType.RecallVariants);
    }

    @Override
    protected void execute() throws PipelineException {
        final List<String> inputVcfFilepaths = getInputVcfFilepaths();
        final JavaSparkContext sparkContext = getSparkContext();
        final JavaRDD<File> inputVcfRDD = sparkContext.parallelize(inputVcfFilepaths).map(new org.apache.spark.api.java.function.Function<String, File>() {
            public File call(final String filepath) throws Exception {
                return new File(filepath);
            }
        });
        final List<File> inputBamFiles = Lists.newArrayList(Collections2.transform(bamFileToSampleName.keySet(), new Function<String, File>() {
            public File apply(final String path) {
                return new File(path);
            }
        }));
//        final JavaRDD<File> inputBamRDD = sparkContext.parallelize(inputBamFiles);
        final List<String> gatkArgs = Lists.newArrayList("--suppressCommandLineHeader", "--unsafe", "LENIENT_VCF_PROCESSING", "-R", getReferenceGenomeFilepath(), "-T", "UnifiedGenotyper", "-gt_mode", "GENOTYPE_GIVEN_ALLELES", "-out_mode", "EMIT_ALL_SITES", "-glm", "BOTH", "--filter_reads_with_N_cigar"); //TODO?
        final List<List<String[]>> gatkCommandLists = Lists.newArrayList();
        final String outputFilename = getOutputFilename();
//        final List<File> inputBamFilesDuplicated = Lists.newArrayList();
        // parallelize by sample (bam), then by chromosome (vcf)
        for (final File bamFile : inputBamFiles) {
//            inputBamFilesDuplicated.add(bamFile);
            final List<String[]> bamGatkCommandLists = Lists.newArrayList();
            for (final String vcfFilePath : inputVcfFilepaths) {
                final List<String> gatkCommandList = Lists.newArrayList(gatkArgs);
                gatkCommandList.addAll(Lists.newArrayList("-I", bamFile.getAbsolutePath(), "-o", outputFilename, "-L", vcfFilePath, "--alleles", vcfFilePath));
                bamGatkCommandLists.add((String[]) gatkCommandList.toArray());
            }
            gatkCommandLists.add(bamGatkCommandLists);
        }
        final List<String[]> gatkCommands = Lists.newArrayList();
        for (final List<String[]> commandList : gatkCommandLists) {
            for (final String[] command : commandList) {
                gatkCommands.add(command);
            }
        }
        final JavaRDD<String[]> gatkCommandsRDD = sparkContext.parallelize(gatkCommands);

        final List<CommandLineGATK> gatkRunners = Lists.newArrayList();
        for (int i = 0; i < gatkCommands.size(); i++) {
            gatkRunners.add(new CommandLineGATK());
        }
        final JavaRDD<CommandLineGATK> gatkRunnersRDD = sparkContext.parallelize(gatkRunners);
        final JavaPairRDD<CommandLineGATK, String[]> runnersAndArgs = gatkRunnersRDD.zip(gatkCommandsRDD);
        runnersAndArgs.foreach(new VoidFunction<Tuple2<CommandLineGATK, String[]>>() {
            public void call(final Tuple2<CommandLineGATK, String[]> tuple) throws Exception {
                CommandLineGATK.start(tuple._1(), tuple._2());
            }
        });


//        inputBamRDD.foreach(new VoidFunction<File>() {
//            public void call(final File file) throws Exception {
//                final List<String> gatkCommandList =
//            }
//        });


    }

    @Override
    protected void checkFileSystem() throws PipelineException {
//        logger.info("Deriving SMF/BAM list from " + getRefBamListFile());
//        final File resultDir = createResultsDir();
//        setResultsDir(resultDir);
//        buildBamListFile();
        super.checkFileSystem();

        createSampleNameToBamMap();
        for (final String bamFilePath : bamFileToSampleName.keySet()) {
            Assert.isTrue((new File(bamFilePath)).isFile(), "Could not find BAM file " + bamFilePath);
        }

//        final File alleleVcfDir = getAlleleVcfDir();
//        if (!alleleVcfDir.isDirectory()) {
//            throw new PipelineException("No directory " + alleleVcfDir.getAbsolutePath() + " found");
//        }
//        final int numContigs = getNumContigs();
//        File[] alleleVcfFiles = alleleVcfDir.listFiles(new FilenameFilter() {
//            public boolean accept(File dir, String name) {
//                final Pattern p = Pattern.compile("(\\d+)_" + getAlleleVcfFilename() + "\\.vcf");
//                final Matcher m = p.matcher(name);
//                if (!m.matches()) {
//                    return false;
//                }
//                final int contigNum = Integer.parseInt(m.group(1));
//                return contigNum >= 1 && contigNum <= numContigs;
//            }
//        });
//        if (alleleVcfFiles.length == 0 && numContigs == 1) {
//            alleleVcfFiles = alleleVcfDir.listFiles(new FilenameFilter() {
//                public boolean accept(File dir, String name) {
//                    final Pattern p = Pattern.compile(getAlleleVcfFilename() + "\\.vcf");
//                    return p.matcher(name).matches();
//                }
//            });
//        }
//        Assert.isTrue(alleleVcfFiles.length == numContigs,
//                "Number of allele VCF files found in directory " + alleleVcfDir.getAbsolutePath() + " is " + alleleVcfFiles.length + ", should be " + numContigs);

//        final File script = new File(resultDir, getScriptFilename());
//        setScript(script);
    }

    private void createSampleNameToBamMap() throws PipelineException {
        final File bamListFile = getRefBamListFile();
        final Scanner sc;
        try {
            sc = new Scanner(bamListFile);
        } catch (final IOException e) {
            throw new PipelineException("Could not find BAM reference file " + bamListFile.getAbsolutePath(), e);
        }
        while (sc.hasNextLine()) {
            final String line = sc.nextLine().trim();
            final String[] fields = line.split("\t");
            final String sample = fields[0];
            final String bamFile = fields[1];
            bamFileToSampleName.put(bamFile, sample);
        }
    }

//    private File createResultsDir() throws PipelineException {
//        final File resultDir = new File(getOutputDirPath(), getActionType().name());
//        if(resultDir.exists()){
//            logger.warn(resultDir.getAbsolutePath() + " already exists. Will overwrite!!");
//            resultDir.mkdir();
//        }else if(!resultDir.mkdir()){
//            throw new PipelineException("Could not create dir: " + resultDir);
//        }
//        return resultDir;
//    }

//    private void buildBamListFile() throws IOException {
//        //File to write list to
//        final File bamListFile = new File(getOutputDirPath(), BAM_LIST_FILE_NAME);
//        final PrintWriter out
//                = new PrintWriter(new BufferedWriter(new FileWriter(bamListFile)));
//        final List<String> lines = extractSMFAndBamLocations(getRefBamListFile());
//        setNumSamples(lines.size() - 1);
//        out.print(StringUtils.join(lines, "\n"));
//        out.println();
//        out.flush();
//        out.close();
//    }

//    static final List<String> extractSMFAndBamLocations(final File refFile) throws IOException {
//        final Pattern bamPattern = Pattern.compile("file:\\/\\/(\\w+)(\\/.*\\.bam)");
//        final Pattern smfIdPattern = Pattern.compile("[A-Z]{2}\\d{2}-\\d{2}[A-Z]{2}");
//        final FileIterator fit = new FileIterator(refFile);
//        Assert.isTrue(fit.getHeader().contains("CONCEPT_SMFID"), "File must have the CONCEPT_SMFID column header");
//        Assert.isTrue(fit.getHeader().contains("BAM_URI"), "File must have the BAM_URI column header");
//        final List<String> lines = Lists.newArrayList("CONCEPT_SMFID\tBAM_URI");
//        for (final Row row : fit) {
//            final String smfId = row.valueAt("CONCEPT_SMFID");
//            Assert.isTrue(smfIdPattern.matcher(smfId).matches(), "Bad SMF ID " + smfId);
//            final String bamUri = row.valueAt("BAM_URI");
//            final Matcher bamMatcher = bamPattern.matcher(bamUri);
//            Assert.isTrue(bamMatcher.matches(), "Bad BAM URI " + bamUri);
//            final String bam = bamMatcher.group(2);
//
//            if(! new File(bam).exists()){
//                logger.error("Ignoring missing File does not exist: " + bam);
//            }else {
//                lines.add(smfId + "\t" + bam);
//            }
//        }
//        return lines;
//    }

//    @Override
//    protected String createScript(String scriptContent) throws PipelineException {
//        scriptContent = scriptContent.replaceAll("@SMF_BAM_LIST_FILE@", BAM_LIST_FILE_NAME);
//        scriptContent = scriptContent.replaceAll("@NUM_SAMPLES@", getNumSamples()+"");
//        scriptContent = scriptContent.replaceAll("@ALLELES_DIR@", getAlleleVcfDir().getAbsolutePath());
//        scriptContent = scriptContent.replaceAll("@ALLELES_FILENAME@", getAlleleVcfFilename());
//        scriptContent = scriptContent.replaceAll("@NUM_CONTIGS@", getNumContigs()+"");
////        scriptContent = scriptContent.replaceAll("@MAX_RUNTIME@", getMaxRunTime()+"");
////        scriptContent = scriptContent.replaceAll("@NUM_CORES@", getNumCores()+"");
//        scriptContent = scriptContent.replaceAll("@NUM_GIGS_MEM@", getNumGigsMem()+"");
//        return scriptContent;
//    }
//
//    @Override
//    protected boolean hasFinishAction() {
//        return true;
//    }
//
//    @Override
//    protected String createFinishScript(String scriptContent) throws PipelineException {
//        scriptContent = scriptContent.replaceAll("@NUM_CONTIGS@", getNumContigs()+"");
//        return scriptContent;
//    }

//    public String getAlleleVcfFilename() {
//        return alleleVcfFilename;
//    }
//
//    public void setAlleleVcfFilename(String alleleVcfFilename) {
//        this.alleleVcfFilename = alleleVcfFilename;
//    }
//
//    public int getNumContigs() {
//        return numContigs;
//    }
//
//    public void setNumContigs(int numContigs) {
//        this.numContigs = numContigs;
//    }
//
    public File getRefBamListFile() {
        return refBamListFile;
    }

    public void setRefBamListFile(File refBamListFile) {
        this.refBamListFile = refBamListFile;
    }
//
//    public File getAlleleVcfDir() {
//        return alleleVcfDir;
//    }

//    public void setAlleleVcfDir(File alleleVcfDir) {
//        this.alleleVcfDir = alleleVcfDir;
//    }

    public int getNumSamples() {
        return numSamples;
    }

    public void setNumSamples(int numSamples) {
        this.numSamples = numSamples;
    }

//    public int getMaxRunTime() {
//        return maxRunTime;
//    }
//
//    public void setMaxRunTime(final int maxRunTime) {
//        this.maxRunTime = maxRunTime;
//    }
//
//    public int getNumCores() {
//        return numCores;
//    }
//
//    public void setNumCores(final int numCores) {
//        this.numCores = numCores;
//    }

//    public int getNumGigsMem() {
//        return numGigsMem;
//    }
//
//    public void setNumGigsMem(final int numGigsMem) {
//        this.numGigsMem = numGigsMem;
//    }

//    public JavaSparkContext getSparkContext() {
//        return sparkContext;
//    }
//
//    public void setSparkContext(final JavaSparkContext sparkContext) {
//        this.sparkContext = sparkContext;
//    }

//    public List<File> getInputBamFiles() {
//        return ImmutableList.copyOf(inputBamFiles);
//    }
//
//    public void setInputBamFiles(final List<File> inputBamFiles) {
//        this.inputBamFiles = inputBamFiles;
//    }
}
