package com.novartis.nibr.oncology.bioinformatics.common.util;

import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.ActionHalted;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.spark.api.java.JavaSparkContext;

/**
 *
 * Created by JONESMIC on 4/24/2014.
 */
public abstract class ActionExecutor extends AbstractApplication {
    public abstract void build(final String[] args) throws PipelineException;

    public abstract void start() throws PipelineException;

    public abstract Enum getActionType();

    private static JavaSparkContext SPARK_CONTEXT;

    private static String SPARK_MASTER;

    public String getSparkMaster() {
        return SPARK_MASTER;
    }

    public void setSparkMaster(final String sparkMaster) {
        SPARK_MASTER = sparkMaster;
    }

    protected JavaSparkContext getSparkContext() {
        return SPARK_CONTEXT;
    }

    protected void setSparkContext(final JavaSparkContext sparkContext) { SPARK_CONTEXT = sparkContext; }

}
