package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERCountAnnotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.VEERScoreAnnotator;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.CombineVariants;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Created by humema3 on 1/23/17.
 */
@Component
public class GenerateVEERScoresExecutor extends VEERActionExecutor {
    private static final Logger LOGGER = Logger.getLogger(GenerateVEERScoresExecutor.class);

    @Autowired
    private AnnotateWithVEERExecutor annotator;
//    @Autowired
//    private AnnotateWithVEER annotator;

    @Autowired
    private CombineVariantsExecutor combiner;

    private boolean runCombineVariants;

    @Override
    protected VEERPipelineAction getAction() {
        return annotator.getAction();
    }

    @Override
    protected Options getOptions() {
        final Options options = annotator.getOptions();
//        options.addOption("d", "input_dir", true, "The directory in which the input VCF files are stored");
//        options.getOption("d").setRequired(true);
//        options.addOption("n", "num_files", true, "The number of files over which the input is parallelized. Input VCF files should start with an integer from 1 to this number inclusive (see -f argument). Default is 1 (no parallelization).");
//        options.getOption("n").setRequired(false);
//        options.addOption("f", "input_filename", true, "The filename for all the input VCF files. If not supplied, all files in the directory will be used. If supplied, each input file should be called [i]_[filename].vcf, where [i] is an integer between " +
//                "1 and the '-n' argument, and [filename] is this argument. If you supply -n 1, the '[i]_' prefix is optional.");
        options.addOption("c", "counts_only", false, "Annotate with counts only (no scores)");
        options.getOption("c").setRequired(false);
        options.addOption("no_cv", "no_combine_variants", false, "Don't run CombineVariants step after finishing.");
        options.getOption("no_cv").setRequired(false);
        return options;
    }

    @Override
    public void build(final String[] args) throws PipelineException {
        LOGGER.info("Building " + annotator.getClass().getSimpleName() + " from arguments");
        final Options options = getOptions();
        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd = processArgs(parser, options, args);
//        super.build(args);
        annotator.build(args);
//        annotator.setAnnotateScores(true);
//        Assert.isTrue(!annotator.getCountsResource().isPresent());
        final String[] annotations;
        if (cmd.hasOption("counts_only")) {
            annotations = new String[]{VEERCountAnnotator.ID};
        } else {
            annotations = new String[]{VEERCountAnnotator.ID, VEERScoreAnnotator.ID};
        }
        annotator.setAnnotations(annotations);
        final Optional<Integer> numFiles = annotator.getAction().getNumFiles();
        setRunCombineVariants(numFiles.isPresent() && numFiles.get() > 1 && !cmd.hasOption("no_cv"));
        //TODO
//        combiner.build(args);
    }

    @Override
    public void start() throws PipelineException {
        LOGGER.info("Starting");
        annotator.start();
        if (runCombineVariants()) {
            combiner.start();
        }
    }

    public boolean runCombineVariants() {
        return runCombineVariants;
    }
    public void setRunCombineVariants(final boolean runCombineVariants) {
        this.runCombineVariants = runCombineVariants;
    }

    @Override
    public VEERPipelineAction.PipelineActionType getActionType() {
        return VEERPipelineAction.PipelineActionType.GenerateVEERScores;
    }
}
