package com.novartis.nibr.oncology.bioinformatics.common.io;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 *
 * User: jonesmic
 * Date: Mar 29, 2012
 * Time: 7:36:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileIterator implements Iterable<Row>{

    private int numHeaderLines=0;
    private List<String> headers;
    private final Pattern commentPattern = Pattern.compile("^#.*");
    private String resourceName;
    private final static Pattern tab = Pattern.compile("\t", -1);
    private enum ResourceType {SpringResource, File}
    private final ResourceType resourceType;
    private final Resource resource;

    public FileIterator(Resource resource) throws IOException {
        this.resourceType = ResourceType.SpringResource;
        init(resource.getInputStream(), false);
        this.resource = resource;
    }

    /**
     *
     * @param file  -
     * @param headerIsCommented  if true the header has a comment character in it
     * @throws java.io.IOException
     */
    public FileIterator(File file, boolean headerIsCommented) throws IOException {
        this.resourceType = ResourceType.File;
        init(getInputStreamFromFile(file), headerIsCommented);
        this.resource = new FileSystemResource(file);
    }

    /**
     *
     *
     * @param file the file
     * @param rFormatFirstColumnName - If present assume the first column is not named and use this as the value
     * @throws java.io.IOException
     */
    public FileIterator(File file, String rFormatFirstColumnName) throws IOException {
        this.resourceType = ResourceType.File;
        this.resourceName = file.getAbsolutePath();

        init(getInputStreamFromFile(file), false);
        this.resource = new FileSystemResource(file);

        List<String> newList = new ArrayList<String>();
        newList.add(rFormatFirstColumnName);
        newList.addAll(headers);
        headers = newList;

    }

    /**
     * Create a file iterator assuming that there might be commented lines ^#.*
     * @param file the file
     * @throws java.io.IOException -
     */
    public FileIterator(File file) throws IOException {
        this.resourceType = ResourceType.File;
        init(getInputStreamFromFile(file), false);
        this.resource = new FileSystemResource(file);
    }

    private InputStream getInputStreamFromFile(File file) throws IOException {
        InputStream is;
        if(file.getName().endsWith(".gz")){
            is = new GZIPInputStream(new FileInputStream(file));
        }else {
            is = new FileInputStream(file);
        }
        return is;
    }

    private void init(InputStream is, boolean headerIsCommented) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        String line;
        while((line = in.readLine())!=null){
            numHeaderLines++;

            if(headerIsCommented){
                if(!line.isEmpty()){
                    headers = Arrays.asList(tab.split(line));
                    break;
                }
            }else{
                if(!commentPattern.matcher(line).matches() && !line.isEmpty()){
                    headers = Arrays.asList(tab.split(line));
                    break;
                }
            }
        }

        if(headers == null){
            in.close();
            throw new IOException("No Header found in file: " + resourceName);
        }
    }

    /**
     * Returns an iterator over a set of elements of type T.
     *
     * @return an Iterator.
     */
    public Iterator<Row> iterator() {
        try {
            return new RowIterator(getInputStream(), numHeaderLines, resourceName, headers);
        } catch (IOException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    private InputStream getInputStream() throws IOException {
        if(resourceType == ResourceType.SpringResource){
            return resource.getInputStream();
        }else if(resourceType == ResourceType.File){
            File file = ((FileSystemResource)resource).getFile();
            return getInputStreamFromFile(file);
        }else{
            throw new IllegalArgumentException("No resource type: " + resourceType);
        }
    }

    public List<String> getHeader() {
        return headers;
    }

    private static final class RowIterator implements Iterator<Row> {
        private final String resourceName;
        int lineNum=0;
        private final BufferedReader in;
        private String line;
        private final List<String> headers;

        public RowIterator(InputStream inputStream, int numHeaderLines, String resourceName, List<String> headers) throws IOException {
            this.resourceName = resourceName;
            this.headers = headers;
            in = new BufferedReader(new InputStreamReader(inputStream));
            for(int i = 0; i <= numHeaderLines; i++){
                line = in.readLine(); //This will be the first non header line
                lineNum++;
            }
        }

        @Override
        public String toString() {
            return "FileIterator{" +
                    "lineNum=" + lineNum +
                    ", line='" + line + '\'' +
                    '}';
        }

        public int getLineNum() {
            return lineNum;  //To change body of created methods use File | Settings | File Templates.
        }

        /**
         * Returns <tt>true</tt> if the iteration has more elements. (In other
         * words, returns <tt>true</tt> if <tt>next</tt> would return an element
         * rather than throwing an exception.)
         * TODO: Re-implement this so that no work is done here.
         * @return <tt>true</tt> if the iterator has more elements.
         */
        public boolean hasNext() {
            return line != null;
        }

        /**
         * Returns the next element in the iteration or null if the column isn't present.
         *
         * @return the next element in the iteration.
         * @throws java.util.NoSuchElementException
         *          iteration has no more elements.
         */
        public Row next() {
            String[] toks;
            if(line == null){
                throw new NoSuchElementException("No such element");
            }else{
                if(line.isEmpty()){
                    throw new NoSuchElementException("Empty line: " + lineNum);
                }else{
                    toks = tab.split(line, -1);
                    if(toks.length!=headers.size()){
                        for(String val : toks){
                            System.err.println("val: " + val);
                        }

                        for(String val : headers){
                            System.err.println("header: " + val);
                        }
                        throw new NoSuchElementException(resourceName + ": Wrong number of columns in row. Header: " +
                                headers.size() + " row: " + toks.length + ", line: " + line);
                    }
                }
            }
            try {
                line = in.readLine();
            } catch (IOException e) {
                throw new NoSuchElementException(e.getMessage());
            }
            return buildRow(toks);
        }

        protected Row buildRow(final String[] toks){
            return new Row(){
                public String valueAt(String col) {
                    if(headers.indexOf(col)==-1){
                        return null;
                    }
                    return toks[headers.indexOf(col)];
                }

                public String valueAt(int col) {
                    return toks[col];
                }

                public int length() {
                    return toks.length;
                }

                public String toString(){
                    StringBuilder sb = new StringBuilder();
                    sb.append("toks.length: ").append(toks.length).append("\n");
                    for(String col : headers){
                        try{
                            sb.append(toks[headers.indexOf(col)]).append("\t");
                        }catch (ArrayIndexOutOfBoundsException a){
                            sb.append("Index out of bound error for Col: '").append(col).append("' at index '")
                                    .append(headers.indexOf(col)).append("'\n");
                            sb.append(a.getMessage());
                        }
                    }
                    sb.setLength(sb.length()-1);
                    return sb.toString();
                }
            };
        }

        /**
         * Removes from the underlying collection the last element returned by the
         * iterator (optional operation).  This method can be called only once per
         * call to <tt>next</tt>.  The behavior of an iterator is unspecified if
         * the underlying collection is modified while the iteration is in
         * progress in any way other than by calling this method.
         *
         * @throws UnsupportedOperationException if the <tt>remove</tt>
         *                                       operation is not supported by this Iterator.
         * @throws IllegalStateException         if the <tt>next</tt> method has not
         *                                       yet been called, or the <tt>remove</tt> method has already
         *                                       been called after the last call to the <tt>next</tt>
         *                                       method.
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
