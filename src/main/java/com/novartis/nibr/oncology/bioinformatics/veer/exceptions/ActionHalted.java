package com.novartis.nibr.oncology.bioinformatics.veer.exceptions;

/**
 *
 * User: jonesmic
 * Date: 1/30/13
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActionHalted extends RuntimeException {
    public ActionHalted(String message) {
        super(message);
    }

    public ActionHalted(String message, Throwable cause) {
        super(message, cause);
    }
}
