package com.novartis.nibr.oncology.bioinformatics.common.util;

import java.math.BigDecimal;

public class MathUtils {
    private static double LOG_MIN_DOUBLE = Math.log(Double.MIN_VALUE);

    /**
     * log gamma function
     * @param  a is the value at which log gamma is to be returned
     */
    public static double loggamma(double a) {
/*C                              Method
C
C
C     Renames GAMLN from:
C     DiDinato, A. R. and Morris,  A.   H.  Algorithm 708: Significant
C     Digit Computation of the Incomplete  Beta  Function Ratios.  ACM
C     Trans. Math.  Softw. 18 (1993), 360-373.
C
C**********************************************************************
C-----------------------------------------------------------------------
C            EVALUATION OF LN(GAMMA(A)) FOR POSITIVE A
C-----------------------------------------------------------------------
C     WRITTEN BY ALFRED H. MORRIS
C          NAVAL SURFACE WARFARE CENTER
C          DAHLGREN, VIRGINIA
*/
//--------------------------
//     D = 0.5*(LN(2*PI) - 1)
//--------------------------
//    .. Scalar Arguments ..
        double c0 = .833333333333333E-01;
        double c1 = -.277777777760991E-02;
        double c2 =  .793650666825390E-03;
        double c3 = -.595202931351870E-03;
        double c4 = .837308034031215E-03;
        double c5 = -.165322962780713E-02;
        double d = .418938533204673E0;
        double t;
        double w;
        double dlngam;  //return value
        int n;
        if (a <= 0.8) {
            dlngam = gamln1(a) - Math.log(a);
        }
        else  if (a <= 2.250) {
            t = (a-0.5) - 0.5;
            dlngam = gamln1(t);
        }
        else if (a < 10.0) {
            n = (int)(a - 1.25);
            t = a;
            w = 1.00;
            for (int i = 1; i <= n; i++) {
                t = t - 1.0;
                w = t*w;
            }

            dlngam = gamln1(t-1.00) + Math.log(w);
        }
        else {
            t = (1.00/a)*(1.00/a);
            w = (((((c5*t+c4)*t+c3)*t+c2)*t+c1)*t+c0)/a;
            dlngam = (d+w) + (a-0.50)* (Math.log(a)-1.00);
        }
        return dlngam;
    }


    public static double  gamln1(double a) {
//-----------------------------------------------------------------------
//     EVALUATION OF LN(GAMMA(1 + A)) FOR -0.2 .LE. A .LE. 1.25
//-----------------------------------------------------------------------
//     .. Scalar Arguments ..
        double w,x, result;
        double p0 =.577215664901533E+00;
        double p1=.844203922187225E+00;
        double p2 = -.168860593646662E+00;
        double p3 = -.780427615533591E+00;
        double p4 = -.402055799310489E+00;
        double p5 = -.673562214325671E-01;
        double p6 = -.271935708322958E-02;
        double q1 = .288743195473681E+01;
        double q2 = .312755088914843E+01;
        double q3 = .156875193295039E+01;
        double q4 = .361951990101499E+00;
        double q5 = .325038868253937E-01;
        double q6 = .667465618796164E-03;
        double r0 = .422784335098467E+00;
        double r1 = .848044614534529E+00;
        double r2 = .565221050691933E+00;
        double r3 = .156513060486551E+00;
        double r4 = .170502484022650E-01;
        double r5 =  .497958207639485E-03;
        double s1 =  .124313399877507E+01;
        double s2 =  .548042109832463E+00;
        double s3 =  .101552187439830E+00;
        double s4 =  .713309612391000E-02;
        double s5 =  .116165475989616E-03;

        if (a < 0.6) {
            w = ((((((p6*a+p5)*a+p4)*a+p3)*a+p2)*a+p1)*a+p0)/((((((q6*a+  q5)*a+q4)*a+q3)*a+q2)*a+q1)*a+1.0);
            result = -a*w;
        }
        else {
            x = (a-0.5) - 0.5;
            w = (((((r5*x+r4)*x+r3)*x+r2)*x+r1)*x+r0)/( 	((((s5*x+s4)*x+s3)*x+s2)*x+s1)*x+1.00);
            result = x*w;
        }
        return result;
    }  				//END of gamln1

    public static double logNChooseKG(double N, double k) {
        double result;
        result = loggamma(N+1) - loggamma(k+1) - loggamma(N-k+1);
        return result;
    }

    /**
     * The extended full Fisher Test
     * @param N Total number of objects in universe
     * @param m Number of objects in category
     * @param n Number of object sin test set
     * @param k Overlap of objects
     * @return probablity that the null hypothesis is true
     *          (null hypothesis - this overalap is unrelated to the properties of the test set)
     */
    public static double hyperGeometricG(int N, int m, int n, int k){
        double score = 0;
        if((m < 0) | (N < 0) | (n <= 0) | (m > N) | (n > N)){
            score = Double.NaN;
        }else if(!((k < 0) | (k > m) | (n < k) | (n-k > N-m))){
            //((x != roundDecimal (x)) | (x < 0) | (x > m) | (n < x) | (n-x > t-m));
            int min_m_n = Math.min(m, n);
            //CHeck to see that this is mathmatically correct
            min_m_n = Math.min(N, min_m_n);
            for(int kp = k; kp<=min_m_n; kp++){
                double logScore = logNChooseKG(m, kp)
                        + logNChooseKG((N-m), (n-kp))
                        - logNChooseKG(N, n);

                //Kindve irrelevant to do this but to be conservative,
                //since a low P-value is better, use the log of the
                //lowest double rather then just adding 0
                if (logScore<LOG_MIN_DOUBLE){  logScore = LOG_MIN_DOUBLE; }
                score += Math.exp(logScore);
            }
        }
        return score;
    }

    /**
     * Perform an interpolated HG test
     * @param N Total number of objects in universe
     * @param m Number of objects in category
     * @param n Number of object sin test set
     * @param k Overlap of objects
     * @return probablity that the null hypothesis is true
     *          (null hypothesis - this overalap is unrelated to the properties of the test set)
     */
    public static double hyperGeometricInterpolated(int N, int m, int n, int k){
        double score = 0;
        if((m < 0) | (N < 0) | (n <= 0) | (m > N) | (n > N)){
            score = Double.NaN;
        }else if(!((k < 0) | (k > m) | (n < k) | (n-k > N-m))){
            //((x != roundDecimal (x)) | (x < 0) | (x > m) | (n < x) | (n-x > t-m));
            int min_m_n = Math.min(m, n);
            //CHeck to see that this is mathmatically correct
            min_m_n = Math.min(N, min_m_n);
            for(int kp = k+1; kp<=min_m_n; kp++){
                double logScore = logNChooseKG(m, kp)
                        + logNChooseKG((N-m), (n-kp))
                        - logNChooseKG(N, n);

                //Kindve irrelevant to do this but to be conservative,
                //since a low P-value is better, use the log of the
                //lowest double rather then just adding 0
                if (logScore<LOG_MIN_DOUBLE){  logScore = LOG_MIN_DOUBLE; }
                score += Math.exp(logScore);
            }
        }

        //Add half of the simple
        return score + 0.5*hyperGeometricPoint(N,m,n,k);
    }

    public static double hyperGeometricInterpolated2(int N, int m, int n, int k){
        double score = 0;
        if((m < 0) | (N < 0) | (n <= 0) | (m > N) | (n > N)){
            score = Double.NaN;
        }else if(!((k < 0) | (k > m) | (n < k) | (n-k > N-m))){
            //((x != roundDecimal (x)) | (x < 0) | (x > m) | (n < x) | (n-x > t-m));
            int min_m_n = Math.min(m, n);
            //CHeck to see that this is mathmatically correct
            min_m_n = Math.min(N, min_m_n);
            for(int kp = k+1; kp<=min_m_n; kp++){
                double logScore = logNChooseKG(m, kp)
                        + logNChooseKG((N-m), (n-kp))
                        - logNChooseKG(N, n);

                //Kind've irrelevant to do this but to be conservative,
                //since a low P-value is better, use the log of the
                //lowest double rather then just adding 0
                if (logScore<LOG_MIN_DOUBLE){  logScore = LOG_MIN_DOUBLE; }
                score += Math.exp(logScore);
            }
        }

        //Estimate Interpolated Value
        //1/4(Xk + X(k+1/2)
        return score + 0.25d*(hyperGeometricPoint(N,m,n,k)+hyperGeometricPoint(N,m,n,k+0.5d));
    }

    public static double hyperGeometricPoint(double N, double m, double n, double k){
        double score = 0;
        if((m < 0) | (N < 0) | (n <= 0) | (m > N) | (n > N)){
            score = Double.NaN;
        }else if(!((k < 0) | (k > m) | (n < k) | (n-k > N-m))){
            double logScore = logNChooseKG(m, k)
                    + logNChooseKG((N-m), (n-k))
                    - logNChooseKG(N, n);

            score = Math.exp(logScore);
        }
        return score;
    }

    public static double roundDecimal(final double value, final int numberOfDigitsAfterDecimalPoint) {
        if (Double.isNaN(value) || Double.isInfinite(value)) {
            return value;
        }
        return new BigDecimal(value).setScale(numberOfDigitsAfterDecimalPoint,
                BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * Basic sum of int[]. Java 8 will probably make this unnecessary.
     * @param arr
     * @return
     */
    public static int sum(final int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

}



