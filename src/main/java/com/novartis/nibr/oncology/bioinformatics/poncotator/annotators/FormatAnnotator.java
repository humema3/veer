package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.google.common.collect.Sets;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.GenotypeBuilder;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.GenotypesContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContextBuilder;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFCompoundHeaderLine;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFFormatHeaderLine;
import htsjdk.variant.variantcontext.Genotype;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Set;

/**
 * Implementation of Annotator for VCF FORMAT fields.
 *
 * Created by humema3 on 10/13/2015.
 */
public abstract class FormatAnnotator extends Annotator {
    private static final Logger logger = Logger.getLogger(FormatAnnotator.class);
    static {
        logger.setLevel(Level.INFO);
    }

    public abstract VCFFormatHeaderLine getVCFFormatHeaderLine();

    @Override
    public VCFCompoundHeaderLine getVCFHeaderLine() {
        return getVCFFormatHeaderLine();
    }

    @Override
    public VariantContext createNewVariantContext(VariantContext variantContext) {
        logger.debug(variantContext.getChr() + "," + variantContext.getStart() + ": start");

        VariantContextBuilder variantContextBuilder = new VariantContextBuilder(variantContext);

        final GenotypesContext newGenotypes = GenotypesContext.create();
        final Set<AnnotWarning.ID> allMessages = Sets.newHashSet();
        for (final Genotype genotype : variantContext.getGenotypes()) {
            logger.debug(variantContext.getChr() + "," + variantContext.getStart()
                    + "," + genotype.getSampleName() + ": start");

            final AnnotatorValueHolder annotationHolder
                    = buildAnnotationValue(variantContext, genotype);

            // don't use Assert.notNull here because it will evaluate the message string first and the toString calls are expensive
            if (annotationHolder == null) {
                throw new IllegalArgumentException("Null value returned from " + this.getClass().getName() + " in context " + variantContext + " for genotype " + genotype);
            }

            final Genotype newGenotype;
            if(!annotationHolder.getValue().isEmpty()) {
                logger.debug(variantContext.getChr() + "," + variantContext.getStart()
                        + "," + genotype.getSampleName() + ": value found");
                newGenotype = new GenotypeBuilder(genotype)
                        .attribute(getVCFFormatHeaderLine().getID(), annotationHolder.getValue())
                        .make();
                allMessages.addAll(annotationHolder.getMessages());
            } else {
                logger.debug(variantContext.getChr() + "," + variantContext.getStart()
                        + "," + genotype.getSampleName() + ": no value");
                newGenotype = genotype;
            }
            newGenotypes.add(newGenotype);

            logger.debug(variantContext.getChr() + "," + variantContext.getStart()
                    + "," + genotype.getSampleName() + ": end");
        }
        variantContextBuilder.genotypes(newGenotypes);

        if(!allMessages.isEmpty()) {
            logger.debug(variantContext.getChr() + "," + variantContext.getStart() + ": adding messages");
            final AnnotatorValueHolder messagePlaceholder = new AnnotatorValueHolder("");
            messagePlaceholder.addWarnings(allMessages);
            variantContextBuilder.attribute(ANNOTATOR_MSG_INFO.getID(), messagePlaceholder.getMessageString());
        }

        logger.debug(variantContext.getChr() + "," + variantContext.getStart() + ": end");
        if (next() != null) {
            return next().createNewVariantContext(variantContextBuilder.make());
        }
        return variantContextBuilder.make();
    }

    protected abstract AnnotatorValueHolder buildAnnotationValue(VariantContext variantContext,
                                                                 Genotype genotype);

}
