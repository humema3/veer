package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.CheckVariantReferenceAlleles;
import htsjdk.samtools.reference.FastaSequenceIndex;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.File;

/**
 * Created by humema3 on 10/27/2015.
 */
@Component
//@ActionType("PoncTools")
public class CheckVariantReferenceAllelesExecutor extends VEERActionExecutor {

//    @Autowired
    private CheckVariantReferenceAlleles checkVariantReferenceAlleles;

    @Autowired
    public CheckVariantReferenceAllelesExecutor(final CheckVariantReferenceAlleles checkVariantReferenceAlleles) { this.checkVariantReferenceAlleles = checkVariantReferenceAlleles; }

    @Override
    protected Options getOptions() {
        final Options options = super.getOptions();
//        options.addOption("v", "vcf_list_file", true, "The input file listing 1 VCF filepath on each line with no other content");
//        options.getOption("v").setRequired(true);
//        options.addOption("header", "includes_header", true, "Indicates whether the input file has a header line [true|false]. Defaults to true.");
//        options.getOption("header").setRequired(false);
        options.addOption("report", "report_file", true, "Path to generated error report file");
        options.getOption("report").setRequired(true);
//        options.addOption("ref", true, "Reference genome FASTA file");
        options.addOption("index", "index_file", true, "Index file for reference genome. If not supplied, defaults to reference filepath (including extension) + '.fai' extension");
        options.getOption("index").setRequired(false);
        options.addOption("edit", "edit_vcf", false, "If specified, erroneous variants will be removed from the VCFs (or from generated copies) in addition to simply being reported.");
        options.getOption("edit").setRequired(false);
        options.addOption("inplace", "edit_in_place", false, "Edit VCF files in place. If not supplied, copies will be made in the directory specified by the copydir argument. " +
                "Be absolutely sure you don't need the original file contents before using this option!");
        options.getOption("inplace").setRequired(false);
        options.addOption("copydir", "vcf_copy_dir", true, "Directory where copies of error-containing VCFs will be stored. Should not be the same as the directory where any of the input VCFs are stored."
                + " Ignored if inplace flag is supplied.");
        options.getOption("copydir").setRequired(false);
        options.addOption("copylist", "vcf_copy_list_file", true, "Path to edited copy of VCF list file, if -edit flag is supplied. Ignored if -inplace flag is supplied.");
        options.getOption("copylist").setRequired(false);
        return options;
    }

    @Override
    public void build(final String[] args) throws PipelineException {
        final Options options = getOptions();
//        options.addOption("type", true, "The action type");

        try {
            super.build(args);
        } catch (final PipelineException e) {
            throwIfUnrecognized(e);
        }

        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd = processArgs(parser, options, args);
//        final String[] required = { "type", "v", "report", "ref" };
//        checkArgs(cmd, (String[]) required, options);

//        final File vcfListFile = new File(cmd.getOptionValue("v"));
//        final boolean hasHeader = !cmd.hasOption("h") || Boolean.parseBoolean(cmd.getOptionValue("h"));
        final File outFile = new File(cmd.getOptionValue("report"));
//        final File refFile = new File(cmd.getOptionValue("ref"));
        final String refFilepath = checkVariantReferenceAlleles.getReferenceGenomeFilepath();
        final File indexFile = new File(cmd.hasOption("index") ? cmd.getOptionValue("index") : refFilepath + ".fai");
        final FastaSequenceIndex index = new FastaSequenceIndex(indexFile);
        final IndexedFastaSequenceFile fsf = new IndexedFastaSequenceFile(new File(refFilepath), index);

//        checkVariantReferenceAlleles.setVcfListFile(vcfListFile);
//        checkVariantReferenceAlleles.setHasHeader(hasHeader);
        checkVariantReferenceAlleles.setIndexedReferenceGenome(fsf);
        checkVariantReferenceAlleles.setReportFile(outFile);

        final boolean edit = cmd.hasOption("edit");
        checkVariantReferenceAlleles.setToEditFiles(edit);
        if (edit) {
            final boolean editInPlace = cmd.hasOption("inplace");
            checkVariantReferenceAlleles.setEditInPlace(editInPlace);
            if (!editInPlace) {
                if (!cmd.hasOption("copydir") && !cmd.hasOption("copylist")) {
                    usage("-edit flag supplied without -inplace flag, but -copydir or -copylist is missing", options);
                }
                final File copyDir = new File(cmd.getOptionValue("copydir"));
                final File copyList = new File(cmd.getOptionValue("copylist"));
                checkVariantReferenceAlleles.setCopyDir(copyDir);
                checkVariantReferenceAlleles.setCopyListFile(copyList);
            }
        }
    }

    @Override
    public void start() throws PipelineException {
        checkVariantReferenceAlleles.start();
    }

    @Override
    protected VEERPipelineAction getAction() {
        return checkVariantReferenceAlleles;
    }
}
