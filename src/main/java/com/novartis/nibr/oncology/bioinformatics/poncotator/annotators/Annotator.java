package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFCompoundHeaderLine;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.List;

/**
 * Created by humema3 on 10/13/2015.
 */
public abstract class Annotator implements Serializable {
    private final static Logger logger = Logger.getLogger(Annotator.class);

    public static final VCFInfoHeaderLine ANNOTATOR_MSG_INFO = AnnotWarning.INFO_HEADER;

    private Annotator next;

    public Annotator next(){
        return next;
    }

    public void setNext(Annotator annotator){
        this.next = annotator;
    }

    public void logFinalStatus() {
        logger.info("Finished " + this.getClass().getSimpleName());
    }

    public abstract VariantContext createNewVariantContext(VariantContext variantContext);

    public abstract VCFCompoundHeaderLine getVCFHeaderLine();
}
