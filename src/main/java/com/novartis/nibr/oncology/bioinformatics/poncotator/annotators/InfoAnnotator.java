package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContextBuilder;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFCompoundHeaderLine;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Implementation of Annotator for VCF INFO fields.
 *
 * Created by JONESMIC on 5/2/2014.
 */
public abstract class InfoAnnotator extends Annotator {
    private final static Logger logger = Logger.getLogger(InfoAnnotator.class);
    static {
        logger.setLevel(Level.INFO);
    }

    public abstract VCFInfoHeaderLine getVCFInfoHeaderLine();

    @Override
    public VariantContext createNewVariantContext(VariantContext variantContext) {
        logger.debug(variantContext.getChr() + "," + variantContext.getStart() + ": start");

        final VariantContextBuilder variantContextBuilder = new VariantContextBuilder(variantContext);

        final AnnotatorValueHolder annotationHolder = buildAnnotationValue(variantContext);

        // don't use Assert.notNull here because it will evaluate the message string first and the toString calls are expensive
        if (annotationHolder == null) {
            throw new IllegalArgumentException("Null value returned from " + this.getClass().getName() + " in context " + variantContext);
        }

        if(!annotationHolder.getValue().isEmpty()){
            variantContextBuilder.attribute(getVCFInfoHeaderLine().getID(), annotationHolder.getValue());
        }

        if(annotationHolder.hasMessages()){
            variantContextBuilder.attribute(ANNOTATOR_MSG_INFO.getID()
                    ,annotationHolder.getMessageString());
        }

        logger.debug(variantContext.getChr() + "," + variantContext.getStart() + ": end");
        if (next() != null) {
            return next().createNewVariantContext(variantContextBuilder.make());
        }
        return variantContextBuilder.make();
    }

    @Override
    public VCFCompoundHeaderLine getVCFHeaderLine() {
        return getVCFInfoHeaderLine();
    }

    protected abstract AnnotatorValueHolder buildAnnotationValue(VariantContext variantContext);
}
