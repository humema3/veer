package com.novartis.nibr.oncology.bioinformatics.common.io;

/**
 *
 * User: jonesmic
 * Date: Mar 29, 2012
 * Time: 9:06:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Row {
    public String valueAt(String col);

    public String valueAt(int col);

    public int length();
}
