package com.novartis.nibr.oncology.bioinformatics.poncotator.model;


import com.google.common.collect.ComparisonChain;
import htsjdk.samtools.util.Interval;

/**
 *
 * Created by jonesmic on 3/3/14.
 */
public class MutationInterval implements Comparable<MutationInterval> {
    private final Interval interval;
    private final String ref;
    private final String alt;
    private final String suffix;

    public MutationInterval(Interval interval, String ref, String alt) {
        this.interval = interval;
        this.ref = ref;
        this.alt = alt;
        this.suffix = this.interval.getSequence().startsWith("chr") ? "g." : "g.chr";
    }

    public Interval getInterval() {
        return interval;
    }

    public String getRef() {
        return ref;
    }

    public String getAlt() {
        return alt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MutationInterval that = (MutationInterval) o;

        if (interval != null ? !interval.equals(that.interval) : that.interval != null) return false;
        if (alt != null ? !alt.equals(that.alt) : that.alt != null) return false;
        if (ref != null ? !ref.equals(that.ref) : that.ref != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = interval != null ? interval.hashCode() : 0;
        result = 31 * result + (ref != null ? ref.hashCode() : 0);
        result = 31 * result + (alt != null ? alt.hashCode() : 0);
        return result;
    }

    /**
     *
     * @return g.1:1178482A>G
     */
    @Override
    public String toString() {
        return suffix + interval.getSequence() + ":" + interval.getStart() + ref + ">" + alt;
    }

    public int compareTo(final MutationInterval that) {
        if (that == null) return -1; // nulls last

        return ComparisonChain.start().compare(this.interval, that.interval)
                .compare(this.getRef(), that.getRef())
                .compare(this.getAlt(), that.getAlt())
                .result();
    }

}
