package com.novartis.nibr.oncology.bioinformatics.poncotator.util;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFFileReader;
import com.novartis.nibr.oncology.bioinformatics.poncotator.annotators.Annotator;
import com.novartis.nibr.oncology.bioinformatics.poncotator.model.MutationInterval;
import htsjdk.samtools.util.Interval;
import htsjdk.variant.variantcontext.Allele;
import org.springframework.util.Assert;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Created by jonesmic on 3/3/14.
 */
public class VariantUtils {

    private static Pattern refSeqPattern = Pattern.compile("(\\w+_\\d+)(\\.\\d+)*");
    private final VCFFileReader vcfReader;

    public VariantUtils(VCFFileReader vcfFileReader) {
        this.vcfReader = vcfFileReader;
    }

    public static String extractVersionLessTranscriptID(String transcript) {
        Matcher m = refSeqPattern.matcher(transcript);
        Assert.isTrue(m.matches(), "Invalid Transcript ID [" + transcript + "] Transcript currently only supports RefSeq transcripts");
        return m.group(1);
    }

    public MutationInterval getMutationInterval(VariantContext variantContext) {
        String chr = variantContext.getChr();
        int start = variantContext.getStart();
        int end =  variantContext.getEnd();
        Interval interval = new Interval(chr, start, end);

        List<Allele> alleles = variantContext.getAlleles();
        Allele ref = alleles.get(0);
        Allele alt = alleles.get(1);

        return new MutationInterval(interval, ref.getBaseString(), alt.getBaseString());
    }

    public VariantContext buildAnnotatedVariantContext(VariantContext variantContext, Annotator annotatorChain) {
        return annotatorChain.createNewVariantContext(variantContext);
    }
}
