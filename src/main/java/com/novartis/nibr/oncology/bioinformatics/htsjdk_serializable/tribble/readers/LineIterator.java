package com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.tribble.readers;

import java.io.Serializable;
import java.util.Iterator;

/**
 * A very simple descriptor for line-iterables.
 * @author mccowan
 */
public interface LineIterator extends Iterator<String>, Serializable {
    /** Peeks at the next line, without expending any elements in the underlying iterator. */
    public String peek();
}

