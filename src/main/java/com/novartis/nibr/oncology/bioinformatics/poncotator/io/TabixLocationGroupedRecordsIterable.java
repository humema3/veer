package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import com.google.common.collect.Lists;
import htsjdk.tribble.readers.TabixReader;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Iterates through a tabix query iterator returning records grouped by location.
 * Created by jonesmic on 6/29/2015.
 */
public class TabixLocationGroupedRecordsIterable implements Iterable<TabixLocationGroupedRecords> {

    private final TabixReader.Iterator tabixIterator;

    public TabixLocationGroupedRecordsIterable(final TabixReader.Iterator tabixIterator) {
        this.tabixIterator = tabixIterator;
    }

    public Iterator<TabixLocationGroupedRecords> iterator() {
        return new TabixLocationGroupedRecordsIterator(tabixIterator);
    }


    private class TabixLocationGroupedRecordsIterator implements Iterator<TabixLocationGroupedRecords>{

        private final TabixReader.Iterator tabixIterator;
        private TabixRecord lastTabixRecord = null;
        private boolean buildNewGroup = true;
        private TabixLocationGroupedRecords currentTabixLocationGroupedRecords;


        public TabixLocationGroupedRecordsIterator(final TabixReader.Iterator tabixIterator) {
            this.tabixIterator = tabixIterator;
        }

        public boolean hasNext() {
            if(buildNewGroup){
                currentTabixLocationGroupedRecords = buildCurrentTabixLocationGroupedRecords();
                buildNewGroup = false;
            }
            return currentTabixLocationGroupedRecords != null;
        }

        private TabixLocationGroupedRecords buildCurrentTabixLocationGroupedRecords() {
            LinkedList<TabixRecord> records = Lists.newLinkedList();
            if(lastTabixRecord != null){
                records.add(lastTabixRecord);
            }

            String recordString;
            try {
                while ((recordString = tabixIterator.next()) != null) {
                    TabixRecord tabixRecord = createTabixRecord(recordString);
                    if(records.isEmpty()){
                        records.add(tabixRecord);
                    }else{
                        if(records.peekLast().getChr().equals(tabixRecord.getChr()) &&
                                records.peekLast().getPosition() == tabixRecord.getPosition()){
                            records.add(tabixRecord);
                        }else{
                            lastTabixRecord = tabixRecord;
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            }

            return new TabixLocationGroupedRecords(records.peek().getChr(), records.peek().getPosition(), records);
        }

        private TabixRecord createTabixRecord(String recordString) {
            String[] tabixRecordColumns = recordString.split("\t");
            String chr = tabixRecordColumns[0];
            int position = Integer.parseInt(tabixRecordColumns[1]);

            return new TabixRecord(chr, position, tabixRecordColumns);
        }

        public TabixLocationGroupedRecords next() {
            buildNewGroup = true;
            return currentTabixLocationGroupedRecords;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
