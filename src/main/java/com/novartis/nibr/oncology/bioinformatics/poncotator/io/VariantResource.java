package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import java.io.File;

import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.Allele;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

/**
 * A generic interface to handle any type of variant.
 * Created by jonesmic on 7/8/2014.
 */
public abstract class VariantResource {
    private final static Logger logger = Logger.getLogger(VariantResource.class);


    public enum QueryType {Index, Iterative}

    private final String resourceName;
    private final File location;
    private final QueryType queryType;


    protected VariantResource(final String resourceName, final File location, final QueryType queryType) {
        Assert.isTrue(location.exists(), location + " for resource " + resourceName + " does not exist");

        this.resourceName = resourceName;
        this.location = location;
        this.queryType = queryType;

        logger.info("resourceName " + resourceName);
        logger.info("location " + location);
        logger.info("queryType " + queryType);

    }

    public abstract String query(String chr, int start, int end, Allele refAllele, Allele altAllele, String infoID, boolean altAlleleSpecific);

    public String getResourceName() {
        return resourceName;
    }

    public File getLocation() {
        return location;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public abstract boolean hasInfoLine(String infoField);

    public abstract VCFInfoHeaderLine getInfoHeaderLine(String infoField);

    @Override
    public String toString() {
        return location.toString();
    }

    public abstract CloseableIterator<VariantContext> iterator();
}
