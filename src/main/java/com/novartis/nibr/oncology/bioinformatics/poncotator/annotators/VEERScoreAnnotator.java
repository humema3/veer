package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.novartis.nibr.oncology.bioinformatics.common.util.MathUtils;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFFormatHeaderLine;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;

/**
 *
 * Created by HUMEMA3 on 10/7/2015.
 */
@Component
public class VEERScoreAnnotator extends FormatAnnotator {
    private static final Logger logger = Logger.getLogger(VEERScoreAnnotator.class);
    static {
        logger.setLevel(Level.INFO);
    }

    public static final VCFFormatHeaderLine VCF_FORMAT_HEADER_LINE = new VCFFormatHeaderLine("VS", VCFHeaderLineCount.A,
        VCFHeaderLineType.Float, "p-value, odds ratio for hypergeometric test on Variant Empirical Error Rate");
    public static final String ID = VCF_FORMAT_HEADER_LINE.getID();

    static final int NUM_DECIMAL_PLACES = 2;
    private static final NumberFormat NUMBER_FORMATTER = new DecimalFormat("0." + StringUtils.repeat("#", NUM_DECIMAL_PLACES) + "E0");

    @Override
    protected AnnotatorValueHolder buildAnnotationValue(VariantContext variantContext, Genotype genotype) {
        if (!genotype.hasAD()) {
            return new AnnotatorValueHolder("");
        }
        final Optional<int[][]> backgroundCountsOpt = getVEERCounts(variantContext);
        if (!backgroundCountsOpt.isPresent()) {
            return new AnnotatorValueHolder("");
        }
        final int[][] backgroundCounts = backgroundCountsOpt.get();
        final int numAlleles = variantContext.getAlleles().size();
        final int numAltAlleles = numAlleles - 1;
        Assert.isTrue(backgroundCounts.length == numAltAlleles);
        final int[] sampleCounts = genotype.getAD();
        Assert.isTrue(sampleCounts.length == numAlleles);

        final StringBuilder sb = new StringBuilder();
        for (int i = 1; i < numAlleles; i++) {
            int N = backgroundCounts[i-1][0] + backgroundCounts[i-1][1];
            int m = backgroundCounts[i-1][1];
            int n = sampleCounts[0] + sampleCounts[i];
            int k = sampleCounts[i];

            // pseudocounts
            m += 1;
            k += 1;
            N += 2; // N - (m+1) + 2 = N - m + 1
            n += 2;

            final String oddsRatio = getRoundedOddsRatio(N, n, m, k);
            final String pval = getRoundedHypergeometricPValue(N, n, m, k);

            sb.append(pval + "|" + oddsRatio + ",");
        }
        sb.setLength(sb.length() - 1);
        return new AnnotatorValueHolder(sb.toString());
    }

    final String getRoundedHypergeometricPValue(final int N, final int n, final int m, final int k) {
        final double pval = MathUtils.hyperGeometricInterpolated(N, n, m, k);
        if (!Double.isNaN(pval) && !Double.isInfinite(pval)) {
            return NUMBER_FORMATTER.format(pval);
        } else {
            return String.valueOf(pval);
        }
    }

    private Optional<int[][]> getVEERCounts(final VariantContext variant) {
        final Optional<String> annotationOpt = getVEERCountsString(variant);
        if (!annotationOpt.isPresent()) {
            return Optional.absent();
        }
        final String annotation = annotationOpt.get();
        Assert.notNull(annotation);
        final String[] alleleAnnotations = annotation.split(",");
        final int[][] counts = new int[alleleAnnotations.length][2];
        for (int i = 0; i < alleleAnnotations.length; i++) {
            final String[] countStrings = alleleAnnotations[i].split("\\|");
            Assert.isTrue(countStrings.length == 2);
            for (int j = 0; j < 2; j++) {
                counts[i][j] = Integer.parseInt(countStrings[j]);
            }
        }
        return Optional.of(counts);
    }

    private Optional<String> getVEERCountsString(final VariantContext variantContext) {
        final Collection<String> possibleAttributes = getPossibleVEERCountsAttributes(variantContext);
        if (possibleAttributes.isEmpty()) { // don't use Assert - variantContext.toString is expensive
//            throw new IllegalArgumentException("Trying to annotate a variant with VEER scores that does not have the counts: " + variantContext.toString());
            return Optional.absent();
        }
        Assert.isTrue(possibleAttributes.size() == 1, "More than one resource apparently used to add VEER counts: " + possibleAttributes.toString());
        final String attribute = possibleAttributes.iterator().next();
        if (!variantContext.hasAttribute(attribute)) { // don't use Assert - variantContext.toString is expensive
//            throw new IllegalArgumentException("Trying to annotate a variant with VEER scores that does not have the counts: " + variantContext.toString());
            return Optional.absent();
        }
        final Object rawAnnotationValue = variantContext.getAttribute(attribute);
        Assert.isTrue(rawAnnotationValue instanceof String);
        final String annotationString = (String) rawAnnotationValue;
        if (annotationString.equals("") || annotationString.equals(",")) { // don't use Assert - variantContext.toString is expensive
//            throw new IllegalArgumentException("Trying to annotate a variant with VEER scores that does not have the counts: " + variantContext.toString());
            return Optional.absent();
        }
        return Optional.of(annotationString);
    }

    private Collection<String> getPossibleVEERCountsAttributes(final VariantContext variantContext) {
        final Predicate<String> isVEERCountAnnotation = new Predicate<String>() {
            public boolean apply(final String input) {
                return input.endsWith(VEERCountAnnotator.ID);
            }
        };
        return Collections2.filter(variantContext.getAttributes().keySet(), isVEERCountAnnotation);
    }

    String getRoundedOddsRatio(final int N, final int n, final int m, final int k) {
        final double oddsRatio = getOddsRatio(N, n, m, k);
//        return MathUtils.roundDecimal(oddsRatio, NUM_DECIMAL_PLACES);
        return NUMBER_FORMATTER.format(oddsRatio);
    }

    private double getOddsRatio(final int N, final int n, final int m, final int k) {
        return (double) ((N - m) * k) / ((n - k) * m);
    }

    @Override
    public VCFFormatHeaderLine getVCFFormatHeaderLine() {
        return VCF_FORMAT_HEADER_LINE;
    }
}
