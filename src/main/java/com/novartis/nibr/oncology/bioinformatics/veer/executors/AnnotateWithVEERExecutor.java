package com.novartis.nibr.oncology.bioinformatics.veer.executors;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.AnnotateWithVEER;
import com.novartis.nibr.oncology.bioinformatics.veer.actions.VEERPipelineAction;
import com.novartis.nibr.oncology.bioinformatics.veer.exceptions.PipelineException;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class AnnotateWithVEERExecutor extends VEERActionExecutor {

//    private static final int DEFAULT_NUM_FILES = 1;

    private final AnnotateWithVEER annotateWithVEER;

    @Autowired
    public AnnotateWithVEERExecutor(final AnnotateWithVEER annotateWithVEER) {
        this.annotateWithVEER = annotateWithVEER;
    }

    @Override
    protected Options getOptions() {
        final Options options = super.getOptions();
//        options.addOption("type", true, "The action type");
//        options.addOption("o", true, "The directory for annotation output");
//        options.addOption("ref", true, "The reference FastA");
//        options.addOption("n", "num_files", true, "The number of files over which the input is parallelized. Input VCF files should start with an integer from 1 to this number inclusive (see -f argument). Default is 1 (no parallelization).");
//        options.getOption("n").setRequired(false);
//        options.addOption("f", "input_filename", true, "The filename for all the input VCF files. If not supplied, all files in the directory will be used. If supplied, each input file should be called [i]_[filename].vcf, where [i] is an integer between " +
//                "1 and the '-n' argument, and [filename] is this argument. If you supply -n 1, the '[i]_' prefix is optional.");
//        options.getOption("f").setRequired(false);
//        options.addOption("t", true, "Max running time in seconds (default is " + DEFAULT_MAX_RUNTIME + ")");
//        options.addOption("c", "counts_only", false, "Annotate with counts only (no scores)");
//        options.getOption("c").setRequired(false);
        options.addOption("res", "counts_resource", true, "Counts resource VCF. Default is to use input VCF file.");
        options.getOption("res").setRequired(false);
        options.addOption("a", "annotator", true, "One or more annotator types");
        options.getOption("a").setRequired(false);
//        options.addOption("cv", "run_combine_variants", false, "Run CombineVariants step after finishing. Only used if -f and -n > 1 arguments are supplied.");
//        options.getOption("cv").setRequired(false);
        return options;
    }

    @Override
    public void build(final String[] args) throws PipelineException {
        super.build(args);
        final Options options = getOptions();
        final CommandLineParser parser = new PosixParser();
        final CommandLine cmd = processArgs(parser, options, args);
//        final String[] required = {"type", "o", "ref", "d"};
//        checkArgs(cmd, required, options);
//        checkToolArg(cmd, options);

//        final File outPath = new File(cmd.getOptionValue("o"));
//        checkOutputPath(outPath);
//        final File reference = new File(cmd.getOptionValue("ref"));
//        final File inputDir = new File(cmd.getOptionValue("d"));
//        final Optional<String> inputFilename = cmd.hasOption("f") ? Optional.of(cmd.getOptionValue("f")) : Optional.<String>absent();
//        final int numFiles = cmd.hasOption("n") ? Integer.parseInt(cmd.getOptionValue("n")) : DEFAULT_NUM_FILES;
//        Assert.isTrue(numFiles >= 1, "-n argument should be an integer 1 or higher");
//        final Optional<String> countsResource = cmd.hasOption("res") ? Optional.of(cmd.getOptionValue("res")) : Optional.<String>absent();
//        final boolean countsOnly = cmd.hasOption("c");
//        final boolean combineVariants = cmd.hasOption("cv");
//        final String email = cmd.hasOption("email") ? cmd.getOptionValue("email") : DEFAULT_EMAIL;

//        annotateWithVEER.setOutputDirPath(outPath);
//        annotateWithVEER.setReference(reference);
//        annotateWithVEER.setNumFiles(numFiles);
//        annotateWithVEER.setInputDir(inputDir);
//        annotateWithVEER.setInputFilename(inputFilename);
//        annotateWithVEER.setAnnotateScores(!countsOnly);
//        annotateWithVEER.setCountsResource(countsResource);
//        annotateWithVEER.setEmail(email);
//        annotateWithVEER.setRunCombineVariants(combineVariants);
//        annotateWithVEER.setSparkMaster(getSparkMaster());
        if (cmd.hasOption("res")) {
            annotateWithVEER.setResource(cmd.getOptionValue("res"));
        }
        annotateWithVEER.setAnnotators(cmd.getOptionValues("a"));
//        printArgs(args, annotateWithVEER.getOutputDirPath(), options, getActionType().name());
//        annotateWithVEER.promptForSubmit();
        annotateWithVEER.setSparkContext(getSparkContext());
    }

    @Override
    protected VEERPipelineAction getAction() {
        return annotateWithVEER;
    }

//    Optional<String> getCountsResource() {
//        return annotateWithVEER.getCountsResource();
//    }
//    void setAnnotateScores(final boolean annotateScores) {
//        annotateWithVEER.setAnnotateScores(annotateScores);
//    }
//    void setCountsResource(final Optional<String> countsResource) {
//        annotateWithVEER.setCountsResource(countsResource);
//    }
    void setAnnotations(final String[] annotations) {
        annotateWithVEER.setAnnotators(annotations);
    }
    void setResource(final String resource) {
        annotateWithVEER.setResource(resource);
    }
//    void setRunCombineVariants(final boolean runCombineVariants) {
//        annotateWithVEER.setRunCombineVariants(runCombineVariants);
//    }
}
