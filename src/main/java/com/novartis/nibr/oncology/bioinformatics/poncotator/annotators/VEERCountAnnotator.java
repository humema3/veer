package com.novartis.nibr.oncology.bioinformatics.poncotator.annotators;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import com.novartis.nibr.oncology.bioinformatics.common.util.MathUtils;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.GenotypesContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.variantcontext.VariantContext;
import com.novartis.nibr.oncology.bioinformatics.htsjdk_serializable.variant.vcf.VCFInfoHeaderLine;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * Created by HUMEMA3 on 10/6/2015.
 */
@Component
public class VEERCountAnnotator extends InfoAnnotator implements Serializable {
    private static final Logger logger = Logger.getLogger(VEERCountAnnotator.class);
    static {
        logger.setLevel(Level.INFO);
    }

    public static final VCFInfoHeaderLine INFO_HEADER_LINE = new VCFInfoHeaderLine("VEERCounts", VCFHeaderLineCount.A, VCFHeaderLineType.Integer,
            "Variant Empirical Error Rate counts (REF|ALT1[,REF|ALT2,...])");
    public static final String ID = INFO_HEADER_LINE.getID();

    private static final double PERCENTILE_CUTOFF = 90.0;

    protected AnnotatorValueHolder buildAnnotationValue(VariantContext variantContext) {
        final GenotypesContext genotypesContext = variantContext.getGenotypes();
        final int numSamples = genotypesContext.size();
        final int numAlleles = variantContext.getAlleles().size();
        final int numAltAlleles = numAlleles - 1;

        final AlleleInfo[][] allAltAlleleInfo = new AlleleInfo[numAltAlleles][numSamples];
        for (int sampleNum = 0; sampleNum < numSamples; sampleNum++) {
            final Genotype genotype = genotypesContext.get(sampleNum);
            for (int altAlleleNum = 0; altAlleleNum < numAltAlleles; altAlleleNum++) {
                final AlleleInfo altAlleleInfo = buildAltAlleleInfo(genotype, altAlleleNum, numAlleles);
                allAltAlleleInfo[altAlleleNum][sampleNum] = altAlleleInfo;
            }
        }

        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numAltAlleles; i++) {
            final String counts = getCountString(Arrays.asList(allAltAlleleInfo[i]));
            sb.append(counts + ",");
        }
        sb.setLength(sb.length() - 1);

        return new AnnotatorValueHolder(sb.toString());
    }

    private AlleleInfo buildAltAlleleInfo(final Genotype genotype, final int altAlleleNum, final int numAlleles) {
        Assert.isTrue(altAlleleNum >= 0 && altAlleleNum < numAlleles - 1);
        if (genotype.hasAD()) {
            final int[] alleleDepths = genotype.getAD();
            Assert.isTrue(alleleDepths.length == numAlleles);
            final int refDepth = alleleDepths[0];
            final int altDepth = alleleDepths[altAlleleNum + 1];
            return new AlleleInfo(refDepth, altDepth);
        } else {
            return new AlleleInfo(0, 0);
        }
    }

    private void populateMatrixRows(final int sampleNum, final Genotype genotype, final int numAlleles, final int[][] allAlleleDepths, final double[][] allAlleleBalances) {
        final int numAltAlleles = numAlleles - 1;

        // initialize rows
        allAlleleDepths[sampleNum] = new int[numAlleles];
        for (int allele = 0; allele < numAlleles; allele++) {
            allAlleleDepths[sampleNum][allele] = 0;
        }
        allAlleleBalances[sampleNum] = new double[numAltAlleles];
        for (int allele = 0; allele < numAltAlleles; allele++) {
            allAlleleBalances[sampleNum][allele] = Double.NaN;
        }

        if (genotype.hasAD()) {
            final int[] alleleDepths = genotype.getAD();
            Assert.isTrue(alleleDepths.length == numAlleles);
            if (MathUtils.sum(alleleDepths) > 0) {
                allAlleleDepths[sampleNum] = alleleDepths;
                for (int allele = 1; allele < numAlleles; allele++) {
                    allAlleleBalances[sampleNum][allele-1] = (double) alleleDepths[allele] / (alleleDepths[0] + alleleDepths[allele]);
                }
            }
        }
    }

    private String getCountString(final List<AlleleInfo> info) {
        final List<AlleleInfo> numericInfo = Lists.newArrayList(Collections2.filter(info, new Predicate<AlleleInfo>() {
            public boolean apply(AlleleInfo input) {
                return !Double.isNaN(input.alleleBalance);
            }
        }));
//        final List<AlleleInfo> unsortedNumericInfo = ImmutableList.copyOf(numericInfo);
        Collections.sort(numericInfo, new Comparator<AlleleInfo>() {
            public int compare(AlleleInfo o1, AlleleInfo o2) {
                return ComparisonChain.start()
                        .compare(o1.alleleBalance, o2.alleleBalance)
                        .compare(o1.alleleDepth + o1.refDepth, o2.alleleDepth + o2.refDepth)
                        .result();
            }
        });

        final double rankThreshold = (PERCENTILE_CUTOFF / 100) * numericInfo.size();

        int refCount = 0;
        int altCount = 0;
        for (int i = 0; i < rankThreshold; i++) {
            final AlleleInfo alleleInfo = numericInfo.get(i);
            refCount += alleleInfo.refDepth;
            altCount += alleleInfo.alleleDepth;
        }

        return refCount + "|" + altCount;

    }

    @Override
    public VCFInfoHeaderLine getVCFInfoHeaderLine() {
        return INFO_HEADER_LINE;
    }

    private class AlleleInfo {
        private final int refDepth;
        private final int alleleDepth;
        private final double alleleBalance;
        private AlleleInfo(final int refDepth, final int alleleDepth) {
            this.refDepth = refDepth;
            this.alleleDepth = alleleDepth;
            this.alleleBalance = (double) alleleDepth / (refDepth + alleleDepth);
        }
    }

}
