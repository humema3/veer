package com.novartis.nibr.oncology.bioinformatics.poncotator.io;

import htsjdk.variant.variantcontext.Allele;

import java.util.List;

/**
 * Created by jonesmic on 6/29/2015.
 *
 * Holds all of the tabix records at a specific location.
 */
public class TabixLocationGroupedRecords {

    private final String chr;
    private final int position;
    private final List<TabixRecord> records;

    public TabixLocationGroupedRecords(String chr, int position, List<TabixRecord> records) {
        this.chr = chr;
        this.position = position;
        this.records = records;
    }

    public String getChr() {
        return chr;
    }

    public int getPosition() {
        return position;
    }

    public List<TabixRecord> getRecords() {
        return records;
    }

    public TabixRecord query(Allele refAllele, Allele altAllele) {
        for(TabixRecord tabixRecord : records){
            if(tabixRecord.getRefAllele().equals(refAllele) && tabixRecord.getAltAllele().equals(altAllele)){
                return tabixRecord;
            }
        }
        return null;
    }
}
