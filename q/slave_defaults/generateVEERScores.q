# The GenerateVEERScores slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/6_GenerateVEERScores/$TIMESTAMP
$PREV_STEP 5_RecallVariants
mkdir -p $RUN_DIRECTORY/output/1
mkdir -p $RUN_DIRECTORY/output/2
qsub $SCRIPT_BASE/GenerateVEERScores/copy_files.sh
$INPUT_FILENAME recallVariants
$OUTPUT_SUFFIX annotateWithVEER
$NUM_FILES $NUM_CONTIGS
qsub $SCRIPT_BASE/GenerateVEERScores/generateVEERScores.sh
$INPUT_FILENAME recallVariants_annotateWithVEER
$OUTPUT_FILENAME VEER_annotation_resource
qsub $SCRIPT_BASE/GenerateVEERScores/finish_generateVEERScores.sh