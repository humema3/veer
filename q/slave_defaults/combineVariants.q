# The CombineVariants slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$PREV_STEP 1_CheckVariantReferenceAlleles
$RUN_DIRECTORY $RUN_DIRECTORY/2_CombineVariants/$TIMESTAMP
mkdir -p $RUN_DIRECTORY/output
qsub $SCRIPT_BASE/CombineVariants/copy_files.sh
$OUTPUT_SUFFIX combineVariants
qsub $SCRIPT_BASE/CombineVariants/combineVariants.sh