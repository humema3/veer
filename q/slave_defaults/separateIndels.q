# The SeparateIndels slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/3_SeparateIndels/$TIMESTAMP
mkdir -p $RUN_DIRECTORY/output
$PREV_STEP 2_CombineVariants
$FILE_SUFFIX combineVariants
qsub $SCRIPT_BASE/SeparateIndels/copy_files.sh
qsub $SCRIPT_BASE/SeparateIndels/separateIndels.sh