# The CheckVariantReferenceAlleles slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see http://web.global.nibr.novartis.net/apps/confluence/display/ONCOR/VEER+error+correction+pipeline

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/1_CheckVariantReferenceAlleles/$TIMESTAMP
qsub $SCRIPT_BASE/CheckVariantReferenceAlleles/copy_files.sh
mkdir -p $RUN_DIRECTORY/output
$ORIGINAL_VCF_LIST_FILENAME old_vcf_list.txt
$NEW_VCF_LIST_FILENAME new_vcf_list.txt
$REPORT_FILENAME check_report.txt
qsub $SCRIPT_BASE/CheckVCFs/createVcfListFile.sh
qsub $SCRIPT_BASE/CheckVCFs/fixVcfBugs.sh
qsub $SCRIPT_BASE/CheckVCFs/finish_fixVcfBugs.sh
qsub $SCRIPT_BASE/CheckVCFs/checkVariantReferenceAlleles.sh
qsub $SCRIPT_BASE/CheckVCFs/finish_checkVariantReferenceAlleles.sh