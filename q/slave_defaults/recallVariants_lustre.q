# The RecallVariants slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$ORIGINAL_RUN_DIRECTORY $RUN_DIRECTORY/5_RecallVariants/$TIMESTAMP
mkdir -p $ORIGINAL_RUN_DIRECTORY/output/1
mkdir -p $ORIGINAL_RUN_DIRECTORY/output/2
$USER run whoami
$RUN_DIRECTORY /flock/scratch/$USER/RecallVariants/$DATASET/$TIMESTAMP
$PREV_STEP 4_RemoveFilterCalls
qsub $SCRIPT_BASE/RecallVariants/copy_files_to_lustre.sh
qsub $SCRIPT_BASE/RecallVariants/copy_bam_files_to_lustre.sh
$INDEL_ALLELES_FILENAME combineVariants_indelsOnly_noFilterCalls
$SNV_ALLELES_FILENAME combineVariants_noIndels_noFilterCalls
$MEMORY 6G
$MAX_CONCURRENT_TASKS $NUM_SAMPLES
$OUTPUT_SUFFIX recallVariants
qsub $SCRIPT_BASE/RecallVariants/recallVariants_lustre.sh
$TEMP $RUN_DIRECTORY
$RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY
$ORIGINAL_RUN_DIRECTORY $TEMP
qsub $SCRIPT_BASE/RecallVariants/copy_files_from_lustre_1.sh
$TEMP $RUN_DIRECTORY
$RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY
$ORIGINAL_RUN_DIRECTORY $TEMP
qsub $SCRIPT_BASE/RecallVariants/finish_recallVariants_lustre.sh
$TEMP $RUN_DIRECTORY
$RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY
$ORIGINAL_RUN_DIRECTORY $TEMP
qsub $SCRIPT_BASE/RecallVariants/copy_files_from_lustre_2.sh