# Deprecated, use recallVariants_lustre.q

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/5_RecallVariants/$TIMESTAMP
$PREV_STEP 4_RecallVariants
mkdir -p $RUN_DIRECTORY/output
qsub $SCRIPT_BASE/RecallVariants/copy_files.sh
$INDEL_ALLELES_FILENAME combineVariants_indelsOnly_noFilterCalls
$SNV_ALLELES_FILENAME combineVariants_noIndels_noFilterCalls
$MEMORY 6G
$MAX_CONCURRENT_TASKS 500
$OUTPUT_SUFFIX recallVariants
qsub $SCRIPT_BASE/RecallVariants/recallVariants.sh
qsub $SCRIPT_BASE/RecallVariants/finish_recallVariants.sh