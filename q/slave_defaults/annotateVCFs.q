# Deprecated, use annotateVCFs_lustre.q

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/7_AnnotateVCFs/$TIMESTAMP
mkdir -p $RUN_DIRECTORY/output
$OUTPUT_SUFFIX VEER
$RESOURCE_FILENAME VEER_annotation_resource
qsub $SCRIPT_BASE/AnnotateVCFs/annotateVCFs.sh