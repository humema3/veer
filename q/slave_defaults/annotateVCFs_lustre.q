# The AnnotateVCFs slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$ORIGINAL_RUN_DIRECTORY $RUN_DIRECTORY/7_AnnotateVCFs/$TIMESTAMP
mkdir -p $ORIGINAL_RUN_DIRECTORY/output
$USER run whoami
$RUN_DIRECTORY /flock/scratch/$USER/VEER_AnnotateVCFs/$DATASET/$TIMESTAMP
$RESOURCE_FILENAME VEER_annotation_resource
$OUTPUT_SUFFIX VEER
$PREV_STEP 6_GenerateVEERScores
qsub $SCRIPT_BASE/AnnotateVCFs/copy_files_to_lustre.sh
qsub $SCRIPT_BASE/AnnotateVCFs/annotateVCFs_lustre.sh
$TEMP $RUN_DIRECTORY
$RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY
$ORIGINAL_RUN_DIRECTORY $TEMP
qsub $SCRIPT_BASE/AnnotateVCFs/copy_files_from_lustre.sh