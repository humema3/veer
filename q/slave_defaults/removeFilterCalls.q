# The RemoveFilterCalls slave file for use with q pipeline manager.
# Copy to "slaves" subdirectory in your analysis directory.
# For more details, see... # TODO

$TIMESTAMP run date +"%Y_%m_%d"
$RUN_DIRECTORY $RUN_DIRECTORY/4_RemoveFilterCalls/$TIMESTAMP
$PREV_STEP 3_SeparateIndels
mkdir -p $RUN_DIRECTORY/output
$FILE_SUFFIX noFilterCalls
qsub $SCRIPT_BASE/RemoveFilterCalls/copy_files.sh
qsub $SCRIPT_BASE/RemoveFilterCalls/removeFilterCalls.sh