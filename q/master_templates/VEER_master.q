# The VEER master file for use with q pipeline manager.
# Copy this into the root of your analysis directory.
# Fill in all values surrounded by <> and check that all default values are suitable.
# For more details, see http://web.global.nibr.novartis.net/apps/confluence/display/ONCOR/VEER+error+correction+pipeline

$RUN_DIRECTORY run pwd
$SAMPLE_DETAILS_FILE <sample_details_file>
$NUM_SAMPLES <num_samples>
$NUM_FILES_FOR_RECALL <num_samples * 2>
$DATASET <dataset>
$SCRIPT_BASE /home/humema3/code/VEER/q/scripts
$FIRST_STEP 1_CheckVariantReferenceAlleles
$NUM_CONTIGS 24
$NUM_FILES_FOR_SEPARATE_INDELS 48
$SMF_BAM_LIST_FILENAME smf_bam_list.txt

invoke slaves/checkVCFs.q
invoke slaves/combineVariants.q
invoke slaves/separateIndels.q
invoke slaves/removeFilterCalls.q
invoke slaves/recallVariants_lustre.q
invoke slaves/generateVEERScores.q
invoke slaves/annotateVCFs_lustre.q