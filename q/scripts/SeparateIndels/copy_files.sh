#!/bin/bash
#q require $RUN_DIRECTORY $PREV_STEP
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_SeparateIndels
#$ -l h_rt=3600
#$ -l m_mem_free=2G

# Copy and prep input files for the SeparateIndels step.
# Currently just link to the VCFs output from the last step.

PREV_STEP=$PREV_STEP
if [ -z "$PREV_STEP" ]; then
    echo "\$PREV_STEP is not specified" >&2
    exit 100
fi
if [ ! -d "../../$PREV_STEP" ]; then
    echo "\$PREV_STEP (../../$PREV_STEP) is not a directory" >&2
    exit 100
fi

mkdir -p input
cd input
LATEST_RUN=`ls -t ../../../$PREV_STEP | head -n 1`
rm *.vcf*
for vcf in ../../../$PREV_STEP/$LATEST_RUN/output/*.vcf*; do
    ln -s $vcf
done
