#!/bin/bash
#q require $RUN_DIRECTORY $FILE_SUFFIX $SCRIPT_BASE $NUM_FILES_FOR_SEPARATE_INDELS
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_FILES_FOR_SEPARATE_INDELS
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=10800
#$ -N SeparateIndels
#$ -binding linear:1

# Use GATK's SelectVariants to divide VCFs into 2 separate files for SNV and indel variants.
# Necessary to run GATK's UnifiedGenotyper downstream.
# Parallelized by chromosome and variant type.

FILE_SUFFIX=$FILE_SUFFIX
if [ -z "$FILE_SUFFIX" ]; then
    echo "\$FILE_SUFFIX not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE ($SCRIPT_BASE) is not a directory" >&2
    exit 100
fi
echo "Running from $PWD" >&2

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

stime=$(date '+%s')

# Num tasks = Num contigs * 2 (1 for SNVs, 1 for indels)
ID=$SGE_TASK_ID
# task id 1 = chr. 1 SNVs
# task id 2 = chr. 1 indels
# task id 3 = chr. 2 SNVs
# task id 4 = chr. 2 indels
# etc.
CONTIGS=( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y )
IDX=$(((ID-1)/2)) # in 1-based nomenclature, chromosome number = ceil(task_id/2)
CONTIG=${CONTIGS[$IDX]}
if [ "$CONTIG" == "X" ]; then
    OUTPUT_ID=23
elif [ "$CONTIG" == "Y" ]; then
    OUTPUT_ID=24
else
    OUTPUT_ID=$CONTIG
fi
if [ "$(expr $ID % 2)" -eq "0" ]; then
    OUTPUT_TYPE=SNV
else
    OUTPUT_TYPE=INDEL
fi

# Assign input/output filenames
INPUT_VCF=input/${OUTPUT_ID}_${FILE_SUFFIX}.vcf
INDEL_OUTPUT_VCF=output/${OUTPUT_ID}_${FILE_SUFFIX}_indelsOnly.vcf
SNV_OUTPUT_VCF=output/${OUTPUT_ID}_${FILE_SUFFIX}_noIndels.vcf

# Construct and run GATK command
GATK_PARAMS="--unsafe LENIENT_VCF_PROCESSING -L ${CONTIG} -R ${REFERENCE}"
CMD="$JAVA -jar $GATK $GATK_PARAMS -T SelectVariants -V $INPUT_VCF "
if [ "$OUTPUT_TYPE" == "INDEL" ]; then
    CMD+="-o $INDEL_OUTPUT_VCF --selectTypeToInclude INDEL"
else
    CMD+="-o $SNV_OUTPUT_VCF --selectTypeToExclude INDEL"
fi
echo $CMD
$CMD

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total SeparateIndels Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO
