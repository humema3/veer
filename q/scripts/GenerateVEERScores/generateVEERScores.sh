#!/bin/bash
#q require $RUN_DIRECTORY $NUM_FILES $INPUT_FILENAME $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_FILES
#$ -l m_mem_free=24G
#$ -l h_rt=345599
#$ -j y
#$ -N GenerateVEERScores
#$ -binding linear:1

# Run the VEER algorithm on the recalled variant VCFs.

INPUT_FILENAME=$INPUT_FILENAME
if [ -z "$INPUT_FILENAME" ]; then
    echo "\$INPUT_FILENAME not specified" >&2
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >&2
    exit 100
fi
NUM_FILES=$NUM_FILES
if [ -z "$NUM_FILES" ]; then
    echo "\$NUM_FILES not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >&2
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >&2
    exit 100
fi
echo "Running from $PWD" >&2

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

stime=$(date '+%s')

ID=$SGE_TASK_ID
INPUT_DIR=input/1
ANNOTATE_SCORES=1 # if set to 1, annotate scores as well as counts
COUNTS_RESOURCE= # if left empty, use input file itself as the resource

# Select the appropriate input file based on the task id
if [ ! -z $INPUT_FILENAME ] && [ "$NUM_FILES" -eq "1" ] && [ ! -f ${INPUT_DIR}/1_${INPUT_FILENAME}.vcf ] && [ -f ${INPUT_DIR}/${INPUT_FILENAME}.vcf ]; then
    # If a filename is supplied, and there's only one file, and it doesn't have a numeric prefix, use that file
    INPUT_VCF=${INPUT_DIR}/${INPUT_FILENAME}.vcf
elif [ ! -z $INPUT_FILENAME ]; then
    # Otherwise, if a filename is supplied, use the VCF with that filename prefixed by the task id
    INPUT_VCF="${INPUT_DIR}/${ID}_${INPUT_FILENAME}.vcf"
else
    # If no filename is supplied, use the i'th VCF in the input directory, where i = task id
    INPUT_VCF=`ls $INPUT_DIR/*.vcf | head -n $ID | tail -n 1`
fi
INPUT_INDEX=${INPUT_VCF}.idx

# Stage data in temporary /scratch directory to reduce I/O cost
TEMP_DIR=/scratch/`whoami`/tmpdir.${JOB_ID}.${ID}
mkdir -p $TEMP_DIR
checkForError $LINENO $?
cp $INPUT_VCF -t $TEMP_DIR
checkForError $LINENO $?
INPUT_VCF=${TEMP_DIR}/`basename $INPUT_VCF`
if [ -f $INPUT_INDEX ]; then
  cp $INPUT_INDEX -t $TEMP_DIR
  checkForError $LINENO $?
fi

# Stage resource file to /scratch if used
if [ ! -z $COUNTS_RESOURCE ]; then
    RESOURCE_INDEX=${COUNTS_RESOURCE}.idx
    LOCKFILE=/scratch/`whoami`/ponc.lock
    lockfile $LOCKFILE
    checkForError $LINENO $?
    if [ ! -f `dirname $TEMP_DIR`/`basename $COUNTS_RESOURCE` ]; then
      cp $COUNTS_RESOURCE -t `dirname $TEMP_DIR`
      checkForError $LINENO $?
      if [ -f $RESOURCE_INDEX ] && [ ! -f `dirname $TEMP_DIR`/`basename $RESOURCE_INDEX` ]; then
        cp $RESOURCE_INDEX -t `dirname $TEMP_DIR`
        checkForError $LINENO $?
      fi
    fi
    rm -f $LOCKFILE
    checkForError $LINENO $?
    COUNTS_RESOURCE=`dirname ${TEMP_DIR}`/`basename $COUNTS_RESOURCE`
fi

# define output filename
VCF_FILENAME="${INPUT_VCF%.*}"
OUTPUT_VCF=${VCF_FILENAME}_${OUTPUT_SUFFIX}.vcf
OUTPUT_INDEX=${OUTPUT_VCF}.idx

# Run Poncotator
CMD="$PONCOTATOR -o $OUTPUT_VCF -r $REFERENCE -vcf $INPUT_VCF"
if [ ! -z $COUNTS_RESOURCE ]; then
    CMD="$CMD -a Resource:VeerCountDB:VEERCounts -resource VeerCountDB:${COUNTS_RESOURCE}"
else
    CMD="$CMD -a VEERCounts"
fi
if [ "$ANNOTATE_SCORES" -eq "1" ]; then
    CMD="$CMD -a VS"
fi
echo $CMD
$CMD
checkForError $LINENO $?

# Move all output from temp directory
mv $OUTPUT_VCF -t output/1
checkForError $LINENO $?
if [ -f $OUTPUT_INDEX ]; then
  mv $OUTPUT_INDEX -t output/1
  checkForError $LINENO $?
fi

# Clean up temp directory
rm -r $TEMP_DIR
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total AnnotateWithVEER action run time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO $NUM_FILES