#!/bin/bash
#q require $RUN_DIRECTORY $PREV_STEP
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_GenerateVEERScores
#$ -l h_rt=3600
#$ -l m_mem_free=2G

# Copy and prep files for the GenerateVEERScores step.
# Currently just link to the output VCFs from the last step.

PREV_STEP=$PREV_STEP
if [ -z "$PREV_STEP" ]; then
    echo "\$PREV_STEP is not specified" >&2
    exit 100
fi
if [ ! -d "../../$PREV_STEP" ]; then
    echo "\$PREV_STEP (../../$PREV_STEP) is not a directory" >&2
    exit 100
fi

mkdir -p input/1
cd input/1
LATEST_RUN=`ls -t ../../../../$PREV_STEP | head -n 1`
rm *
for vcf in ../../../../$PREV_STEP/$LATEST_RUN/output/2/*; do
    ln -s $vcf
done
