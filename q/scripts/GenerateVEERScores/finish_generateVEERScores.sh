#!/bin/bash
#q require $RUN_DIRECTORY $INPUT_FILENAME $OUTPUT_FILENAME $NUM_FILES $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=43200
#$ -binding linear:1
#$ -N Finish_GenerateVEERScores

##############################################################################
# After running GenerateVEERScores we combine all the results from the individual
# chromosomes into one VCF.
##############################################################################

INPUT_FILENAME=$INPUT_FILENAME
if [ -z "$INPUT_FILENAME" ]; then
    echo "\$INPUT_FILENAME not specified" >&2
    exit 100
fi
OUTPUT_FILENAME=$OUTPUT_FILENAME
if [ -z "$OUTPUT_FILENAME" ]; then
    echo "\$OUTPUT_FILENAME not specified" >&2
    exit 100
fi
NUM_FILES=$NUM_FILES
if [ -z "$NUM_FILES" ]; then
    echo "\$NUM_FILES not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi

echo "Running from $PWD" >&2

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

stime=$(date '+%s')

RUN_COMBINE_VARIANTS=1
VARIANT_FILES=""
if [ "$RUN_COMBINE_VARIANTS" -eq "1" ]; then
    if [ "$NUM_FILES" -gt "1" ]; then
        # Build up VCF arguments for CombineVariants
        for i in `seq 1 $NUM_FILES`; do
            VARIANT_FILES+="-V output/1/${i}_$INPUT_FILENAME.vcf "
        done
        # Set GATK parameters
        NUM_THREADS=1
        GATK_PARAMS="-R ${REFERENCE} --suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -nt $NUM_THREADS --disable_auto_index_creation_and_locking_when_reading_rods"
        # Run CombineVariants
        CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o output/2/${OUTPUT_FILENAME}.vcf --assumeIdenticalSamples ${VARIANT_FILES}"
        echo $CMD
        $CMD
    elif [ "$NUM_FILES" -eq "1" ]; then
        # If there's only one input file, then no combination is necessary and that file becomes the VEER annotation resource VCF,
        # so just provide a link.
        cd output/2
        if [ -f ../1/1_${INPUT_FILENAME}.vcf ]; then # look for file prefixed with "1_"
            ln -s ../1/1_${INPUT_FILENAME}.vcf $OUTPUT_FILENAME.vcf
        elif [ -f ../1/${INPUT_FILENAME}.vcf ]; then # look for file with no numeric prefix
            ln -s ../1/${INPUT_FILENAME}.vcf $OUTPUT_FILENAME.vcf
        else
            echo "No output from VEER score generation found" >&2
            exit 100
        fi
        cd ../..
    fi
fi
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_generateVEERScores run time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO 1