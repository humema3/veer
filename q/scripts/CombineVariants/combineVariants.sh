#!/bin/bash
#q require $RUN_DIRECTORY $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-24
#$ -j y
#$ -l m_mem_free=30G
#$ -l h_rt=64800
#$ -N CombineVariants
#$ -binding linear:1

# Run GATK's CombineVariants to combine variants from multiple single-sample VCFs into one multi-sample VCF.
# Parallelized by chromosome, so end up with 24 VCF files containing the variants from their respective chromosomes.

OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi

### ponc environment variables
source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

stime=$(date '+%s')
VCF_LIST_FILE=input/vcf_list.txt

ID=$SGE_TASK_ID
CHROMOSOMES=( `seq 1 22` X Y )
if [ ! -z "$FAILED_TASK_IDS_FILE" ] && [ -f $FAILED_TASK_IDS_FILE ]; then # if this variable is filled in or supplied via q, only use the task ids contained therein
    FAILED_TASK_ID=`sed "${ID}q;d" $FAILED_TASK_IDS_FILE`
    CHR=${CHROMOSOMES[$((FAILED_TASK_ID-1))]}
else
    CHR=${CHROMOSOMES[$((ID-1))]}
fi
# Map back from chromosome to number
if [ "$CHR" == "X" ]; then
    OUTPUT_ID=23
elif [ "$CHR" == "Y" ]; then
    OUTPUT_ID=24
else
    OUTPUT_ID=$CHR
fi

###############################################################
### Make VCF list argument ##
# fd 10 replaced with a file supplied as a first argument
exec 10<"${VCF_LIST_FILE}"
VARIANT_FILES=""
read -u 10 LINE # skip header line
while read -u 10 LINE; do
    VCF=`echo ${LINE} | awk '{print $1}'`
    VARIANT_FILES+=" -V ${VCF}"
done

GATK_PARAMS="--unsafe LENIENT_VCF_PROCESSING -L ${CHR} -R ${REFERENCE} --disable_auto_index_creation_and_locking_when_reading_rods"
CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o output/${OUTPUT_ID}_${OUTPUT_SUFFIX}.vcf ${VARIANT_FILES} --suppressCommandLineHeader"

echo $CMD
$CMD

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total CombineVariants Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO