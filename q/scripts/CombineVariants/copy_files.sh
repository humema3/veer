#!/bin/bash
#q require $RUN_DIRECTORY $PREV_STEP
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_CombineVariants
#$ -l h_rt=3600
#$ -l m_mem_free=2G

# Copy and prep files for the CombineVariants step.
# Currently just link to the updated VCF list file from the CheckVariantReferenceAlleles step.

PREV_STEP=$PREV_STEP
if [ -z "$PREV_STEP" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not specified" >&2
    exit 100
fi
if [ ! -d "../../$PREV_STEP" ]; then
    echo "\$PREV_STEP (../../$PREV_STEP) is not a directory" >&2
    exit 100
fi

mkdir -p input
cd input
LATEST_RUN=`ls -t ../../../$PREV_STEP | head -n 1`
rm vcf_list.txt # remove if exists
ln -s ../../../$PREV_STEP/$LATEST_RUN/output/new_vcf_list.txt vcf_list.txt