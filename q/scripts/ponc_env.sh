#!/bin/sh

## Annotations
## The indexed FASTA file for the genome build. See http://www.broadinstitute.org/gsa/wiki/index.php/Input_files_for_the_GATK#Reference_sequence
## FASTA must also be indexed for BWA. $ bwa index
REFERENCE=/da/onc/sequencing/genomes/broad/Homo_sapiens_assembly19.fasta
## VCF file for dbSNP

## TODO: Probaly don't really need to set anything more. Just Pull everything out of ponc.properties
DBSNP_VCF=/da/onc/sequencing/annotation/dbSNP137.vcf
## VCF file for NHLBI Exom sequencing project
NHLBI_VCF=/da/onc/sequencing/annotation/ESP6500SI.snps_indels/ESP6500SI.all.left.snps_indels.vcf

DBNSFP_TABIX=/da/onc/sequencing/annotation/dbNSFP/dbNSFP2.4.txt.gz

COSMIC_VCF=/da/onc/sequencing/annotation/CosmicCompleteExport_v63_300113LeftAligned.vcf
CLD_TO_MODEL=/da/onc/sequencing/annotation/CLD_to_Model_IDs_Aug2011.txt

## Tools   HCHCHC
PONC_TOOLS=${PONC_HOME}/bin/PoncTools
GATK=/usr/prog/gatk/3.6-0/GenomeAnalysisTK.jar
## Tested with snpEff_2_0_5d
EFF_DIR=/usr/prog/onc/seqtools/snpEff/4.1b
EFF_DB=NG00009.0_RefSeq_h
## Tested with picard-tools-1.59
PICARD_TOOLS_DIR=/usr/prog/picard-tools/1.113/
SAMTOOLS=/usr/prog/samtools/0.1.19-goolf-1.5.14-NX/bin/samtools
## tested with bwa Version: 0.5.9-r16
BWA=/usr/prog/bwa/0.7.8-goolf-1.5.14-NX/bin/bwa
SCRIPTS_DIR=/usr/prog/onc/seqtools/scripts
## Require Python-2.6.6 or later
PYTHON_PATH=/usr/prog/onc/python/Anaconda3-4.1.1
PYTHON=$PYTHON_PATH/bin/python
PATH=$PATH:$PYTHONPATH
PYTHON_SCRIPTS_DIR=$SCRIPTS_DIR/python

## Require java version 1.6 or later
PONC_JAVA_HOME=/usr/prog/java/1.8.0_20
JAVA="${PONC_JAVA_HOME}/bin/java -XX:ParallelGCThreads=1"

FASTQ=/usr/prog/fastqc/0.10.1/fastqc

FASTQ=/usr/prog/fastqc/0.10.1/fastqc

PARSE_VCF=/usr/prog/onc/parsevcf/parsevcf-1.1.6/bin/parsevcf
PONCOTATOR=/home/humema3/poncotator-2.1.0-SNAPSHOT/bin/poncotator

BWA=/usr/prog/bwa/0.7.8-goolf-1.5.14-NX/bin/bwa

## export PYTHONPATH=/home/kornjo1/Python-2.6.6/my-pkgs/lib/python
