#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY $NUM_CONTIGS
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RecallVariants_fromLustre_2
#$ -l h_rt=14399
#$ -l m_mem_free=2G
#$ -l lustre=1
#$ -t 1-$NUM_CONTIGS

# After running finish_RecallVariants, copy output files back to analysis directory in normal file system.

ORIGINAL_RUN_DIRECTORY=$ORIGINAL_RUN_DIRECTORY
if [ -z "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not specified" >&2
    exit 100
fi
echo "Running from $RUN_DIRECTORY" >&2
ID=$SGE_TASK_ID

mkdir -p output/2

CMD="cp $ORIGINAL_RUN_DIRECTORY/output/2/${ID}_*  -t output/2"
echo $CMD >&2
$CMD
exit 0
