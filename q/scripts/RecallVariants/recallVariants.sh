#!/bin/bash
#q require $RUN_DIRECTORY $NUM_FILES_FOR_RECALL $MEMORY $NUM_CONTIGS $MAX_CONCURRENT_TASKS $INDEL_ALLELES_FILENAME $SNV_ALLELES_FILENAME $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_FILES_FOR_RECALL
#$ -l m_mem_free=$MEMORY
#$ -l h_rt=345599
#$ -pe smp 1
#$ -j y
#$ -N RecallVariants
#$ -binding linear:1
#$ -tc $MAX_CONCURRENT_TASKS

# Deprecated. Use recallVariants_lustre.sh.

NUM_FILES_FOR_RECALL=$NUM_FILES_FOR_RECALL
if [ -z "$NUM_FILES_FOR_RECALL" ]; then
    echo "\$NUM_FILES not specified" >> $LOG_FILE
    exit 100
fi
MEMORY=$MEMORY
if [ -z "$MEMORY" ]; then
    echo "\$MEMORY not specified" >&2
    exit 100
fi
NUM_CONTIGS=$NUM_CONTIGS
if [ -z "$NUM_CONTIGS" ]; then
    echo "\$NUM_CONTIGS not specified" >&2
    exit 100
fi
MAX_CONCURRENT_TASKS=$MAX_CONCURRENT_TASKS
if [ -z "$MAX_CONCURRENT_TASKS" ]; then
    echo "\$MAX_CONCURRENT_TASKS not specified" >&2
    exit 100
fi
INDEL_ALLELES_FILENAME=$INDEL_ALLELES_FILENAME
if [ -z "$INDEL_ALLELES_FILENAME" ]; then
    echo "\$INDEL_ALLELES_FILENAME not specified" >&2
    exit 100
fi
SNV_ALLELES_FILENAME=$SNV_ALLELES_FILENAME
if [ -z "$SNV_ALLELES_FILENAME" ]; then
    echo "\$SNV_ALLELES_FILENAME not specified" >&2
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi

echo "Running from $RUN_DIRECTORY" >&2

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

function ceil {
    local n=$1
    local PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python
    echo `$PYTHON -c "from math import ceil; print(ceil($n))"`
}

stime=$(date '+%s')

ID=$SGE_TASK_ID
ALLELES_DIR=input/1/alleles
SMF_BAM_LIST_FILE=input/1/smf_bam_list.txt
JAVA_MEMORY=`echo "$MEMORY" | tr [A-Z] [a-z]`
JAVA="${JAVA} -Xmx${JAVA_MEMORY}"
SAMPLE_NUM=$(ceil `echo "$ID/2" | bc -l`)
if [ "$(expr $ID % 2)" -eq "0" ]; then
    VARIANT_TYPE=SNP
else
    VARIANT_TYPE=INDEL
fi

# check that there are enough samples listed in the file
NUM_SAMPLE_LINES=$((`wc -l $SMF_BAM_LIST_FILE | awk '{print $1}'` - 1))
NUM_SAMPLES=`echo "$NUM_FILES_FOR_RECALL / 2" | bc`
if [ "$NUM_SAMPLE_LINES" -lt "$NUM_SAMPLES" ]; then
    echo "More samples requested than are present in the file"
    exit 1
fi
# warn if not all samples are used
if [ "$NUM_SAMPLE_LINES" -gt "$NUM_SAMPLES" ]; then
    echo "********** WARNING: Only the first $NUM_SAMPLES out of the total $NUM_SAMPLE_LINES samples from the file $SMF_BAM_LIST_FILE are being used **********"
fi

LINE=`sed "$((SAMPLE_NUM+1))q;d" $SMF_BAM_LIST_FILE`
SMF=`echo $LINE | awk '{print $1}'`
BAM_FILE=`echo $LINE | awk '{print $2}'`
BAM_INDEX=${BAM_FILE}.bai
TEMP_DIR=/scratch/`whoami`/tmpdir.${JOB_ID}.${ID}
mkdir -p $TEMP_DIR
cp $BAM_FILE -t $TEMP_DIR
checkForError $LINENO $?
cp $BAM_INDEX -t $TEMP_DIR
checkForError $LINENO $?
BAM_FILE=${TEMP_DIR}/`basename $BAM_FILE`
BAM_INDEX=${TEMP_DIR}/`basename $BAM_INDEX`

NUM_CPU_THREADS_PER_DATA_THREAD=1
NUM_DATA_THREADS=1
GATK_PARAMS="-nct $NUM_CPU_THREADS_PER_DATA_THREAD -nt $NUM_DATA_THREADS -R $REFERENCE"

for i in `seq 1 $NUM_CONTIGS`; do
    if [ "$NUM_CONTIGS" -eq "1" ] && [ ! -f ${ALLELES_DIR}/1_${INDEL_ALLELES_FILENAME}.vcf ] && [ -f ${ALLELES_DIR}/${INDEL_ALLELES_FILENAME}.vcf ] \
            && [ ! -f ${ALLELES_DIR}/1_${SNV_ALLELES_FILENAME}.vcf ] && [ -f ${ALLELES_DIR}/${SNV_ALLELES_FILENAME}.vcf ]; then
        ALLELES_FILE=${ALLELES_DIR}/
    else
        ALLELES_FILE=${ALLELES_DIR}/${i}_
    fi
    if [ "$VARIANT_TYPE" == "INDEL" ]; then
        ALLELES_FILE+=$INDEL_ALLELES_FILENAME
    else
        ALLELES_FILE+=$SNV_ALLELES_FILENAME
    fi
    #cp $ALLELES_FILE -t $TEMP_DIR
    ln -s `pwd`/$ALLELES_FILE $TEMP_DIR/`basename $ALLELES_FILE`
    checkForError $LINENO $?
    ALLELES_FILE=${TEMP_DIR}/`basename $ALLELES_FILE`
    OUTPUT_FILE=${TEMP_DIR}/${i}_${OUTPUT_SUFFIX}_${SMF}.vcf
    CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T UnifiedGenotyper -gt_mode GENOTYPE_GIVEN_ALLELES \
        -L $ALLELES_FILE --alleles $ALLELES_FILE -glm $VARIANT_TYPE \
        -o $OUTPUT_FILE \
        -out_mode EMIT_ALL_SITES \
        -I $BAM_FILE --filter_reads_with_N_cigar"
    echo ${CMD}
    $CMD
    checkForError $LINENO $?

    mv $OUTPUT_FILE -t output/1 
    checkForError $LINENO $?
    mv ${OUTPUT_FILE}.idx -t output/1
    checkForError $LINENO $?
    rm $ALLELES_FILE
    checkForError $LINENO $?
    rm ${ALLELES_FILE}.idx
    checkForError $LINENO $?
done

rm -r $TEMP_DIR
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total RecallVariants Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO $NUM_SAMPLES
