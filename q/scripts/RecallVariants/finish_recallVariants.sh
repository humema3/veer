#!/bin/bash
#q require $RUN_DIRECTORY $NUM_TASKS $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_TASKS
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=345599
#$ -binding linear:1
#$ -pe smp 1
#$ -N Finish_RecallVariants

# Deprecated. Use finish_recallVariants_lustre.sh.

NUM_TASKS=$NUM_TASKS
if [ -z "$NUM_TASKS" ]; then
    echo "\$NUM_TASKS not specified" >&2
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE ($SCRIPT_BASE) is not a directory" >&2
    exit 100
fi

echo "Running from $RUN_DIRECTORY" >&2

##############################################################################
# After running RecallVariants we combine all the results from individual
# samples (for each chromosome or other genome division) into one VCF.
##############################################################################

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

cleanup ()
{
  LINE_NUM=$1
  EXIT_STATUS=$2
  TEMP_DIR=$3
  if [ "$EXIT_STATUS" -ne "0" ] && [ ! -z "$TEMP_DIR" ] && [ -d "$TEMP_DIR" ]; then
    rm -r $TEMP_DIR
  fi
  checkForError $LINE_NUM $EXIT_STATUS
}

### Hack for forcing Java to use only 1 core
export LD_LIBRARY_PATH=/cm/shared/apps/nibri/sciComp/libc_for_java/:$LD_LIBRARY_PATH
### _SC_NPROCESSORS_ONLN is a variable you should set ( if not defined, java will reserve all available cores)
### You must set it to amount of cores you reserve from the scheduler
export _SC_NPROCESSORS_ONLN=1

stime=$(date '+%s')

FAILED_TASK_IDS_FILE=$FAILED_TASK_IDS_FILE
if [ ! -z "$FAILED_TASK_IDS_FILE" ] && [ -f $FAILED_TASK_IDS_FILE ]; then
    NUM_FAILED_TASKS=`wc -l $FAILED_TASK_IDS_FILE | awk '{print $1}'`
    if [ "$NUM_FAILED_TASKS" -lt "$SGE_TASK_ID" ]; then
        echo "Failed task listed at position $SGE_TASK_ID requested; but only $NUM_FAILED_TASKS are listed in the file"
        exit 1
    fi
    ID=`sed "${SGE_TASK_ID}q;d" $FAILED_TASK_IDS_FILE`
else
    ID=$SGE_TASK_ID
fi
VARIANT_FILES=""
TEMP_DIR=/scratch/`whoami`/tmpdir.$JOB_ID.$SGE_TASK_ID
mkdir -p $TEMP_DIR
for file in $(ls output/1/${ID}_${OUTPUT_SUFFIX}_*-*_*.vcf*); do
    if [ -s $file ]; then
        CMD="cp $file -t $TEMP_DIR"
        echo $CMD
        $CMD
        if [[ $file =~ .+\.vcf$ ]]; then
            VARIANT_FILES+="-V $TEMP_DIR/`basename $file` "
        fi
    else
        echo "$file is empty or nonexistent"
        echo "Make sure all files are present, then rerun finish_recallVariants.sh"
        rm -r $TEMP_DIR
        exit 1
    fi
done

NUM_THREADS=1
GATK_PARAMS="--suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -R ${REFERENCE} -nt $NUM_THREADS --disable_auto_index_creation_and_locking_when_reading_rods"

OUTPUT_FILE=$TEMP_DIR/${ID}_${OUTPUT_SUFFIX}.vcf
CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o $OUTPUT_FILE ${VARIANT_FILES} --genotypemergeoption UNSORTED"
echo $CMD
$CMD
cleanup $LINENO $? $TEMP_DIR

cp $OUTPUT_FILE -t output/2
cleanup $LINENO $? $TEMP_DIR

rm -r $TEMP_DIR
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_recallVariants run time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO $NUM_TASKS