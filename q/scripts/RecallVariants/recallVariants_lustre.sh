#!/bin/bash
#q require $RUN_DIRECTORY $NUM_FILES_FOR_RECALL $MEMORY $NUM_CONTIGS $MAX_CONCURRENT_TASKS $INDEL_ALLELES_FILENAME $SNV_ALLELES_FILENAME $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -l lustre=1
#$ -t 1-$NUM_FILES_FOR_RECALL
#$ -l m_mem_free=$MEMORY
#$ -l h_rt=345599
#$ -j y
#$ -N RecallVariants
#$ -pe smp 1
#$ -binding linear:1

# Run GATK's UnifiedGenotyper to recall all of the variants.

LOG_FILE=logs/$JOB_ID

# fill in if not using q or some other templating process
NUM_FILES_FOR_RECALL=$NUM_FILES_FOR_RECALL
if [ -z "$NUM_FILES_FOR_RECALL" ]; then
    echo "\$NUM_FILES not specified" >> $LOG_FILE
    exit 100
fi
MEMORY=$MEMORY
if [ -z "$MEMORY" ]; then
    echo "\$MEMORY not specified" >> $LOG_FILE
    exit 100
fi
NUM_CONTIGS=$NUM_CONTIGS
if [ -z "$NUM_CONTIGS" ]; then
    echo "\$NUM_CONTIGS not specified" >> $LOG_FILE
    exit 100
fi
MAX_CONCURRENT_TASKS=$MAX_CONCURRENT_TASKS
if [ -z "$MAX_CONCURRENT_TASKS" ]; then
    echo "\$MAX_CONCURRENT_TASKS not specified" >> $LOG_FILE
    exit 100
fi
INDEL_ALLELES_FILENAME=$INDEL_ALLELES_FILENAME
if [ -z "$INDEL_ALLELES_FILENAME" ]; then
    echo "\$INDEL_ALLELES_FILENAME not specified" >> $LOG_FILE
    exit 100
fi
SNV_ALLELES_FILENAME=$SNV_ALLELES_FILENAME
if [ -z "$SNV_ALLELES_FILENAME" ]; then
    echo "\$SNV_ALLELES_FILENAME not specified" >> $LOG_FILE
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >> $LOG_FILE
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >> $LOG_FILE
    exit 100
fi

echo "Running from $PWD" >> $LOG_FILE

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

function ceil {
    local n=$1
    local PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python
    echo `$PYTHON -c "from math import ceil; print(ceil($n))"`
}

### Hack for forcing Java to use only 1 core
export LD_LIBRARY_PATH=/cm/shared/apps/nibri/sciComp/libc_for_java/:$LD_LIBRARY_PATH
### _SC_NPROCESSORS_ONLN is a variable you should set ( if not defined, java will reserve all available cores)
### You must set it to amount of cores you reserve from the scheduler
export _SC_NPROCESSORS_ONLN=1

cleanup ()
{
  # If exit status is bad, remove temporary directory and throw an error
  LINE_NUM=$1
  EXIT_STATUS=$2
  TEMP_DIR=$3
  if [ "$EXIT_STATUS" -ne "0" ] && [ ! -z "$TEMP_DIR" ] && [ -d "$TEMP_DIR" ]; then
    rm -r $TEMP_DIR
  fi
  checkForError $LINE_NUM $EXIT_STATUS
}

stime=$(date '+%s')

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Entering the workflow >> $LOG_FILE
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Hostname is `hostname` >> $LOG_FILE

# If you fill in this variable here manually or using q, it will only use the task ids listed in this file
# Note that you have to adjust the -t flag to match the length of the file.
FAILED_TASK_ID_FILE=$FAILED_TASK_ID_FILE
if [ ! -z "$FAILED_TASK_ID_FILE" ] && [ -f $FAILED_TASK_ID_FILE ]; then
	ID=`sed "${SGE_TASK_ID}q;d" $FAILED_TASK_ID_FILE`
else
	ID=$SGE_TASK_ID
fi

# task id = 1 --> sample number 1, SNVs
# task id = 2 --> sample number 1, indels
# task id = 3 --> sample number 2, SNVs
# task id = 4 --> sample number 2, indels
# etc.
SAMPLE_NUM=$(ceil `echo "$ID/2" | bc -l`)
if [ "$(expr $ID % 2)" -eq "0" ]; then
    VARIANT_TYPE=SNP
    ALLELES_FILENAME=combineVariants_noIndels_noFilterCalls
else
    VARIANT_TYPE=INDEL
    ALLELES_FILENAME=combineVariants_indelsOnly_noFilterCalls
fi

# check that there are enough samples listed in the SMF id / BAM file URL file
SMF_BAM_LIST_FILE=smf_bam_list.txt
NUM_SAMPLE_LINES=$((`wc -l $SMF_BAM_LIST_FILE | awk '{print $1}'` - 1))
NUM_SAMPLES=`echo "$NUM_FILES_FOR_RECALL / 2" | bc`
if [ "$NUM_SAMPLE_LINES" -lt "$NUM_SAMPLES" ]; then
    echo "More samples requested than are present in the file" >> $LOG_FILE
    exit 1
fi
# warn if not all samples are used
if [ "$NUM_SAMPLE_LINES" -gt "$NUM_SAMPLES" ]; then
    echo "********** WARNING: Only the first $NUM_SAMPLES out of the total $NUM_SAMPLE_LINES samples from the file $SMF_BAM_LIST_FILE are being used **********" >> $LOG_FILE
fi

# Create output directory
OUTPUT_DIR=output/1
mkdir -p $OUTPUT_DIR
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Output dir is $OUTPUT_DIR >> $LOG_FILE

# Select the appropriate BAM file for the sample
LINE=`sed "$((SAMPLE_NUM+1))q;d" $SMF_BAM_LIST_FILE`
SMF=`echo $LINE | awk '{print $1}'`
BAM_FILE=`echo $LINE | awk '{print $2}'`
BAM_FILE=bams/`basename $BAM_FILE`
BAM_INDEX=${BAM_FILE}.bai

### Setting temp dir. Temp dir is separate for every JOB_ID and SGE_TASK_ID, to prevent overwhelming lustre.
TEMP_DIR=temp/${JOB_ID}/tmpdir.${JOB_ID}.${ID}
CMD="mkdir -p $TEMP_DIR"
echo $CMD >&2
$CMD
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] TEMP DIR is: $TEMP_DIR >> $LOG_FILE
CMD="cp $BAM_FILE -t $TEMP_DIR"
echo $CMD >&2
$CMD
cleanup $LINENO $? $TEMP_DIR
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input BAM $BAM_FILE is copied to $TEMP_DIR >> $LOG_FILE
CMD="cp $BAM_INDEX -t $TEMP_DIR"
echo $CMD >&2
$CMD
cleanup $LINENO $? $TEMP_DIR
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input BAM index $BAM_INDEX is copied to $TEMP_DIR >> $LOG_FILE
BAM_FILE=${TEMP_DIR}/`basename $BAM_FILE`
BAM_INDEX=${TEMP_DIR}/`basename $BAM_INDEX`

# Set GATK parameters
NUM_CPU_THREADS_PER_DATA_THREAD=1
NUM_DATA_THREADS=1
GATK_PARAMS="-nct $NUM_CPU_THREADS_PER_DATA_THREAD -nt $NUM_DATA_THREADS -R $REFERENCE"
GENOTYPE_LIKELIHOOD_MODEL=$VARIANT_TYPE
ALLELES_DIR=vcfs
JAVA_MEMORY=`echo "$MEMORY" | tr [A-Z] [a-z]`
JAVA="${JAVA} -Xmx${JAVA_MEMORY}"

# Iterate by chromosome
for i in `seq 1 $NUM_CONTIGS`; do
    # Take the appropriate VCF and submit as the "alleles" argument
    # If there's only one VCF and it isn't prefixed with "1_", then use that, else just the appropriately prefixed file
    if [ "$VARIANT_TYPE" == "INDEL" ]; then
        if [ "$NUM_CONTIGS" -eq "1" ] && [ ! -f ${ALLELES_DIR}/1_${INDEL_ALLELES_FILENAME}.vcf ] && [ -f ${ALLELES_DIR}/${INDEL_ALLELES_FILENAME}.vcf ]; then
            ALLELES_FILE="${ALLELES_DIR}/"
        else
            ALLELES_FILE="${ALLELES_DIR}/${i}_"
        fi
        ALLELES_FILE+=$INDEL_ALLELES_FILENAME
    else
        if [ "$NUM_CONTIGS" -eq "1" ] && [ ! -f ${ALLELES_DIR}/1_${SNV_ALLELES_FILENAME}.vcf ] && [ -f ${ALLELES_DIR}/${SNV_ALLELES_FILENAME}.vcf ]; then
            ALLELES_FILE="${ALLELES_DIR}/"
        else
            ALLELES_FILE="${ALLELES_DIR}/${i}_"
        fi
        ALLELES_FILE+=$SNV_ALLELES_FILENAME
    fi
    ALLELES_FILE+=".vcf"

    # create a symlink for the VCF in the temp directory
    CMD="ln -s `pwd`/$ALLELES_FILE $TEMP_DIR/`basename $ALLELES_FILE`"
    echo $CMD >&2
    $CMD
    cleanup $LINENO $? $TEMP_DIR
    ALLELES_FILE=${TEMP_DIR}/`basename $ALLELES_FILE`

    # define output file
    OUTPUT_FILE=${TEMP_DIR}/${i}_${OUTPUT_SUFFIX}_${SMF}_${VARIANT_TYPE}.vcf

    # Run program
    CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T UnifiedGenotyper -gt_mode GENOTYPE_GIVEN_ALLELES \
        -L $ALLELES_FILE --alleles $ALLELES_FILE -glm $GENOTYPE_LIKELIHOOD_MODEL \
        -o $OUTPUT_FILE \
        -out_mode EMIT_ALL_SITES \
        -I $BAM_FILE --filter_reads_with_N_cigar"
    echo $CMD >&2
    echo ${CMD} >> $LOG_FILE
    $CMD &> /dev/null
    # $CMD >> $LOG_FILE 2>> $LOG_FILE # for debugging only, otherwise too much logging output
    cleanup $LINENO $? $TEMP_DIR

    # Move output files out of temp directory and remove any input files copied there
    CMD="mv $OUTPUT_FILE -t $OUTPUT_DIR"
    echo $CMD >&2
    $CMD
    cleanup $LINENO $? $TEMP_DIR
    CMD="mv ${OUTPUT_FILE}.idx -t $OUTPUT_DIR"
    echo $CMD >&2
    $CMD
    cleanup $LINENO $? $TEMP_DIR
    CMD="rm $ALLELES_FILE"
    echo $CMD >&2
    $CMD
    cleanup $LINENO $? $TEMP_DIR
    CMD="rm ${ALLELES_FILE}.idx"
    echo $CMD >&2
    $CMD
    cleanup $LINENO $? $TEMP_DIR
done

CMD="rm -r $TEMP_DIR"
echo $CMD >&2
$CMD
checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total RecallVariants Action Run Time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO $NUM_SAMPLES
