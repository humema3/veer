#!/bin/bash
#q require $RUN_DIRECTORY
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RemoveFilterCalls
#$ -l h_rt=3600
#$ -l m_mem_free=2G

# Deprecated. Use copy_file_to_lustre.sh and coyp_bam_files_to_lustre.sh.

mkdir -p input/1/alleles
cd input/1/alleles
LATEST_RUN=`ls -t ../../../../../4_RemoveFilterCalls | head -n 1`
rm *.vcf*
for vcf in ../../../../../4_RemoveFilterCalls/$LATEST_RUN/output/*.vcf*; do
    ln -s $vcf
done
cd ..
LATEST_RUN=`ls -t ../../../../1_CheckVariantReferenceAlleles| head -n 1`
cat /../../../../1_CheckVariantReferenceAlleles/$LATEST_RUN/input/MutSampleDetails.txt | awk 'BEGIN{FS="\t"; OFS="\t"} {print $7, $3}' > smf_bam_list.txt