#!/bin/bash
#q require $RUN_DIRECTORY $NUM_CONTIGS $SCRIPT_BASE $OUTPUT_SUFFIX
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_CONTIGS
#$ -j y
#$ -l m_mem_free=10G
#$ -l h_rt=172800
#$ -binding linear:1
#$ -pe smp 1
#$ -l lustre=1
#$ -N Finish_RecallVariants

# After running RecallVariants, we use GATK's CombineVariants to combine all
# the results from individual samples into one VCF for each chromosome
# (or other genome division).

NUM_CONTIGS=$NUM_CONTIGS
if [ -z "$NUM_CONTIGS" ]; then
    echo "\$NUM_CONTIGS not specified" >&2
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" > $LOG_FILE
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE ($SCRIPT_BASE) is not a directory" >&2
    exit 100
fi
echo "Running from $PWD" >&2

source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

### Hack for forcing Java to use only 1 core
export LD_LIBRARY_PATH=/cm/shared/apps/nibri/sciComp/libc_for_java/:$LD_LIBRARY_PATH
### _SC_NPROCESSORS_ONLN is a variable you should set ( if not defined, java will reserve all available cores)
### You must set it to amount of cores you reserve from the scheduler
export _SC_NPROCESSORS_ONLN=1

stime=$(date '+%s')

# Assemble the VCF arguments for CombineVariants for this chromosome
ID=$SGE_TASK_ID
VARIANT_FILES=""
for file in $(ls output/1/${ID}_recallVariants_*-*_*.vcf); do
    if [[ "$file" =~ .+\.vcf ]]; then
        if [[ -s $file ]]; then
            VARIANT_FILES+="-V $file "
        else
            echo "$file is empty or nonexistent"
            echo "Make sure all files are present, then rerun finish_recallVariants.sh"
            exit 1
        fi
    fi
done

# Set GATK parameters
NUM_THREADS=1
GATK_PARAMS="--suppressCommandLineHeader --unsafe LENIENT_VCF_PROCESSING -R ${REFERENCE} -nt $NUM_THREADS --disable_auto_index_creation_and_locking_when_reading_rods"

# Run CombineVariants
CMD="${JAVA} -jar ${GATK} ${GATK_PARAMS} -T CombineVariants -o output/2/${ID}_recallVariants.vcf ${VARIANT_FILES} --genotypemergeoption UNSORTED"
echo $CMD
$CMD

checkForError $LINENO $?

etime=$(date '+%s')

dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total finish_recallVariants run time: %d:%02d:%02d\n' $dh $dm $ds

success $LINENO $NUM_CONTIGS
