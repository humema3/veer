#!/bin/bash
#q require $RUN_DIRECTORY $NUM_SAMPLES $SMF_BAM_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RecallVariants_toLustre_BAM
#$ -l h_rt=10800
#$ -l m_mem_free=6G
#$ -l lustre=1
#$ -t 1-$NUM_SAMPLES
#$ -tc 200

# Copy BAM files to analysis directory on Lustre file system.
# Parallelized by sample.

SMF_BAM_LIST_FILENAME=$SMF_BAM_LIST_FILENAME
if [ -z "$SMF_BAM_LIST_FILENAME" ]; then
    echo "\$SMF_BAM_LIST_FILENAME not specified" >&2
    exit 100
fi
echo "Running from $RUN_DIRECTORY" >&2

SAMPLE_LINE_NUM=$((SGE_TASK_ID+1)) # skip the header line
BAM_FILE=`sed "${SAMPLE_LINE_NUM}q;d" $SMF_BAM_LIST_FILENAME | cut -f2` # extract the BAM filepath
cp $BAM_FILE -t bams
cp $BAM_FILE.bai -t bams