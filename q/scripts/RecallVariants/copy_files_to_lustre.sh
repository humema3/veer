#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY $PREV_STEP $FIRST_STEP $SCRIPT_BASE $NUM_CONTIGS $SMF_BAM_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RecallVariants_toLustre
#$ -l h_rt=7200
#$ -l m_mem_free=2G
#$ -l lustre=1
#$ -t 1-$NUM_CONTIGS

# Set up the appropriate directory structure and striping in the Lustre file system
# and copy the necessary input files there for running RecallVariants.
# Parallelized by chromosome.
# Files copied/linked to currently include the VCFs and the reference genome.
# The BAM files are copied separately in copy_bam_files_to_lustre.sh,
# which is parallelized by sample.

ORIGINAL_RUN_DIRECTORY=$ORIGINAL_RUN_DIRECTORY
if [ -z "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not specified" >&2
    exit 100
fi
if [ ! -d "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY ($ORIGINAL_RUN_DIRECTORY) is not a directory" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >&2
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE ($SCRIPT_BASE) is not a directory" >&2
    exit 100
fi
PREV_STEP=$PREV_STEP
if [ -z "$PREV_STEP" ]; then
    echo "\$PREV_STEP not specified" >&2
    exit 100
fi
FIRST_STEP=$FIRST_STEP
if [ -z "$FIRST_STEP" ]; then
    echo "\$FIRST_STEP not specified" >&2
    exit 100
fi
SMF_BAM_LIST_FILENAME=$SMF_BAM_LIST_FILENAME
if [ -z "$SMF_BAM_LIST_FILENAME" ]; then
    echo "\$SMF_BAM_LIST_FILENAME not specified" >&2
    exit 100
fi
echo "Running from $RUN_DIRECTORY" >&2
ID=$SGE_TASK_ID

# Set up directories
mkdir -p logs
mkdir -p output
mkdir -p temp
mkdir -p vcfs
lfs setstripe -c 4 vcfs
rm -r reference
ln -s /flock/scratch/humema3/VEER_AnnotateVCFs/CCLE_public/reference # an appropriately striped reference genome directory is already here

# VCFs
mkdir -p $ORIGINAL_RUN_DIRECTORY/input/1/vcfs
cd $(dirname `dirname $ORIGINAL_RUN_DIRECTORY`) # root analysis directory
LATEST_RUN=`ls -t $PREV_STEP | head -n 1`
for vcf in $PREV_STEP/$LATEST_RUN/output/${ID}_*.vcf*; do # VCF output files from last step
    CMD="cp $vcf -t $RUN_DIRECTORY/vcfs" # copy to Lustre
    echo $CMD >&2
    $CMD
    ln -s $vcf $ORIGINAL_RUN_DIRECTORY/input/1/vcfs/`basename $vcf` # create link on normal FS
done

if [ "$SGE_TASK_ID" -eq "1" ]; then # only do once
    # Create SMF id / BAM filepath mapping file
    LATEST_RUN=`ls -t $FIRST_STEP | head -n 1`
    cat $FIRST_STEP/$LATEST_RUN/input/MutSampleDetails.txt | awk 'BEGIN{FS="\t"; OFS="\t"} {print $7, $3}' > $RUN_DIRECTORY/$SMF_BAM_LIST_FILENAME
    ln -s $RUN_DIRECTORY/$SMF_BAM_LIST_FILENAME $ORIGINAL_RUN_DIRECTORY/input/1/$SMF_BAM_LIST_FILENAME

    # Provide link in regular file system analysis directory to reference genome
    # The $REFERENCE variable is declared in ponc_env.sh
    # The file of the same name should be present in the reference folder set up in the Lustre analysis directory
    ln -s $REFERENCE $ORIGINAL_RUN_DIRECTORY/input/1/`basename $REFERENCE`

    # Create BAM file directory
    mkdir -p bams
    lfs setstripe -c 4 bams
fi

exit 0