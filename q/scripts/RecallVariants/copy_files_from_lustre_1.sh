#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY $NUM_SAMPLES $SMF_BAM_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RecallVariants_fromLustre
#$ -l h_rt=14400
#$ -l m_mem_free=2G
#$ -l lustre=1
#$ -t 1-$NUM_SAMPLES

# After running RecallVariants on Lustre, copy output files back to analysis directory in normal file system.

ORIGINAL_RUN_DIRECTORY=$ORIGINAL_RUN_DIRECTORY
if [ -z "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not specified" >&2
    exit 100
fi
SMF_BAM_LIST_FILENAME=$SMF_BAM_LIST_FILENAME
if [ -z "$SMF_BAM_LIST_FILENAME" ]; then
    echo "\$SMF_BAM_LIST_FILENAME not specified" >&2
    exit 100
fi
echo "Running from $RUN_DIRECTORY" >&2
ID=$SGE_TASK_ID

mkdir -p output/1

SMF_ID=`sed "$((ID+1))q;d" $ORIGINAL_RUN_DIRECTORY/$SMF_BAM_LIST_FILENAME | cut -f1`
CMD="cp $ORIGINAL_RUN_DIRECTORY/output/1/*${SMF_ID}* -t output/1"
echo $CMD >&2
$CMD
exit 0