#!/bin/sh

if [ ! -e RUN_RESULT ]; then
    lockfile ponc.lock
	echo -e "JOB\tSTATUS\tLINE\tRATIO_DONE" > RUN_RESULT
	rm -f ponc.lock
fi


lockfile ponc.lock
echo -e "$JOB_NAME.o$JOB_ID.$SGE_TASK_ID\tSTARTED\t0" >> RUN_RESULT
rm -f ponc.lock

function checkForError() {
    EXIT_STATUS_ON_CALL=$?
    if [ "$#" -gt "1" ]; then
        EXIT_STATUS=$2
    else
        EXIT_STATUS=$EXIT_STATUS_ON_CALL
    fi
    if [ "$EXIT_STATUS" -ne "0" ]; then
        SCRIPT_LINE_NUM=$1
        lockfile ponc.lock
        echo "FAIL! Non zero status code! Exiting"
        echo -e "$JOB_NAME.o$JOB_ID.$SGE_TASK_ID\tFAIL\t$SCRIPT_LINE_NUM" >> RUN_RESULT
        rm -f ponc.lock
        exit $EXIT_STATUS
    fi

    if [ $JOB_NAME ]; then
        SCRIPT_OUT="$JOB_NAME.o$JOB_ID.$SGE_TASK_ID"
        EXCEPTION_COUNT=`grep -v ConnectException ${SCRIPT_OUT} | grep -c -e Exception -e "##### ERROR"`

        if [ $EXCEPTION_COUNT != "0" ]; then
            lockfile ponc.lock
            echo "FAIL! Exception detected! Exiting"
            echo -e "$JOB_NAME.o$JOB_ID.$SGE_TASK_ID\tFAIL\t$SCRIPT_LINE_NUM" >> RUN_RESULT
            rm -f ponc.lock
            exit 1
        fi
    fi
}

## Check to see if the currently running task is the last task of the run
function taskCount() {
    NUM_TASKS=`qstat -j $JOB_ID | grep -c usage`
    echo "$NUM_TASKS"
}

function success() {
     lockfile ponc.lock
     SCRIPT_LINE_NUM=$1
     NUM_TASKS=$2
     SUCCESS_COUNT=`grep -c SUCCESS RUN_RESULT`
     FAIL_COUNT=`grep -c FAIL RUN_RESULT`
     RATIO=$((SUCCESS_COUNT+$FAIL_COUNT+1))/$NUM_TASKS
     echo -e "$JOB_NAME.o$JOB_ID.$SGE_TASK_ID\tSUCCESS\t$SCRIPT_LINE_NUM\t$RATIO" >> RUN_RESULT
     rm -f ponc.lock
}
