#!/bin/bash
#q require $RUN_DIRECTORY $NUM_SAMPLES $ORIGINAL_VCF_LIST_FILENAME $NEW_VCF_LIST_FILENAME $REPORT_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N CheckVariantReferenceAlleles
#$ -l h_rt=14399
#$ -l m_mem_free=32G
#$ -j y
#$ -t 1-$NUM_SAMPLES
#$ -pe smp 1
#$ -binding linear:1

#####################################################################################################
# Checks reference alleles listed in input VCFs against the reference genome.                       #
# If discrepancies are found, a new VCF file is generated in the output directory for this step.    #
# Each task checks one sample's VCF.                                                                #
# Generates a one-line file listing the location of the VCF to be used henceforth                   #
# (either the old filepath or the new one if there was any change)                                  #
# Also generates a report file listing any found discrepancies.                                     #
#####################################################################################################

ORIGINAL_VCF_LIST_FILENAME=$ORIGINAL_VCF_LIST_FILENAME
if [ -z "$ORIGINAL_VCF_LIST_FILENAME" ]; then
    echo "\$ORIGINAL_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi
NEW_VCF_LIST_FILENAME=$NEW_VCF_LIST_FILENAME
if [ -z "$NEW_VCF_LIST_FILENAME" ]; then
    echo "\$NEW_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi
REPORT_FILENAME=$REPORT_FILENAME
if [ -z "$REPORT_FILENAME" ]; then
    echo "\$REPORT_FILENAME not specified" >&2
    exit 100
fi
echo "Running from $PWD" >&2

ID=$SGE_TASK_ID

### Hack for forcing Java to use only 1 core
export LD_LIBRARY_PATH=/cm/shared/apps/nibri/sciComp/libc_for_java/:$LD_LIBRARY_PATH
### _SC_NPROCESSORS_ONLN is a variable you should set (if not defined, java will reserve all available cores)
### You must set it to amount of cores you reserve from the scheduler
export _SC_NPROCESSORS_ONLN=1

SAMPLE_DETAILS_FILE=input/MutSampleDetails.txt
REF_GENOME=input/Homo_sapiens_assembly19.fasta
OLD_VCF_LIST_FILE=output/$ORIGINAL_VCF_LIST_FILENAME
sed "${ID}q;d" $OLD_VCF_LIST_FILE > $OLD_VCF_LIST_FILE.$ID # give this task's original VCF its own one-line "list" file
NEW_VCF_LIST_FILE=output/$NEW_VCF_LIST_FILENAME.$ID # output updated VCF location to its own one-line "list" file
MODIFIED_VCF_DIR=output/vcfs
mkdir -p $MODIFIED_VCF_DIR
CHECK_REPORT_FILE=output/$REPORT_FILENAME.$ID # print any discrepancies in this VCF to its own file

CMD="$PONC_HOME/bin/PoncTools -type CheckVariantReferenceAlleles -v $OLD_VCF_LIST_FILE.$ID -ref $REF_GENOME -edit -copylist $NEW_VCF_LIST_FILE -copydir $MODIFIED_VCF_DIR -report $CHECK_REPORT_FILE"
echo $CMD
$CMD
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -eq "0" ]; then
    rm $OLD_VCF_LIST_FILE.$ID
else
    echo "Call to CheckVariantReferenceAlleles program failed" >&2
    exit $EXIT_STATUS
fi