#!/bin/bash
#q require $RUN_DIRECTORY $NUM_SAMPLES $NEW_VCF_LIST_FILENAME $REPORT_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N finish_CheckVariantReferenceAlleles
#$ -l h_rt=14399
#$ -l m_mem_free=8G
#$ -j y

# After running CheckVariantReferenceAlleles, concatenate the parallel output files together.

NUM_SAMPLES=$NUM_SAMPLES
if [ -z "$NUM_SAMPLES" ]; then
    echo "\$NUM_SAMPLES not specified" >&2
    exit 100
fi
NEW_VCF_LIST_FILENAME=$NEW_VCF_LIST_FILENAME
if [ -z "$NEW_VCF_LIST_FILENAME" ]; then
    echo "\$NEW_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi
REPORT_FILENAME=$REPORT_FILENAME
if [ -z "$REPORT_FILENAME" ]; then
    echo "\$REPORT_FILENAME not specified" >&2
    exit 100
fi

echo "Running from $RUN_DIRECTORY" >&2

NEW_VCF_LIST_FILE=output/$NEW_VCF_LIST_FILENAME
CHECK_REPORT_FILE=output/$REPORT_FILENAME
rm $NEW_VCF_LIST_FILE
rm $CHECK_REPORT_FILE
for i in `seq 1 $NUM_SAMPLES`; do
    cat $NEW_VCF_LIST_FILE.$i >> $NEW_VCF_LIST_FILE
    cat $CHECK_REPORT_FILE.$i >> $CHECK_REPORT_FILE
done
rm $NEW_VCF_LIST_FILE.*
rm $CHECK_REPORT_FILE.*

