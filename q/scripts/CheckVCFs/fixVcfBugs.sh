#!/bin/bash
#q require $RUN_DIRECTORY $NUM_SAMPLES $ORIGINAL_VCF_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N FixVcfBugs
#$ -t 1-$NUM_SAMPLES
#$ -l h_rt=3600
#$ -l m_mem_free=8G
#$ -j y

# Fix any bugs in the VCF files that have been found thus far.
# Each task fixes one sample's VCF.
# If any changes are made, a new, corrected VCF is generated in the input directory,
# and that VCF is substituted on the VCF list file in place of the original.

ORIGINAL_VCF_LIST_FILENAME=$ORIGINAL_VCF_LIST_FILENAME
if [ -z "$ORIGINAL_VCF_LIST_FILENAME" ]; then
    echo "\$ORIGINAL_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi

VCF_LIST_FILE=output/$ORIGINAL_VCF_LIST_FILENAME
FIXED_VCF_LIST_FILE=output/$ORIGINAL_VCF_LIST_FILENAME.fixed
SAMPLE_NUM=$SGE_TASK_ID
VCF=`sed "${SAMPLE_NUM}q;d" $VCF_LIST_FILE`

# Some of the older samples were updated and the updated versions stored in a "vcfs" subdirectory, but Josh may not have updated the sample details file to reflect this. If not, and this subdirectory exists, switch to this file.
if [ "$(basename `dirname $VCF`)" != "vcfs" ] && [ -d `dirname $VCF`/vcfs ]; then
    VCF=`dirname $VCF`/vcfs/`basename $VCF`
fi
ORIGINAL_VCF=$VCF

# stage VCF to a temp directory in /scratch to reduce I/O cost
TEMP_DIR=/scratch/`whoami`/$JOB_ID.$SGE_TASK_ID
mkdir -p $TEMP_DIR/vcfs
cp $VCF -t $TEMP_DIR/vcfs
VCF=$TEMP_DIR/vcfs/`basename $VCF`
VCF_MODIFIED=0 # flag indicating whether a fix has been made

# "##FILTER" at beginning of header lines must be all caps
if [ ! -z "`grep -m 1 \"^##Filter\" $VCF`" ]; then #
    VCF_MODIFIED=1
    perl -i -pe "s/^##Filter/##FILTER/g;" $VCF
fi

# Sometimes "Description,Description" has been where "Description" should have been
if [ ! -z "`grep -m 1 \"Description,Description\" $VCF`" ]; then
    VCF_MODIFIED=1
    perl -i -pe "s/Description,Description/Description/g;" $VCF
fi

# rewrite any comma-delimited multi-filter calls as "Combo"
for filter in `grep -v "^#" $VCF | cut -f7 | sort | uniq`; do
    if [[ "$filter" =~ .+,.+ ]]; then
        VCF_MODIFIED=1
        NEW_VCF=$VCF.new
        grep "^#" $VCF > $NEW_VCF
        grep -v "^#" $VCF | awk 'BEGIN{FS="\t"; OFS="\t"} { if ($7 ~ /.+,.+/) { $7 = "Combo" }; print }' >> $NEW_VCF
        mv $NEW_VCF $VCF
        break
    fi
done

if [ "$VCF_MODIFIED" -eq "1" ]; then # VCF has been fixed
    # move out of temp directory to input directory
    mv $VCF -t input/vcfs
    VCF=`pwd`/input/vcfs/`basename $VCF`
else
    # remove from temp directory, use original
    rm $VCF
    VCF=$ORIGINAL_VCF
fi

# Append updated VCF location (whether or not it has changed) to the fixed VCF list file
# Use lockfile to make sure one task's VCF is written out in completion before another interrupts
LOCKFILE=VEER.lock
while [ -e "$LOCKFILE" ]; do
    sleep 1
done
lockfile $LOCKFILE
echo $VCF >> $FIXED_VCF_LIST_FILE
rm -f $LOCKFILE

# Cleanup
rm -r $TEMP_DIR