#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_VCF_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N start_CheckVariantReferenceAlleles
#$ -l h_rt=3600
#$ -l m_mem_free=8G
#$ -j y

# Replace the VCF list file with the bug-fixed version.

ORIGINAL_VCF_LIST_FILENAME=$ORIGINAL_VCF_LIST_FILENAME
if [ -z "$ORIGINAL_VCF_LIST_FILENAME" ]; then
    echo "\$ORIGINAL_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi

mv output/$ORIGINAL_VCF_LIST_FILENAME.fixed output/$ORIGINAL_VCF_LIST_FILENAME