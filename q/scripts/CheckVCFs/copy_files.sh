#!/bin/bash
#q require $RUN_DIRECTORY $SAMPLE_DETAILS_FILE
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N CopyFiles_CheckVCFs
#$ -j y
#$ -l h_rt=3600
#$ -l m_mem_free=2G

### Copy and prep files for the CheckVCFs step. ###

SAMPLE_DETAILS_FILE=$SAMPLE_DETAILS_FILE
if [ -z "$SAMPLE_DETAILS_FILE" ]; then
    echo "\$SAMPLE_DETAILS_FILE not specified" >&2
    exit 100
fi
if [ ! -f "$SAMPLE_DETAILS_FILE" ]; then
    echo "\$SAMPLE_DETAILS_FILE ($SAMPLE_DETAILS_FILE) does not exist" >&2
    exit 100
fi

mkdir -p input
cd input

# Sample details file
cp $SAMPLE_DETAILS_FILE MutSampleDetails.txt
perl -i -pe "s/file:\/\/rogue//g;" MutSampleDetails.txt # Get rid of file://rogue prefix on any file URLs

# Reference genome files
# Remove any links/copies that exist first
rm Homo_sapiens_assembly19.fasta
rm Homo_sapiens_assembly19.fasta.fai
rm Homo_sapiens_assembly19.dict
ln -s /da/onc/sequencing/genomes/broad/Homo_sapiens_assembly19.fasta
ln -s /da/onc/sequencing/genomes/broad/Homo_sapiens_assembly19.fasta.fai
ln -s /da/onc/sequencing/genomes/broad/Homo_sapiens_assembly19.dict