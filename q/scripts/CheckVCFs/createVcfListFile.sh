#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_VCF_LIST_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -N CreateVcfListFile
#$ -l h_rt=14399
#$ -l m_mem_free=8G
#$ -j y

# Creates the VCF list file from the sample details file. #

ORIGINAL_VCF_LIST_FILENAME=$ORIGINAL_VCF_LIST_FILENAME
if [ -z "$ORIGINAL_VCF_LIST_FILENAME" ]; then
    echo "\$ORIGINAL_VCF_LIST_FILENAME not specified" >&2
    exit 100
fi
echo "Running from $PWD" >&2

if [ -z "$NUM_SAMPLES" ]; then
    NUM_SAMPLES=$((`wc -l $SAMPLE_DETAILS_FILE | awk '{print $1}'` - 1))
fi
SAMPLE_DETAILS_FILE=input/MutSampleDetails.txt
VCF_LIST_FILE=output/$ORIGINAL_VCF_LIST_FILENAME
tail -n $NUM_SAMPLES $SAMPLE_DETAILS_FILE | cut -f2 > $VCF_LIST_FILE