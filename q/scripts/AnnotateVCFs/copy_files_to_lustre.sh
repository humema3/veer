#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY $RESOURCE_FILENAME $SCRIPT_BASE $PREV_STEP $FIRST_STEP
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_AnnotateVCFs_toLustre
#$ -l h_rt=7200
#$ -l m_mem_free=2G
#$ -l lustre=1

########################################################################################################
# Prior to running AnnotateVCFs step on Lustre, set up the necessary directory structure and striping  #
# on Lustre file system and copy all necessary files there.                                            #
########################################################################################################

RUN_DIRECTORY=$RUN_DIRECTORY
ORIGINAL_RUN_DIRECTORY=$ORIGINAL_RUN_DIRECTORY
if [ -z "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not specified" >&2
    exit 100
fi
if [ ! -d "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY ($ORIGINAL_RUN_DIRECTORY) is not a directory" >&2
    exit 100
fi
RESOURCE_FILENAME=$RESOURCE_FILENAME
if [ -z "$RESOURCE_FILENAME" ]; then
    echo "\$RESOURCE_FILENAME not specified" >&2
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >&2
    exit 100
fi
if [ ! -d "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE ($SCRIPT_BASE) is not a directory" >&2
    exit 100
fi
PREV_STEP=$PREV_STEP
if [ -z "$PREV_STEP" ]; then
    echo "\$PREV_STEP not specified" >&2
    exit 100
fi
FIRST_STEP=$FIRST_STEP
if [ -z "$FIRST_STEP" ]; then
    echo "\$FIRST_STEP not specified" >&2
    exit 100
fi
echo "Running from $PWD" >&2

source $SCRIPT_BASE/ponc_env.sh

mkdir -p logs
mkdir -p output
mkdir -p temp
mkdir -p input
lfs setstripe -c 4 input
mkdir -p ref_vcf
lfs setstripe -c 4 -S 33554432 ref_vcf
rm -r reference
ln -s /flock/scratch/humema3/VEER_AnnotateVCFs/CCLE_public/reference

mkdir -p $ORIGINAL_RUN_DIRECTORY/input/vcfs
cd $ORIGINAL_RUN_DIRECTORY/input
LATEST_RUN=`ls -t ../../../$PREV_STEP | head -n 1`
for vcf in ../../../$PREV_STEP/$LATEST_RUN/output/2/$RESOURCE_FILENAME.vcf*; do
    cp $vcf -t $RUN_DIRECTORY/ref_vcf
    ln -s $vcf # also provide symbolic links to resource VCF and index file in the original analysis directory's input folder
done
cd vcfs
LATEST_RUN=`ls -t ../../../../$FIRST_STEP | head -n 1`
for vcf in `cat ../../../../$FIRST_STEP/$LATEST_RUN/output/new_vcf_list.txt`; do
    cp $vcf* -t $RUN_DIRECTORY/input
    ln -s $vcf
    ln -s $vcf.idx # also provide symbolic links to VCFs and index files in the original analysis directory's input folder
done
ln -s $REFERENCE $ORIGINAL_RUN_DIRECTORY/input/`basename $REFERENCE`

exit 0