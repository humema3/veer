#!/bin/bash
#q require $RUN_DIRECTORY $RESOURCE_FILENAME
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_RemoveFilterCalls
#$ -l h_rt=3600
#$ -l m_mem_free=2G

##### Deprecated, use copy_files_to_lustre.sh #####

RESOURCE_FILENAME=$RESOURCE_FILENAME
if [ -z "$RESOURCE_FILENAME" ]; then
    echo "\$RESOURCE_FILENAME not specified" >&2
    exit 100
fi

mkdir -p input
LATEST_RUN=`ls -t ../../../6_GenerateVEERScores | head -n 1`
rm *.vcf*
for vcf in ../../../6_GenerateVEERScores/$LATEST_RUN/output/2/${RESOURCE_FILENAME}.vcf*; do
    ln -s $vcf
done
LATEST_RUN=`ls -t ../../../1_CheckVariantReferenceAlleles | head -n 1`
for vcf in `cat ../../../1_CheckVariantReferenceAlleles/$LATEST_RUN/output/new_vcf_list.txt`; do
    rm `basename $vcf`
    ln -s $vcf
done