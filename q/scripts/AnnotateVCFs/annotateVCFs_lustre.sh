#!/bin/bash
#q require $RUN_DIRECTORY $NUM_SAMPLES $RESOURCE_FILENAME $OUTPUT_SUFFIX $SCRIPT_BASE
#$ -S /bin/bash
#$ -m n
#$ -wd $RUN_DIRECTORY
#$ -t 1-$NUM_SAMPLES
#$ -l m_mem_free=24G
#$ -l h_rt=86400
#$ -j y
#$ -l lustre=1
#$ -N VEER_AnnotateVCFs
#$ -binding linear:1

######################################################################
# Annotates single-sample VCF files with VEER counts and scores from a
# multi-sample VCF resource file using Poncotator.
######################################################################

LOG_FILE=logs/$JOB_ID

NUM_SAMPLES=$NUM_SAMPLES
if [ -z "$NUM_SAMPLES" ]; then
    echo "\$NUM_SAMPLES not specified" >> $LOG_FILE
    exit 100
fi
RESOURCE_FILENAME=$RESOURCE_FILENAME
if [ -z "$RESOURCE_FILENAME" ]; then
    echo "\$RESOURCE_FILENAME not specified" >> $LOG_FILE
    exit 100
fi
OUTPUT_SUFFIX=$OUTPUT_SUFFIX
if [ -z "$OUTPUT_SUFFIX" ]; then
    echo "\$OUTPUT_SUFFIX not specified" >> $LOG_FILE
    exit 100
fi
SCRIPT_BASE=$SCRIPT_BASE
if [ -z "$SCRIPT_BASE" ]; then
    echo "\$SCRIPT_BASE not specified" >> $LOG_FILE
    exit 100
fi

echo "Running from $RUN_DIRECTORY" >> $LOG_FILE

### ponc environment variables
source $SCRIPT_BASE/ponc_env.sh
source $SCRIPT_BASE/ponc_functions.sh

### Hack for forcing Java to use only 1 core
export LD_LIBRARY_PATH=/cm/shared/apps/nibri/sciComp/libc_for_java/:$LD_LIBRARY_PATH
### _SC_NPROCESSORS_ONLN is a variable you should set ( if not defined, java will reserve all available cores)
### You must set it to amount of cores you reserve from the scheduler
export _SC_NPROCESSORS_ONLN=1

### Cleanup function
cleanup ()
{
  LINE_NUM=$1
  EXIT_STATUS=$2
  TEMP_DIR=$3
  if [ "$EXIT_STATUS" -ne "0" ] && [ ! -z "$TEMP_DIR" ] && [ -d "$TEMP_DIR" ]; then
    rm -r $TEMP_DIR
  fi
  checkForError $LINE_NUM $EXIT_STATUS
}

### start time
stime=$(date '+%s')

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Entering the workflow >> $LOG_FILE
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Hostname is `hostname` >> $LOG_FILE

### Configuration
FAILED_TASK_IDS_FILE=$FAILED_TASK_IDS_FILE
if [ ! -z $FAILED_TASK_IDS_FILE ] && [ -f $FAILED_TASK_IDS_FILE ]; then
    ID=`sed "${SGE_TASK_ID}q;d" $FAILED_TASK_IDS_FILE`
else
    ID=$SGE_TASK_ID
fi
INPUT_DIR=input

OUTPUT_DIR=output
if [ ! -z $OUTPUT_DIR ]; then
    mkdir -p $OUTPUT_DIR
fi

INPUT_FILENAME=
ANNOTATE_SCORES=1
COUNTS_RESOURCE=ref_vcf/${RESOURCE_FILENAME}.vcf

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Output dir is $OUTPUT_DIR >> $LOG_FILE

### Some kind of input filename logic. Didn't touch this. 
if [ ! -z $INPUT_FILENAME ] && [ "$NUM_SAMPLES" -eq "1" ] && [ "$ID" -eq "1" ] && [ ! -f ${INPUT_DIR}/1_${INPUT_FILENAME}.vcf ] && [ -f ${INPUT_DIR}/${INPUT_FILENAME}.vcf ]; then
    INPUT_VCF=${INPUT_DIR}/${INPUT_FILENAME}.vcf
elif [ ! -z $INPUT_FILENAME ]; then
    INPUT_VCF="${INPUT_DIR}/${ID}_${INPUT_FILENAME}.vcf"
else
    INPUT_VCF=`ls $INPUT_DIR/*.vcf | head -n $ID | tail -n 1`
fi
INPUT_INDEX=${INPUT_VCF}.idx

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input VCF is $INPUT_VCF >> $LOG_FILE
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input index is $INPUT_INDEX >> $LOG_FILE

### Setting temp dir. Temp dir is separate for every JOB_ID and SGE_TASK_ID, to prevent overwhelming lustre.
TEMP_DIR=temp/${JOB_ID}/tmpdir.${JOB_ID}.${ID}
mkdir -p $TEMP_DIR

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] TEMP DIR is: $TEMP_DIR >> $LOG_FILE

### Copying input_VCF to this location, not to work directly from common data folder.
cp $INPUT_VCF -t $TEMP_DIR
cleanup $LINENO $? $TEMP_DIR

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input VCF $INPUT_VCF is copied to $TEMP_DIR >> $LOG_FILE

### Copying index file
INPUT_VCF=${TEMP_DIR}/`basename $INPUT_VCF`
if [ -f $INPUT_INDEX ]; then
  cp $INPUT_INDEX -t $TEMP_DIR
  cleanup $LINENO $? $TEMP_DIR
  echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Input index $INPUT_INDEX is copied to $TEMP_DIR >> $LOG_FILE
else
  echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Clean index doesn\'t exist >> $LOG_FILE
fi

### Setting output names
VCF_FILENAME=`basename $INPUT_VCF`
VCF_FILENAME="${VCF_FILENAME%.*}"
OUTPUT_VCF=$TEMP_DIR/${VCF_FILENAME}_${OUTPUT_SUFFIX}.vcf
OUTPUT_INDEX=${OUTPUT_VCF}.idx

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Output VCF is $OUTPUT_VCF >> $LOG_FILE
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Output Index is $OUTPUT_INDEX >> $LOG_FILE

### Running PONCOTATOR
CMD="$PONCOTATOR -o $OUTPUT_VCF -r $REFERENCE -vcf $INPUT_VCF"
if [ ! -z $COUNTS_RESOURCE ]; then
    CMD="$CMD -a Resource:VeerCountDB:VEERCounts -resource VeerCountDB:${COUNTS_RESOURCE}"
else
    CMD="$CMD -a VEERCounts"
fi
if [ "$ANNOTATE_SCORES" -eq "1" ]; then
    CMD="$CMD -a VS"
fi

echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Running CMD: $CMD >> $LOG_FILE
$CMD
cleanup $LINENO $? $TEMP_DIR
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] CMD is finished with no errors >> $LOG_FILE

### Staging output from the TEMP dir to OUTPUT
mv $OUTPUT_VCF -t $OUTPUT_DIR
cleanup $LINENO $? $TEMP_DIR
if [ -f $OUTPUT_INDEX ]; then
  mv $OUTPUT_INDEX -t $OUTPUT_DIR
  cleanup $LINENO $? $TEMP_DIR
fi
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Staging to $OUTPUT_DIR is done >> $LOG_FILE


### Final clean up
rm -r $TEMP_DIR
checkForError $LINENO $?
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Clean up is done. >> $LOG_FILE

### Logging
etime=$(date '+%s')
dt=$((etime - stime))
ds=$((dt % 60))
dm=$(((dt / 60) % 60))
dh=$((dt / 3600))
printf 'Total AnnotateVCFs action run time: %d:%02d:%02d\n' $dh $dm $ds
echo [INFO] [$JOB_ID.$SGE_TASK_ID] [`date +%s`] [`date`] [$$] Workflow is completed successfuly. >> $LOG_FILE

success $LINENO $NUM_SAMPLES
