#!/bin/bash
#q require $RUN_DIRECTORY $ORIGINAL_RUN_DIRECTORY $NUM_SAMPLES
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -j y
#$ -N CopyFiles_AnnotateVCFs_fromLustre
#$ -l h_rt=43200
#$ -l m_mem_free=6G
#$ -l lustre=1
#$ -t 1-$NUM_SAMPLES

###########################################################################################
# After running AnnotateVCFs step, copy output files back to original analysis directory. #
# Each task copies one sample's output VCF.                                               #
###########################################################################################

ORIGINAL_RUN_DIRECTORY=$ORIGINAL_RUN_DIRECTORY
if [ -z "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY not provided" >&2
    exit 100
fi
if [ ! -d "$ORIGINAL_RUN_DIRECTORY" ]; then
    echo "\$ORIGINAL_RUN_DIRECTORY ($ORIGINAL_RUN_DIRECTORY) is not a directory" >&2
    exit 100
fi
echo "Running from $PWD" >&2

FILE=`ls $ORIGINAL_RUN_DIRECTORY/output/*.vcf | sed "${SGE_TASK_ID}q;d"` # get the i'th VCF file in the output folder, where i = task id
CMD="cp $FILE* -t output"
echo $CMD >&2
$CMD
exit 0