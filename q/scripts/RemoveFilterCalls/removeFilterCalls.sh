#!/bin/bash
#q require $RUN_DIRECTORY $FILE_SUFFIX $NUM_CONTIGS
#$ -wd $RUN_DIRECTORY
#$ -m n
#$ -l h_rt=14399
#$ -l m_mem_free=2G
#$ -j y
#$ -N RemoveFilterCalls
#$ -t 1-$NUM_CONTIGS

# Remove all FILTER calls from the VCFs from the last step, replacing with "."
# Necessary to run GATK's UnifiedGenotyper downstream.
# Parallelized by chromosome.

FILE_SUFFIX=$FILE_SUFFIX
if [ -z "$FILE_SUFFIX" ]; then
    echo "\$FILE_SUFFIX not specified" >&2
    exit 100
fi

echo "Running from $RUN_DIRECTORY" >&2

ID=$SGE_TASK_ID
for FILE in input/${ID}_*.vcf; do
    FILENAME=`basename $FILE`
    FILENAME=${FILENAME%.*}
    echo "cat $FILE | awk 'BEGIN{OFS=\"\\t\"} {if ($7 != \"FILTER\") gsub(/.+/,\".\",$7); print}' > output/${FILENAME}_${FILE_SUFFIX}.vcf"
    cat $FILE | awk 'BEGIN{OFS="\t"} {if ($7 != "FILTER") gsub(/.+/,".",$7); print}' > output/${FILENAME}_${FILE_SUFFIX}.vcf
done
